* Compile the project
  Rq: There are 2 sets of VMs Prices/Gflops
  It is defined by constants and can be modified in ./src/simu_tools/hostCost.h
  
  mkdir build && cmake -DSimGrid_PATH=/path/to/ -DHOST_PRICE=1 ..

  HOST_PRICE=1||2,
  SimGrid can be system installed then -DSimGrid is not required.

* Run and compile in docker : 

  - Build the image : cd ./tools/docker && docker build -t simgrid:v3.15 . && cd ../..

  - In the file ./tools/docker/run_docker.sh, modify the "volume" variable by specifying the absolute path of the project directory.

  - Run the docker : ./tools/docker/run_docker.sh

  - Compile the project : make 

  - Now the executable is place in ./bin and we can normaly use it. Example : 
    ./bin/hex_price1 heftBudg 1 data/workflows/cybershake/cybershake30_2 data/platforms/AL60.xml 0.25 0.0 900 604 0.0

* Generation of Strassen and Winograd (directly in the folder ./tools/genWF)

  - Compile the benchmark : gcc -O3 -lcblas benchMatrixOP.c -o benchMatrixOP

  - To generate Strassen's graph : ./genStrassen.sh -l 1 -g (-l : precise the number of level / -g : to have graph)

  - To generate StrassenFred's graph : ./genStrassenFred.sh -l 1 

  - To generate Winograd's graph : ./genWinograd.sh -l 1 

* Run the simulator
  to play with the compiled simulator:
  bin/hex_price1 minmin 2 data/workflows/cybershake/cybershake20_1 data/platforms/datacenter.xml 0.5 0.2 900 1500 0
  bin/hex_price2 heft 2 data/workflows/cybershake20_1 data/platforms/datacenter.xml 0.5 0.2 900 1500 0

  (programme algo workflow platform percent_standardValue)
  in the right order :
- scheduling algorithm (minmin)
- seed (2 in the example)
- worflow (data/workflows/CyberShake_30.xml)
- platform (data/platforms/datacenter.xml)
- percent of mean determining the standard deviation (0.5)
- percent of variation in the cost per flop between two kind of machines of different speed (0.2)
- deadline (900)
- budget (1500)
- variance index var for the random generation of the theoric amount of operations which have to be done for each task of calculus (newVal ~ (oldmean, var*oldmean))
  to keep the same value, put to zero

NB: if you want to generate multiple different samples, DON'T FORGET TO CHANGE THE VALUE OF THE SEED
    the random generator has only been tested for variance index of less than 2 (I wanted to keep in a certain mesure a trace of the original data. If we start to multiply the original value per seven or whatever, we are scheduling an other dag)

    Your results will be in the commonResults file, which will be generated if
    it didn't exist, or will just be appended to the old one if any. The results
    will be given on the form of a csv file, in this order:
    Wf: name of the workflow
    type: not used, cyber by default
    algo: which algorithm created the scheduling
    nbTsk: number of tasks of the workflow
    tmpWf: makespan (in seconds)
    costWf: cost (BEWARE! because of a mistake, it is 3600 times too big, don't forget to take account of this in your analysis!)
    budget_residuel: how much of the budget had not been used at the end of the simulation (same WARNING as costWf)
    budget_ini: how much money was given to the algorithm to find a scheduling (BEWARE (cf. costWf)! Give it 3600 times the cost)
    nb_VM_used: how many VMs have been used
    prct_VM_used: time of execution/time reserved
    worstTCost: cost for a scheduling in which all the tasks have been allocated to one single VM, of the cheapest type
    worstTTime: makespan for a scheduling in which all the tasks have been allocated to one single VM, of the cheapest type
    diffR: difference ratio in cost between two types of VM
    pcVarTeor: percent of variation in the cost per flop between two kind of machines of different speed
    pcVarReal: percent of mean determining the standard deviation
    seed: seed used during the simulation
    worstCCost: cost for a scheduling in which each task has been allocated to its own VM, of the most expansive type
    worsCTime: makespan for a scheduling in which each task has been allocated to its own VM, of the most expansive type

    More precise results are given in commonPreciseResults (the 4 last columns of commonResultVierge)
    hostName: name of the used host
    hostType: number of the type of host used
    hostUse: how much time this host has been reserved
    hostCalc: how much time this host has been used to execute tasks
    
    commonResultsVierge is the header recapituling all the columns of the generated csv for commonPreciseResults. For
    commoResults, delete the 4 last columns.

* Run and compile in docker : 

  - Build the image : cd ./tools/docker && docker build -t simgrid:v3.15 . && cd ../..

  - In the file ./tools/docker/run_docker.sh, modify the "volume" variable by specifying the absolute path of the project directory.

  - Run the docker : ./tools/docker/run_docker.sh

  - Compile the project : make 

  - Now the executable is place in ./bin and we can normaly use it. Example : 
    ./bin/hex heftBudg 1 data/workflows/cybershake/cybershake30_2 data/platforms/AL60.xml 0.25 0.0 900 604 0.0


* Comments and units
  Because of an inattention during the conception of the simulator, prices
  (in $) are actually 3600 times higher than what they should have been.
  Hence the division by 3600 during the analysis.
  Times are in seconds.

  Similarly, because of an inattention in the code, the calculus of minimal 
  values included in each tuple is actually wrong. I had then generated 
  separately good ones, hence their loading from another source instead of 
  from the considered tuple.
* About the generated workflows
  The workflows used in the experiments made for the paper have been created
  with the generator provided there:
  https://confluence.pegasus.isi.edu/display/pegasus/WorkflowGenerator
  You'll find a script-example to show how they have been generated. Halas, 
  because there wasn't any way to fix a specific seed, I have no way to 
  give the exact values to reproduce the exat same workflows I use. This is
  why I provide the generated files rather than the exact way to create them.
  For french users, beware: the workflow generator, writen in java, has the
  problematic compulsion to write comas instead of dots in numbers. Don't 
  forget to change them back or be prepared to exotic errors with the simulator.

* About the data analysis
  Most configurable things are at the very beginning of the file.
  Especially: don't forget to configure the right place where your data are,
  or it won't work. Same thing for the output data. Furthermore, if you don't 
  want to have to change anything later in the save command lines, you will
  have to create, in your output data folder, a *outputData*/FAMD folder (my 
  script can't generate new folders).
  This script is really long, so here are the major parts composing it:
** 1) Configuration
** 2) Data loading
** 3) Multivariate analysis (l.1458)
** 4) Makespan, cost and %VM used (l.2213)
** 5) Viability (l.2656)
** 6) Execution times (l.3204)
