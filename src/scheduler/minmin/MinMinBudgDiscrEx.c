#include <iostream>
#include <map>
#include <string>
#include <sys/wait.h>

#include "MinMin.h"
#include "calcBudgPerTask.h"
#include "hostCost.h"
#include "toolsSched.h"
#include "workflow.h"

// tool min-min : the best host is the one which finish the execution of the
// given task the sooner
sg_host_t SD_task_get_best_host_minmin_budget_disc_ex(
    SD_task_t task, std::map<sg_host_t, HostCost> &mhc,
    std::map<SD_task_t, double> budgPTsk, double cagnotte, double &timeCalc,
    double &cagnotteFin) {
  // récupération des hôtes potentiels
  sg_host_t *hosts = sg_host_list();
  int nhosts = sg_host_count();
  sg_host_t best_host = hosts[0];
  double cost = 0;
  double timeCalcTps = 0;
  double cagnotteTmp = cagnotte;

  // était finish_on_at(task, hosts[0], mhc)
  double min_EFT = finish_on_at(task, hosts[0]);

  double cHor = getHC_hC(mhc[hosts[0]]);

  // initialisation : hôte le moins cher
  for (int i = 0; i < nhosts; i++) {
    // si moins cher, devient le best host
    if (getHC_hC(mhc[hosts[i]]) <= cHor) {
      cHor = getHC_hC(mhc[hosts[i]]);
      best_host = hosts[i];
    }
  }

  // récupération du budget de la tâche
  double budgTsk = cagnotte + budgPTsk[task];

  double costTmp = 0.;

  double EFT = finish_on_at(task, best_host);

  // calcul du premier cost
  if (getHC_occTime(mhc[best_host]) <= 1e-6) {
    std::cout << "nouvel host \n";
    timeCalcTps = EFT - SD_get_clock();
  } else {
    std::cout << "vieil host\n";
    timeCalcTps = EFT - getHC_occTime(mhc[best_host]);
    std::cout << "origine calc : " << getHC_occTime(mhc[best_host]) << "\n";
    std::cout << "temps calc : " << timeCalcTps << "\n";
  }

  // timeCalcTps = EFT - getHC_occTime(mhc[hosts[i]]);
  // cost = timeCalcTps * getHC_hC(mhc[hosts[i]]);

  costTmp = timeCalcTps * getHC_hC(mhc[best_host]);
  std::cout << "cost calc : " << costTmp << "\n";

  // on choisit l'hôte qui permet de finir le plus vite ET qui respecte le
  // budget
  for (int i = 1; i < nhosts; i++) {
    // était finish_on_at(task, hosts[0], mhc)
    EFT = finish_on_at(task, hosts[i]);

    //========================================================================================

    // calcul du coût induit par ce placement :
    // soit la machine est dispo et on commence de suite, soit on attend qu'elle
    // se libère
    // timeCalcTps = EFT - MAX(getHC_occTime(mhc[hosts[i]]), SD_get_clock());

    // soit la machine avait été utilisée et on doit payer tout depuis la
    // dernière fois, soit elle est toute neuve et on ne paie qu'à partir de
    // maintenant
    if (getHC_occTime(mhc[hosts[i]]) <= 1e-6) {
      // std::cout <<"nouvel host \n";
      timeCalcTps = EFT - SD_get_clock();
    } else {
      // std::cout <<"vieil host\n";
      timeCalcTps = EFT - getHC_occTime(mhc[hosts[i]]);
    }

    // timeCalcTps = EFT - getHC_occTime(mhc[hosts[i]]);
    // cost = timeCalcTps * getHC_hC(mhc[hosts[i]]);

    cost = timeCalcTps * getHC_hC(mhc[hosts[i]]);

    // std::cout <<"hôte "<<sg_host_get_name(hosts[i]) <<" dispo à : " <<
    // getHC_occTime(mhc[hosts[i]]) <<"\n";

    if ((EFT < min_EFT) && (cost <= budgTsk)) {
      min_EFT = EFT;
      best_host = hosts[i];
      timeCalc = timeCalcTps;
      costTmp = timeCalcTps * getHC_hC(mhc[hosts[i]]);
      cagnotteTmp = budgTsk - costTmp;
      /*
      std::cout <<"******************** changement ! \n";
      std::cout <<"     getBestHost inter : tâche = " <<SD_task_get_name(task)
      <<"\n"; std::cout<< "     getBestHost inter : nouveau meilleur hôte = "
      <<sg_host_get_name(best_host) <<"\n"; std::cout<< "     getBestHost inter
      : nouveau coût = " <<costTmp <<"\n"; std::cout<< "     getBestHost inter :
      budget = " <<budgTsk <<"\n"; std::cout<< "     getBestHost inter : EFT = "
      <<EFT <<"\n"; std::cout<< "     getBestHost inter : time calc = "
      <<timeCalcTps <<"\n"; std::cout<< "     getBestHost inter : host dispo à :
      " <<sg_host_get_available_at(best_host)<<"\n";
      */
    }
    // si exaequo dans les EFT, on opte pour le moins cher
    if ((EFT == min_EFT) && (cost < costTmp)) {
      min_EFT = EFT;
      best_host = hosts[i];
      timeCalc = timeCalcTps;
      costTmp = timeCalcTps * getHC_hC(mhc[hosts[i]]);
      cagnotteTmp = budgTsk - costTmp;
      /*
      std::cout <<"******************** changement ! \n";
      std::cout <<"     getBestHost inter : tâche = " <<SD_task_get_name(task)
      <<"\n"; std::cout<< "     getBestHost inter : nouveau meilleur hôte = "
      <<sg_host_get_name(best_host) <<"\n"; std::cout<< "     getBestHost inter
      : nouveau coût = " <<costTmp <<"\n"; std::cout<< "     getBestHost inter :
      budget = " <<budgTsk <<"\n"; std::cout<< "     getBestHost inter : EFT = "
      <<EFT <<"\n"; std::cout<< "     getBestHost inter : time calc = "
      <<timeCalcTps <<"\n"; std::cout<< "     getBestHost inter : host dispo à :
      " <<sg_host_get_available_at(best_host)<<"\n";
      */
    }

    /*
    if ((EFT < min_EFT) && (cost > budgTsk)) {
      std::cout <<"******************** trop cher ! \n";
      std::cout <<"     getBestHost inter : tâche = " <<SD_task_get_name(task)
    <<"\n"; std::cout<< "     getBestHost inter : nouveau meilleur hôte = "
    <<sg_host_get_name(hosts[i]) <<"\n"; std::cout<< "     getBestHost inter :
    nouveau coût = " <<timeCalcTps * getHC_hC(mhc[hosts[i]]) <<"\n"; std::cout<<
    "     getBestHost inter : budget = " <<budgTsk <<"\n"; std::cout<< "
    getBestHost inter : EFT = " <<EFT <<"\n"; std::cout<< "     getBestHost
    inter : time calc = " <<timeCalcTps <<"\n"; std::cout<< "     getBestHost
    inter : host dispo à : " <<sg_host_get_available_at(hosts[i])<<"\n";


      }
*/

    std::cout << "                 getBestHost inter : tâche = "
              << SD_task_get_name(task) << "\n";
    std::cout << "                 getBestHost inter : host = "
              << sg_host_get_name(hosts[i]) << "\n";
    std::cout << "                 getBestHost inter : EFT = " << EFT << "\n";
    std::cout << "                 getBestHost inter : min_EFT = " << min_EFT
              << "\n";
    std::cout << "                 getBestHost inter : nb hosts = " << nhosts
              << "\n";
    std::cout << "                 getBestHost inter : vitesse = "
              << sg_host_speed(hosts[i]) << "\n";
    std::cout << "                 getBestHost inter : cost = " << costTmp
              << "\n";
    std::cout << "\n\n";
  }
  std::cout << "     -----CHOISI : \n     getBestHost : tâche = "
            << SD_task_get_name(task) << "\n";
  std::cout << "     getBestHost : min_EFT = " << min_EFT << "\n";
  std::cout << "     getBestHost : cagnotte = " << cagnotte << "\n";
  std::cout
      << "     getBestHost : cagnotteTmp (= ce qui reste après cette tâche) = "
      << cagnotteTmp << "\n";
  std::cout << "     getBestHost : cagnotteFin = "
            << cagnotte + budgPTsk[task] - costTmp << "\n";
  std::cout << "     getBestHost : cost = " << costTmp << "\n";
  std::cout << "     getBestHost : budget seul  = " << budgPTsk[task] << "\n";
  std::cout << "     getBestHost : budget cagnotte comprise = " << budgTsk
            << "\n";
  std::cout << "     getBestHost : meilleur hôte = "
            << sg_host_get_name(best_host) << "\n";
  cagnotteFin = cagnotteTmp;

  //  cagnotteFin = cagnotte + budgPTsk[task] - costTmp;

  xbt_free(hosts);
  return best_host;
}

//========================================================================================
/**
 * Fait l'ordonnancement et retourne le mapping tâche -> hôte + l'odre
 *d'attribution (vu qu'il remet sans cesse en question l'ordre d'attribution et
 *que seul
 *
 * ordo_t = tableau des tâches ordonnancées (données dans l'ordre de leur
 *ordonnancement) wf = workflow à placer m = mapping <tâche, hôte> effectué par
 *l'algorithme
 **/
void algo_offline_minmin_budget_discr_ex(Workflow &wf, SD_task_t ordoT[],
                                         std::map<SD_task_t, sg_host_t> &m,
                                         double deadline, double budget,
                                         double cagnotte,
                                         std::map<sg_host_t, HostCost> &mhc) {
  size_t cursor;
  SD_task_t task;

  // cet algo remet tout l'ordre dans lequel les tâches sont ordonnancées en
  // question pendant ses calculs du meilleur hôte pour la meilleure tâche : une
  // simu est à faire dès le début

  // let's fork
  int fd[2];
  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    close(fd[0]); /* close read */

    unsigned int cursor;
    SD_task_t task;
    double min_finish_time = -1.0;
    SD_task_t selected_task = NULL;
    xbt_dynar_t ready_tasks;
    sg_host_t selected_host = NULL;

    double timeCalc = 0.;

    double cagnotteFin = 0.;
    double cgVraie = 0.;

    xbt_dynar_t dax = getDagWf(wf);

    std::map<SD_task_t, size_t>
        nb_ord; // map associant une tâche au numéro indiquant dans quel ordre
                // elles ont été ordonnancées
    size_t nb_ordo_tmp =
        0; // compteur indiquant à quel moment la tâche a été ordonnancée

    //==================================================
    xbt_dynar_foreach(dax, cursor, task) { SD_task_watch(task, SD_DONE); }
    //==================================================

    //-----------------------------------------------------------
    // pre-calculi: repartition of various budgets, for example
    //.........................................................
    std::list<SD_task_t> lT;

    size_t total_nhosts = sg_host_count();
    sg_host_t *hosts = sg_host_list();

    // bw moyenne sauvage, latence aussi (par modèle, c'est les mêmes partout)
    double bw = sg_host_route_bandwidth(hosts[0], hosts[1]) * 10000;

    // calcul du nombre de VMs max :
    int nbVMsMax = nbMaxVMn(dax);

    std::cout << "----------------------- nb VMs max : " << nbVMsMax << "\n";

    // coûts et vitesse moyens :
    double horCost_m = 0;
    double speed_m = 0;
    double iniCost_m = 0;

    for (cursor = 0; cursor < total_nhosts; cursor++) {
      HostCost testHc = mhc[hosts[cursor]];
      horCost_m += getHC_hC(testHc);
      speed_m += sg_host_speed(hosts[cursor]);
      iniCost_m += getHC_iniC(testHc);
    }

    horCost_m = horCost_m / total_nhosts;
    speed_m = speed_m / total_nhosts;
    iniCost_m = iniCost_m / total_nhosts;

    std::cout << "vitesse moyenne : " << speed_m << "\n";
    std::cout << "coût horaire moyen : " << horCost_m << "\n";

    // réévaluation du budget
    std::cout << "budget initial : " << budget << "\n";
    double updateB = budget - (nbVMsMax * iniCost_m + cagnotte);
    std::cout << "budget à jour : " << updateB << "\n";
    budget = updateB;

    // calcul du budget par tâche
    std::map<SD_task_t, double> budgPTsk;
    calcBudgPTsk(wf, updateB, bw, speed_m, budgPTsk);
    int i = 0;

    xbt_dynar_foreach(dax, i, task) {
      if (isTaskComput(task)) {
        std::cout << "tache " << SD_task_get_name(task)
                  << ", budget : " << budgPTsk[task] << "\n";
      }
    }
    //==================================================
    // scheduling
    //------------

    // Schedule the root first
    xbt_dynar_get_cpy(dax, 0, &task);
    sg_host_t host = SD_task_get_best_host_minmin_budget_disc_ex(
        task, mhc, budgPTsk, cagnotte, timeCalc, cagnotteFin);

    // allouer la tâche à l'hôte
    SD_task_schedulel(task, 1, host);
    m[task] = host;
    // associer son numéro d'ordre d'ordonnancement
    nb_ord[task] = nb_ordo_tmp;
    // incrémenter pour avoir le numéro suivant
    nb_ordo_tmp++;
    // valider la modification de la cagnotte
    cagnotte = cagnotteFin;

    // std::cout<< "------------------ schedulons : " <<SD_task_get_name(task)
    // <<" sur " <<sg_host_get_name(host) <<"\n";

    std::cout << "finish on at premier : "
              << finish_on_at(task, host) + getHC_tmpIni(mhc[host]) << "\n";
    setHC_occTime(mhc[host],
                  finish_on_at(task, host) + getHC_tmpIni(mhc[host]) + 0.1);

    xbt_dynar_t changed_tasks = xbt_dynar_new(sizeof(SD_task_t), NULL);
    SD_simulate_with_update(-1.0, changed_tasks);

    // pour tests : nombre de tâches
    //  int compt, compt2;
    //  SD_task_t comptTsk;

    while (!xbt_dynar_is_empty(changed_tasks)) {

      // Get the set of ready tasks
      ready_tasks = get_ready_tasks(dax);
      xbt_dynar_reset(changed_tasks);

      if (xbt_dynar_is_empty(ready_tasks)) {
        xbt_dynar_free_container(&ready_tasks);
        // there is no ready task, let advance the simulation
        SD_simulate_with_update(-1.0, changed_tasks);
        continue;
      }
      // For each ready task:
      // get the host that minimizes the completion time.
      // select the task that has the minimum completion time on its best host.
      //

      xbt_dynar_foreach(ready_tasks, cursor, task) {
        //      std::cout <<SD_task_get_name(task) <<" is ready \n";
        // XBT_DEBUG("%s is ready", SD_task_get_name(task));
        host = SD_task_get_best_host_minmin_budget_disc_ex(
            task, mhc, budgPTsk, cagnotte, timeCalc, cagnotteFin);
        double finish_time = finish_on_at(task, host);
        if (min_finish_time < 0 || finish_time < min_finish_time) {
          min_finish_time = finish_time;
          selected_task = task;
          selected_host = host;
          cgVraie = cagnotteFin;
          std::cout << SD_task_get_name(task) << ", cagnotte vraie :" << cgVraie
                    << "\n";

          // std::cout <<"éééééééééééééééé hôte "<<sg_host_get_name(host)<<"
          // dispo à : " <<	  sg_host_get_available_at(host) <<"\n";
          // std::cout <<"éééééééééééééééé hôte dispo à : " <<
          // getHC_occTime(mhc[host]) <<"\n"; std::cout <<"éééééééééééééééé hôte
          // dispo --- coût horaire --- : " << getHC_hC(mhc[host]) <<"\n";
        } else {
          // on réinitialise la cagnotteFin avant de rechercher le suivant
          cagnotteFin = cagnotte;
        }
      }

      // allouer la tâche à l'hôte
      SD_task_schedulel(selected_task, 1, selected_host);
      m[selected_task] = selected_host;
      // associer son numéro d'ordre d'ordonnancement
      nb_ord[selected_task] = nb_ordo_tmp;
      // incrémenter pour avoir le numéro suivant
      nb_ordo_tmp++;
      // valider la modification de la cagnotte
      cagnotte = cgVraie;

      // std::cout<< "------------------ schedulons : "
      // <<SD_task_get_name(selected_task) <<" sur "
      // <<sg_host_get_name(selected_host) <<"\n";

      // SimDag allows tasks to be executed concurrently when they can by
      // default. Yet schedulers take decisions assuming that tasks wait for
      // resource availability to start. The solution (well crude hack is to
      // keep track of the last task scheduled on a host and add a special type
      // of dependency if needed to force the sequential execution meant by the
      // scheduler. If the last scheduled task is already done, has failed or is
      // a predecessor of the current task, no need for a new dependency
      SD_task_t last_scheduled_task =
          sg_host_get_last_scheduled_task(selected_host);
      if (last_scheduled_task &&
          (SD_task_get_state(last_scheduled_task) != SD_DONE) &&
          (SD_task_get_state(last_scheduled_task) != SD_FAILED) &&
          !SD_task_dependency_exists(
              sg_host_get_last_scheduled_task(selected_host), selected_task)) {
        SD_task_dependency_add("resource", NULL, last_scheduled_task,
                               selected_task);
      }
      sg_host_set_last_scheduled_task(selected_host, selected_task);
      sg_host_set_available_at(selected_host, min_finish_time);
      setHC_occTime(mhc[selected_host], min_finish_time);
      std::cout << "min finish time mis = " << min_finish_time << "\n";
      std::cout << "min finish time eu = "
                << sg_host_get_available_at(selected_host) << "\n";
      std::cout << "min finish time eu = " << getHC_occTime(mhc[selected_host])
                << "\n";

      xbt_dynar_free_container(&ready_tasks);
      // reset the min_finish_time for the next set of ready tasks
      min_finish_time = -1.;
      xbt_dynar_reset(changed_tasks);
      SD_simulate_with_update(-1.0, changed_tasks);
    }

    //=====================================
    // placer la dernière tache restante
    //-----------------------------------
    ready_tasks =
        get_ready_tasks(dax); // getready tasks fait son petit tri de tâches
    xbt_dynar_reset(changed_tasks);

    if (xbt_dynar_is_empty(ready_tasks)) {
      xbt_dynar_free_container(&ready_tasks);
      // there is no ready task, let advance the simulation
      SD_simulate_with_update(-1.0, changed_tasks);
    }

    // For each ready task:
    // get the host that minimizes the completion time.
    // select the task that has the minimum completion time on its best host.
    //
    xbt_dynar_foreach(ready_tasks, cursor, task) {
      host = SD_task_get_best_host_minmin_budget_disc_ex(
          task, mhc, budgPTsk, cagnotte, timeCalc, cagnotteFin);
      double finish_time = finish_on_at(task, host);
      if (min_finish_time < 0 || finish_time < min_finish_time) {
        min_finish_time = finish_time;
        selected_task = task;
        selected_host = host;
      }
    }

    if ((SD_task_get_state(selected_task) != SD_SCHEDULED) &&
        (SD_task_get_state(selected_task) != 32)) {
      // Allouer la tâche
      SD_task_schedulel(selected_task, 1, selected_host);
      m[selected_task] = selected_host;
      // associer son numéro d'ordre d'ordonnancement
      nb_ord[selected_task] = nb_ordo_tmp;
      // incrémenter pour avoir le numéro suivant
      nb_ordo_tmp++;
      // valider la modification de la cagnotte
      cagnotte = cagnotteFin;
    }

    // std::cout<<"èèèèèè dans fonction  : " << SD_task_get_name(selected_task)
    // <<" sur la machine "<<  sg_host_get_name(m[selected_task]) <<"\n";

    SD_simulate(-1);

    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number;
        ordo_number = nb_ord[task];
        if (write(fd[1], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error write ordo\n");
          exit(-1);
        }

        unsigned int hostnamelen = strlen(sg_host_get_name(m[task]));
        if (write(fd[1], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error write hostname length\n");
          exit(-1);
        }

        while (hostnamelen != 0) {
          size_t nb_bytes =
              write(fd[1], sg_host_get_name(m[task]), hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de write dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        }
      }
    }

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    deleteWf(wf);

    for (cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } // end child
  else {
    // fait l'ordo
    close(fd[1]); /* close write */
    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        unsigned int ordo_number = 0;
        if (read(fd[0], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error read ordo\n");
          exit(-1);
        }

        unsigned int hostnamelen = strlen(SD_task_get_name(task));
        if (read(fd[0], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error read hostname length\n");
          exit(-1);
        }

        unsigned int hostLen =
            hostnamelen; // to keep the lenght of the recieved host name
                         // somewhere and be able to put the \0 at the end of
                         // the reception
        char *nomHost = (char *)malloc(sizeof(char) * (hostnamelen + 1));
        while (hostnamelen != 0) {
          size_t nb_bytes = read(fd[0], nomHost, hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de read dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        } // end of receive name of host

        nomHost[hostLen] = '\0';
        // std::cout <<"j'ai lu le nom de l'hôte : " <<nomHost <<" pour la tâche
        // : " <<SD_task_get_name(task) <<" d'indice : " <<ordo_number  <<"\n";
        m[task] = sg_host_by_name(nomHost);
        ordoT[ordo_number] = task;
        free(nomHost);
      } // end of if task is executable
    }   // end of foreach
  }     // end parent
} // end of func