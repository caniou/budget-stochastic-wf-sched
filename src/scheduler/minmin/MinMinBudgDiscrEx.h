#ifndef MINMIN_BUDG_DISCR_EX_H
#define MINMIN_BUDG_DISCR_EX_H

#include "workflow.h"

sg_host_t SD_task_get_best_host_minmin_budget_disc_ex(
    SD_task_t task, std::map<sg_host_t, HostCost> &mhc,
    std::map<SD_task_t, double> budgPTsk, double cagnotte, double &timeCalc,
    double &cagnotteFin);

void algo_offline_minmin_budget_discr_ex(Workflow &wf, SD_task_t ordoT[],
                                         std::map<SD_task_t, sg_host_t> &m,
                                         double deadline, double budget,
                                         double cagnotte,
                                         std::map<sg_host_t, HostCost> &mhc);

#endif