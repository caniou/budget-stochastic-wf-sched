#include <iostream>
#include <map>
#include <string>
#include <sys/wait.h>

#include "hostCost.h"
#include "toolsSched.h"
#include "workflow.h"

// tool min-min : the best host is the one which finish the execution of the
// given task the sooner
sg_host_t SD_task_get_best_host_minmin(SD_task_t task) {
  //  std::cout <<"best host for " <<SD_task_get_name(task) <<"\n";
  sg_host_t *hosts = sg_host_list();

  int nhosts = sg_host_count();
  sg_host_t best_host = hosts[1];
  //  std::cout<< "finish on at done, host 0 = " <<hosts[0] <<"\n";

  double min_EFT = finish_on_at(task, hosts[1]);

  //--------------------------------------------------------------
  // we won't test multiple times the same type of empty VM
  //--------------------------------------------------------
  int aTester = 0;
  int valVM[3] = {0, 0, 0};
  //--------------------------------------------------------------

  for (int i = 0; i < nhosts; i++) {

    if (strcmp(sg_host_get_name(hosts[i]), "S") != 0) {

      // si la machine est déjà utilisée, faire le test
      if (sg_host_get_last_scheduled_task(hosts[i]) != NULL) {
        // std::cout<< "host vieux\n";
        aTester = 1;
      }

      // si la machine est neuve, n'en prendre qu'une par type de VM
      else {

        // get the type of VM
        const char *name = sg_host_get_name(hosts[i]);
        if (strlen(name) >= 3) {
          if (name[2] == '1') {
            if (valVM[0] != 2) {
              valVM[0] = 1;
              aTester = 1;
            }
          }
          if (name[2] == '2') {
            if (valVM[1] != 2) {
              valVM[1] = 1;
              aTester = 1;
            }
          }
          if (name[2] == '3') {
            if (valVM[2] != 2) {
              valVM[2] = 1;
              aTester = 1;
            }
          }
        } // end of type of vm

      } // end of if host is new

      if (aTester == 1) {
        double EFT = finish_on_at(task, hosts[i]);
        //    XBT_DEBUG("%s finishes on %s at %f", SD_task_get_name(task),
        //    sg_host_get_name(hosts[i]), EFT);

        if (EFT < min_EFT) {
          min_EFT = EFT;
          best_host = hosts[i];

          // remise à zéro des variables de vérif
          int iValVM;
          for (iValVM = 0; iValVM < 3; iValVM++) {
            if (valVM[iValVM] == 1) {
              valVM[iValVM] = 0;
              // std::cout<< "host neuf\n";
            }
          }
          aTester = 0;

        } else {
          // indiquer que ce type de VM neuve n'améliore pas le résultat
          int iValVM;
          for (iValVM = 0; iValVM < 3; iValVM++) {
            if (valVM[iValVM] == 1) {
              valVM[iValVM] = 2;
            }
          }
          aTester = 0;
        }
      }
    }
  }

  xbt_free(hosts);

  //    std::cout <<"best host for " <<SD_task_get_name(task) <<" : "
  //    <<sg_host_get_name(best_host) <<"\n";
  return best_host;
}

//========================================================================================
/**
 * Fait l'ordonnancement et retourne le mapping tâche -> hôte + l'odre
 *d'attribution (vu qu'il remet sans cesse en question l'ordre d'attribution et
 *que seul
 *
 * ordo_t = tableau des tâches ordonnancées (données dans l'ordre de leur
 *ordonnancement) wf = workflow à placer m = mapping <tâche, hôte> effectué par
 *l'algorithme
 **/
void algo_offline_minmin(Workflow &wf, SD_task_t ordoT[],
                         std::map<SD_task_t, sg_host_t> &m) {
  size_t cursor;
  SD_task_t task;

  // cet algo remet tout l'ordre dans lequel les tâches sont ordonnancées en
  // question pendant ses calculs du meilleur hôte pour la meilleure tâche : une
  // simu est à faire dès le début

  // let's fork
  int fd[2];
  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    close(fd[0]); /* close read */
    unsigned int cursor;
    SD_task_t task;
    double min_finish_time = -1.0;
    SD_task_t selected_task = NULL;
    xbt_dynar_t ready_tasks;
    sg_host_t selected_host = NULL;

    xbt_dynar_t dax = getDagWf(wf);

    std::map<SD_task_t, size_t>
        nb_ord; // map associant une tâche au numéro indiquant dans quel ordre
                // elles ont été ordonnancées
    size_t nb_ordo_tmp =
        0; // compteur indiquant à quel moment la tâche a été ordonnancée

    //==================================================
    xbt_dynar_foreach(dax, cursor, task) { SD_task_watch(task, SD_DONE); }
    //==================================================
    // Schedule the root first
    xbt_dynar_get_cpy(dax, 0, &task);
    sg_host_t host = SD_task_get_best_host_minmin(task);

    // allouer la tâche à l'hôte
    SD_task_schedulel(task, 1, host);
    m[task] = host;
    // associer son numéro d'ordre d'ordonnancement
    nb_ord[task] = nb_ordo_tmp;
    // incrémenter pour avoir le numéro suivant
    nb_ordo_tmp++;

    // std::cout<< "------------------ schedulons : " <<SD_task_get_name(task)
    // <<" sur " <<sg_host_get_name(host) <<"\n";

    xbt_dynar_t changed_tasks = xbt_dynar_new(sizeof(SD_task_t), NULL);
    SD_simulate_with_update(-1.0, changed_tasks);

    // pour tests : nombre de tâches
    //  int compt, compt2;
    //  SD_task_t comptTsk;

    while (!xbt_dynar_is_empty(changed_tasks)) {

      // Get the set of ready tasks
      ready_tasks = get_ready_tasks(dax);
      xbt_dynar_reset(changed_tasks);

      if (xbt_dynar_is_empty(ready_tasks)) {
        xbt_dynar_free_container(&ready_tasks);
        // there is no ready task, let advance the simulation
        SD_simulate_with_update(-1.0, changed_tasks);
        continue;
      }
      // For each ready task:
      // get the host that minimizes the completion time.
      // select the task that has the minimum completion time on its best host.
      //
      xbt_dynar_foreach(ready_tasks, cursor, task) {
        //      std::cout <<SD_task_get_name(task) <<" is ready \n";
        // XBT_DEBUG("%s is ready", SD_task_get_name(task));
        host = SD_task_get_best_host_minmin(task);
        double finish_time = finish_on_at(task, host);
        if (min_finish_time < 0 || finish_time < min_finish_time) {
          min_finish_time = finish_time;
          selected_task = task;
          selected_host = host;
        }
      }

      // allouer la tâche à l'hôte
      SD_task_schedulel(selected_task, 1, selected_host);
      m[selected_task] = selected_host;
      // associer son numéro d'ordre d'ordonnancement
      nb_ord[selected_task] = nb_ordo_tmp;
      // incrémenter pour avoir le numéro suivant
      nb_ordo_tmp++;

      // std::cout<< "------------------ schedulons : "
      // <<SD_task_get_name(selected_task) <<" sur "
      // <<sg_host_get_name(selected_host) <<"\n";

      // SimDag allows tasks to be executed concurrently when they can by
      // default. Yet schedulers take decisions assuming that tasks wait for
      // resource availability to start. The solution (well crude hack is to
      // keep track of the last task scheduled on a host and add a special type
      // of dependency if needed to force the sequential execution meant by the
      // scheduler. If the last scheduled task is already done, has failed or is
      // a predecessor of the current task, no need for a new dependency
      SD_task_t last_scheduled_task =
          sg_host_get_last_scheduled_task(selected_host);
      if (last_scheduled_task &&
          (SD_task_get_state(last_scheduled_task) != SD_DONE) &&
          (SD_task_get_state(last_scheduled_task) != SD_FAILED) &&
          (!isEnd(last_scheduled_task)) &&
          !SD_task_dependency_exists(
              sg_host_get_last_scheduled_task(selected_host), selected_task)) {
        SD_task_dependency_add("resource", NULL, last_scheduled_task,
                               selected_task);
      }
      sg_host_set_last_scheduled_task(selected_host, selected_task);
      sg_host_set_available_at(selected_host, min_finish_time);

      xbt_dynar_free_container(&ready_tasks);
      // reset the min_finish_time for the next set of ready tasks
      min_finish_time = -1.;
      xbt_dynar_reset(changed_tasks);
      SD_simulate_with_update(-1.0, changed_tasks);
    }

    //=====================================
    // placer la dernière tache restante
    //-----------------------------------
    ready_tasks =
        get_ready_tasks(dax); // getready tasks fait son petit tri de tâches
    xbt_dynar_reset(changed_tasks);

    if (xbt_dynar_is_empty(ready_tasks)) {
      xbt_dynar_free_container(&ready_tasks);
      // there is no ready task, let advance the simulation
      SD_simulate_with_update(-1.0, changed_tasks);
    }

    // For each ready task:
    // get the host that minimizes the completion time.
    // select the task that has the minimum completion time on its best host.
    //
    xbt_dynar_foreach(ready_tasks, cursor, task) {
      host = SD_task_get_best_host_minmin(task);
      double finish_time = finish_on_at(task, host);
      if (min_finish_time < 0 || finish_time < min_finish_time) {
        min_finish_time = finish_time;
        selected_task = task;
        selected_host = host;
      }
    }

    if ((SD_task_get_state(selected_task) != SD_SCHEDULED) &&
        (SD_task_get_state(selected_task) != 32)) {
      // Allouer la tâche
      SD_task_schedulel(selected_task, 1, selected_host);
      m[selected_task] = selected_host;
      // associer son numéro d'ordre d'ordonnancement
      nb_ord[selected_task] = nb_ordo_tmp;
      // incrémenter pour avoir le numéro suivant
      nb_ordo_tmp++;
    }

    // std::cout<<"èèèèèè dans fonction  : " << SD_task_get_name(selected_task)
    // <<" sur la machine "<<  sg_host_get_name(m[selected_task]) <<"\n";

    SD_simulate(-1);

    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number;
        ordo_number = nb_ord[task];
        if (write(fd[1], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error write ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(sg_host_get_name(m[task]));
        if (write(fd[1], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error write hostname length\n");
          exit(-1);
        }

        while (hostnamelen != 0) {
          size_t nb_bytes =
              write(fd[1], sg_host_get_name(m[task]), hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de write dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        }
      }
    }

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    deleteWf(wf);

    size_t total_nhosts = sg_host_count();
    sg_host_t *hosts = sg_host_list();

    for (cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } // end child
  else {
    // fait l'ordo
    close(fd[1]); /* close write */
    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number = 0;
        if (read(fd[0], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error read ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(SD_task_get_name(task));
        if (read(fd[0], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error read hostname length\n");
          exit(-1);
        }

        unsigned int hostLen =
            hostnamelen; // to keep the lenght of the recieved host name
                         // somewhere and be able to put the \0 at the end of
                         // the reception
        char *nomHost = (char *)malloc(sizeof(char) * (hostnamelen + 1));
        while (hostnamelen != 0) {
          size_t nb_bytes = read(fd[0], nomHost, hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de read dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        } // end of receive name of host

        nomHost[hostLen] = '\0';
        // std::cout <<"j'ai lu le nom de l'hôte : " <<nomHost <<" pour la tâche
        // : " <<SD_task_get_name(task) <<" d'indice : " <<ordo_number  <<"\n";
        m[task] = sg_host_by_name(nomHost);
        ordoT[ordo_number] = task;
        free(nomHost);
      } // end of if task is executable
    }   // end of foreach

    int stat_val;
    waitpid(childpid, &stat_val, 0);
  } // end parent
} // end of func