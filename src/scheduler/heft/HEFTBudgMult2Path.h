#ifndef HEFT_BUDG_MULT_2_PATH_C
#define HEFT_BUDG_MULT_2_PATH_C

#include "hostCost.h"
#include "workflow.h"

void algo_offline_heft_budg_mult2_path(Workflow &wf, SD_task_t ordoT[],
                                       std::map<SD_task_t, sg_host_t> &m,
                                       double deadline, double budget,
                                       double cagnotte,
                                       std::map<sg_host_t, HostCost> mhc);

#endif