#include <iostream>
#include <list>
#include <map>
#include <sys/wait.h>

#include "HEFT.h"
#include "hostCost.h"
#include "toolsSched.h"
#include "workflow.h"
//==========================================
// outils
//-----------
/***
 * Calcul du temps de transfert pour une tâche sur un host
 **/
double dlTimeTaskOnHost(SD_task_t task, sg_host_t host,
                        std::map<sg_host_t, HostCost> mhc) {
  double dlTime = 0;

  int i;

  // récupération des parents
  xbt_dynar_t parents = SD_task_get_parents(task);

  //................................................
  // calcul des temps de transfert
  //...............................
  if (!xbt_dynar_is_empty(parents)) {
    SD_task_t parent;

    // pour chaque parent, si l'host est différent, rajouter la taille du
    // fichier (la bw est la même pour tous)
    xbt_dynar_foreach(parents, i, parent) {
      // cas où le parent de la tâche est une communication
      if (SD_task_get_kind(parent) == SD_TASK_COMM_E2E) {

        // récupération de la liste des hôtes du parent
        sg_host_t *parent_host = SD_task_get_workstation_list(parent);

        // si le fichier à transférer est de taille nulle, pas de calcul
        if (SD_task_get_amount(parent) <= 1e-6) {
          dlTime = 0;
        } else {
          // si la tâche est end, c'est curieusement plantatoire... pas trouvé
          // pourquoi. enfin si, la bw n'a pas l'air accessible, c'est fou
          if (strcmp(SD_task_get_name(task), "end") == 0) {

            // fix temporaire
            //	    dlTime = 100000;
            dlTime = 0;

          } else {
            // Si le tmp réservé de l'host est nul ajouter le temps d'ini après
            // coup
            if (getHC_resTime(mhc[host]) == 0) {
              dlTime += getHC_iniC(mhc[host]);
            }
            // si l'host du parent est différent de l'host actuel, ajouter le
            // temps de transfert
            if (parent_host[0] != host) {
              dlTime += sg_host_route_latency(parent_host[0], host) +
                        SD_task_get_amount(parent) /
                            sg_host_route_bandwidth(parent_host[0], host);
            }
          }
        }
      }
    }
  }
  return dlTime;
}

/***
 * Calcul du temps de calcul d'une tâche sur un host
 **/
double timeTaskOnHost(SD_task_t task, sg_host_t host,
                      std::map<sg_host_t, HostCost> mhc) {
  double res = 0;
  double dlTime = 0;

  dlTime = dlTimeTaskOnHost(task, host, mhc);
  // tout est téléchargé à la suite, par la même bw, donc on peut sommer

  //................................................
  // temps de calcul purement calculatoire
  //......................................
  res = dlTime + SD_task_get_amount(task) / sg_host_speed(host);

  // std::cout << "**********time task on host : " <<res <<"\n";
  return res;
}

sg_host_t SD_task_get_best_host_heft(SD_task_t task) {
  sg_host_t *hosts = sg_host_list();
  int nhosts = sg_host_count();
  sg_host_t best_host = hosts[1];
  double min_EFT = finish_on_at(task, hosts[1]);

  //--------------------------------------------------------------
  // we won't test multiple times the same type of empty VM
  //--------------------------------------------------------
  int aTester = 0;
  int valVM[3] = {0, 0, 0};
  //--------------------------------------------------------------

  for (int i = 0; i < nhosts; i++) {
    if (strcmp(sg_host_get_name(hosts[i]), "S") != 0) {

      // si la machine est déjà utilisée, faire le test
      if (sg_host_get_last_scheduled_task(hosts[i]) != NULL) {
        // std::cout<< "host vieux\n";
        aTester = 1;
      }

      // si la machine est neuve, n'en prendre qu'une par type de VM
      else {

        // get the type of VM
        const char *name = sg_host_get_name(hosts[i]);
        if (strlen(name) >= 3) {
          if (name[2] == '1') {
            if (valVM[0] != 2) {
              valVM[0] = 1;
              aTester = 1;
            }
          }
          if (name[2] == '2') {
            if (valVM[1] != 2) {
              valVM[1] = 1;
              aTester = 1;
            }
          }
          if (name[2] == '3') {
            if (valVM[2] != 2) {
              valVM[2] = 1;
              aTester = 1;
            }
          }
        } // end of type of vm

      } // end of if host is new

      if (aTester == 1) {

        double EFT = finish_on_at(task, hosts[i]);
        if (EFT < min_EFT) {
          min_EFT = EFT;
          best_host = hosts[i];

          // remise à zéro des variables de vérif
          int iValVM;
          for (iValVM = 0; iValVM < 3; iValVM++) {
            if (valVM[iValVM] == 1) {
              valVM[iValVM] = 0;
              // std::cout<< "host neuf\n";
            }
          }
          aTester = 0;

        } else {
          // indiquer que ce type de VM neuve n'améliore pas le résultat
          int iValVM;
          for (iValVM = 0; iValVM < 3; iValVM++) {
            if (valVM[iValVM] == 1) {
              valVM[iValVM] = 2;
            }
          }
          aTester = 0;
        }
      }
    }
  }
  xbt_free(hosts);

  // std::cout<< "=====================================\n";
  return best_host;
}

//===============================================
/**
 * mSpeed : mean of the speed of all VMs
 * mBw : mean of the bandwiths between the various hosts
 * mLat : mean of the latence for all processors
 **/
// void nbMaxVMnS(unsigned int &maxVMs, std::map<SD_task_t, char> mapVu,
// SD_task_t tAct, xbt_dynar_t dag){
double rank(SD_task_t s, xbt_dynar_t dag, double mSpeed, double mBw,
            double mLat) {
  double res;
  double cpt;
  SD_task_t task; //, selected_task;
  std::list<SD_task_t> lChildren;
  xbt_dynar_t dynChildren;
  xbt_dynar_t dynParents;
  double maxTransf = -1.; // max time needed to transfert data

  double chRank = 0.;
  std::cout << "#";

  // récupération de la liste des fils calculables s'il y en a
  getRunnableChildren(dag, s, lChildren);
  //  std::cout <<" runnable children eus\n";

  lChildren.unique();

  // si pas d'enfant calculable, on est au bout et le rang correspond au nombre
  // d'opérations à effectuer
  // std::cout <<"çççççççççççççççççççççççççççç presnode : "
  // <<SD_task_get_name(s)<<"\n";

  if (lChildren.size() == 0) {
    res = SD_task_get_amount(s) / mSpeed;
    return res;
  } else {
    // calcul du rang
    // recherche du fils demandant le plus de transfert : pour chaque fils

    for (std::list<SD_task_t>::iterator it = lChildren.begin();
         it != lChildren.end(); ++it) {
      // std::cout << " rank, enfants de s : " << SD_task_get_name(*it) <<"\n";

      double transfData = 0.;
      double transfTmp = 0.;
      //----------------------------------------------------
      // calculer la quantité de données à transférer à s
      //.................................................
      // récupérer les enfants directs de s
      dynChildren = SD_task_get_children(s);

      // récupérer les parents directs du child en cours
      dynParents = SD_task_get_parents(*it);

      // pour chaque enfant de s qui a un équivalent dans les parents de child,
      // ajouter la quantité de données au total à transférer
      xbt_dynar_foreach(dynChildren, cpt, task) {

        // valable seulement si l'enfant est une tâche de comm
        if (SD_task_get_kind(task) == SD_TASK_COMM_E2E) {
          // std::cout <<"enfant : "	<<SD_task_get_name(task) <<"\n";

          SD_task_t taskTest;
          size_t curs;

          // on cherche les tâches de communication qui se trouvent entre la
          // tâche actuelle et la tâche de calcul suivante (transfert de l'un à
          // l'autre)
          xbt_dynar_foreach(dynParents, curs, taskTest) {
            if (SD_task_get_kind(taskTest) == SD_TASK_COMM_E2E) {

              // std::cout <<"      parent : " <<SD_task_get_name(taskTest)
              // <<"\n";

              if (strcmp(SD_task_get_name(taskTest), SD_task_get_name(task)) ==
                  0) {
                // tâche de comm commune au père et au fils : transfert à
                // prendre en compte
                transfData += SD_task_get_amount(taskTest);
              }

            } // end if task is comm
          }   // end foreach parent of the current child of s

          // std::cout <<"**********  quantité de data à transférer : "
          // <<transfData <<"\n";

          // needed time to transfert all the data
          // std::cout <<"**********  bande passante : " <<mBw <<"\n";
          // std::cout <<"**********  latence : " <<mLat <<"\n";

        } // fin récup de la tâche de comm la plus longue
          // std:: cout << "rang de tâche de comm la plus coûteuse : "
        // <<selected_task <<"\n"; std:: cout << "rang de tâche de comm la plus
        // coûteuse : " << SD_task_get_name(selected_task) <<"\n";

      } // end foreach child of task
      xbt_dynar_free(&dynChildren);
      // vider la liste des parents directs du child

      // calcul du fils de plus haut rang + données à transférer liées
      transfTmp = (transfData / mBw) + mLat;

      // calcul de la part de rang issue des enfants :
      // std::cout <<SD_task_get_name(*it) <<" hop\n";
      chRank = transfTmp + rank(*it, dag, mSpeed, mBw, mLat);
      //	  chRank = transfTmp;

      if (chRank > maxTransf) {
        maxTransf = chRank;
        // selected_task = *it;
      }
      xbt_dynar_free(&dynParents);
    } // end of foreach it

  } // end of if s has children

  res = SD_task_get_amount(s) / mSpeed + maxTransf;

  //  std::cout <<"rang de  " <<SD_task_get_name(s) << " : " << res <<"\n";
  return res;
}

void getTasksSortedByRank(std::list<SD_task_t> &lT, xbt_dynar_t dag,
                          double mSpeed, double mBw, double mLat) {

  std::map<SD_task_t, double> tR; // map <task, rank>

  SD_task_t task;
  int i;

  xbt_dynar_foreach(dag, i, task) {
    if (isTaskComput(task)) {

      // rank(task, dag, mSpeed, mBw, mLat);
      tR[task] = rank(task, dag, mSpeed, mBw, mLat);

      std::cout << "\n~~~~~~~~~~~~~~~ tâche : " << SD_task_get_name(task)
                << ", rank : " << tR[task] << "\n";
    }
  }

  // tri débilou, en bricoler un plus rapide plus tard
  xbt_dynar_foreach(dag, i, task) {
    if (isTaskComput(task)) {
      if (i == 0) {
        lT.push_back(task);
      }
      // parcours de la liste, et si inférieur ou égal à la valeur actuelle,
      // insérer après
      else {
        /*
        for (std::list<SD_task_t>::iterator it=lT.begin(); it != lT.end();
        ++it){ if(tR[task] <= tR[*it]){ lT.push_back(task); break;
          }
        }
        */
        for (std::list<SD_task_t>::iterator it = lT.begin(); it != lT.end();
             ++it) {
          if (tR[task] <= tR[*it]) {
            lT.insert(it, task);
            break;
          }
        }
      }
    }
  }
  lT.reverse();
}

void getTasksSortedByRankV2(std::map<SD_task_t, double> &tR,
                            std::list<SD_task_t> &lT, xbt_dynar_t dag,
                            double mSpeed, double mBw, double mLat) {

  // map <task, rank>

  SD_task_t task;
  int i;

  xbt_dynar_foreach(dag, i, task) {
    if (isTaskComput(task)) {

      // rank(task, dag, mSpeed, mBw, mLat);
      tR[task] = rank(task, dag, mSpeed, mBw, mLat);

      std::cout << "\n~~~~~~~~~~~~~~~ tâche : " << SD_task_get_name(task)
                << ", rank : " << tR[task] << "\n";
    }
  }

  // tri débilou, en bricoler un plus rapide plus tard
  xbt_dynar_foreach(dag, i, task) {
    if (isTaskComput(task)) {
      if (i == 0) {
        lT.push_back(task);
      }
      // parcours de la liste, et si inférieur ou égal à la valeur actuelle,
      // insérer après
      else {
        /*
        for (std::list<SD_task_t>::iterator it=lT.begin(); it != lT.end();
        ++it){ if(tR[task] <= tR[*it]){ lT.push_back(task); break;
          }
        }
        */
        for (std::list<SD_task_t>::iterator it = lT.begin(); it != lT.end();
             ++it) {
          if (tR[task] <= tR[*it]) {
            lT.insert(it, task);
            break;
          }
        }
      }
    }
  }
  lT.reverse();
}
//========================================================================================
/**
 * Fait l'ordonnancement et retourne le mapping tâche -> hôte + l'odre
 *d'attribution (vu qu'il remet sans cesse en question l'ordre d'attribution et
 *que seul
 *
 * ordo_t = tableau des tâches ordonnancées (données dans l'ordre de leur
 *ordonnancement) wf = workflow à placer m = mapping <tâche, hôte> effectué par
 *l'algorithme
 **/
void algo_offline_heft(Workflow &wf, SD_task_t ordoT[],
                       std::map<SD_task_t, sg_host_t> &m) {
  xbt_dynar_t dag = getDagWf(wf);
  size_t cursor;
  SD_task_t task;

  // la bande passante est la même partout (c'est comme ça qu'on a défini le
  // modèle) :
  //  double bw = 125000;
  double bw;

  //-----------------------------------------------------------
  // pre-calculi: repartition of various budgets, for example
  //.........................................................
  std::list<SD_task_t> lT;

  // vitesse moyenne :
  double speed_m = 0;
  double lat_m;

  size_t total_nhosts = sg_host_count();
  sg_host_t *hosts = sg_host_list();

  for (cursor = 0; cursor < total_nhosts; cursor++) {
    speed_m += sg_host_speed(hosts[cursor]);
  }
  speed_m = speed_m / total_nhosts;
  std::cout << "vitesse moyenne : " << speed_m << "\n";

  // bw moyenne sauvage, latence aussi (par modèle, c'est les mêmes partout)
  bw = sg_host_route_bandwidth(hosts[0], hosts[1]);
  lat_m = sg_host_route_latency(hosts[0], hosts[1]);

  std::cout << "calcul des rangs \n";
  // création de la liste des tâches triées par rang décroissant
  getTasksSortedByRank(lT, dag, speed_m, bw, lat_m);

  std::cout << "\n";

  //===========================================================
  // let's fork
  int fd[2];
  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    close(fd[0]); /* close read */
    unsigned int cursor;
    SD_task_t task;

    std::map<SD_task_t, size_t>
        nb_ord; // map associant une tâche au numéro indiquant dans quel ordre
                // elles ont été ordonnancées
    size_t nb_ordo_tmp =
        0; // compteur indiquant à quel moment la tâche a été ordonnancée

    //==================================================
    xbt_dynar_foreach(dag, cursor, task) { SD_task_watch(task, SD_DONE); }
    //==================================================
    // scheduling
    //------------
    xbt_dynar_t changed_tasks = xbt_dynar_new(sizeof(SD_task_t), NULL);
    // attribution des VMs
    for (std::list<SD_task_t>::iterator it = lT.begin(); it != lT.end(); ++it) {

      // get best host
      sg_host_t bHost = SD_task_get_best_host_heft(*it);

      // attribution
      SD_task_schedulel(*it, 1, bHost);

      m[*it] = bHost;

      // associer son numéro d'ordre d'ordonnancement
      nb_ord[*it] = nb_ordo_tmp; // vraiment pour garder le template TODO: faire
                                 // la même proprement
      // incrémenter pour avoir le numéro suivant
      nb_ordo_tmp++;

      // attribution des enfants (car on ne suit pas l'ordre d'exécution pour
      // affecter les tâches, les attributions de comm automatiques ne se feront
      // pas d'elles-mêmes. On place donc ce qu'on sait : on aura au moins de la
      // comm sur l'hôte choisi, vu que la tâche va envoyer ses résultats)

      double min_finish_time = finish_on_at(*it, bHost);
      //=======================
      // SimDag allows tasks to be executed concurrently when they can by
      // default. Yet schedulers take decisions assuming that tasks wait for
      // resource availability to start. The solution (well crude hack is to
      // keep track of the last task scheduled on a host and add a special type
      // of dependency if needed to force the sequential execution meant by the
      // scheduler. If the last scheduled task is already done, has failed or is
      // a predecessor of the current task, no need for a new dependency

      SD_task_t last_scheduled_task = sg_host_get_last_scheduled_task(bHost);
      // pour la prise en compte du nombre de coeurs, c'est là qu'il faut taper
      // (rajouter un compteur ou un tableau, ou alors traduire le nombre de
      // coeurs dans la plateforme (un gros cluster de machine mono-coeur
      if (last_scheduled_task &&
          (SD_task_get_state(last_scheduled_task) != SD_DONE) &&
          (SD_task_get_state(last_scheduled_task) != SD_FAILED) &&
          !SD_task_dependency_exists(sg_host_get_last_scheduled_task(bHost),
                                     *it)) {
        SD_task_dependency_add("resource", NULL, last_scheduled_task, *it);
      }
      sg_host_set_last_scheduled_task(bHost, *it);
      sg_host_set_available_at(bHost, min_finish_time);

      //=======================

      // simulate with update
      SD_simulate_with_update(-1.0, changed_tasks);
    }

    //==================================================
    // sending results
    //---------------------
    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number;
        ordo_number = nb_ord[task];
        if (write(fd[1], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error write ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(sg_host_get_name(m[task]));
        if (write(fd[1], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error write hostname length\n");
          std::cout << "lu : " << hostnamelen << "\n";
          exit(-1);
        }

        std::cout << "j'envoie hostnamelen : " << hostnamelen << "\n";
        while (hostnamelen != 0) {
          size_t nb_bytes =
              write(fd[1], sg_host_get_name(m[task]), hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de write dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        }
      }
      //      std::cout <<"         à envoyer : " <<sg_host_get_name(m[task])
      //      <<", pour la tâche " <<SD_task_get_name(task) <<"\n";
    }

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    deleteWf(wf);

    size_t total_nhosts = sg_host_count();
    sg_host_t *hosts = sg_host_list();

    for (cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } // end child
  else {
    // fait l'ordo
    close(fd[1]); /* close write */
    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      size_t ordo_number = 0;
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        if (read(fd[0], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error read ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(SD_task_get_name(task));
        if (read(fd[0], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error read hostname length\n");
          exit(-1);
        }

        unsigned int hostLen =
            hostnamelen; // to keep the lenght of the recieved host name
                         // somewhere and be able to put the \0 at the end of
                         // the reception
        char *nomHost = (char *)malloc(sizeof(char) * (hostnamelen + 1));
        while (hostnamelen != 0) {
          size_t nb_bytes = read(fd[0], nomHost, hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de read dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        } // end of receive name of host

        nomHost[hostLen] = '\0';
        std::cout << "j'ai lu le nom de l'hôte : " << nomHost
                  << " pour la tâche : " << SD_task_get_name(task)
                  << " d'indice : " << ordo_number << "\n";
        m[task] = sg_host_by_name(nomHost);
        ordoT[ordo_number] = task;
        free(nomHost);
      } // end of if task is executable
    }   // end of foreach

    int stat_val;
    waitpid(childpid, &stat_val, 0);
  } // end parent
} // end of func