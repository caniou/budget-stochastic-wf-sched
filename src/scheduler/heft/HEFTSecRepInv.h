#ifndef HEFT_SEC_REP_INV_H
#define HEFT_SEC_REP_INV_H

#include "hostCost.h"
#include "workflow.h"

void algo_offline_heft_sec_rep_inv(Workflow &wf, SD_task_t ordoT[],
                                   std::map<SD_task_t, sg_host_t> &m,
                                   double deadline, double budget,
                                   double cagnotte,
                                   std::map<sg_host_t, HostCost> mhc);

#endif
