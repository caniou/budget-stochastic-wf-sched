#include <assert.h>
#include <iostream>
#include <list>
#include <map>
#include <sys/wait.h>

#include "HEFT.h"
#include "HEFTBudg.h"
#include "calcBudgPerTask.h"
#include "hostCost.h"
#include "outputFiles.h"
#include "toolsSched.h"
#include "workflow.h"

sg_host_t SD_task_get_best_host_heft_budget(
    SD_task_t task, std::map<sg_host_t, HostCost> mhc,
    std::map<SD_task_t, double> budgPTsk, double cagnotte, double &timeCalc,
    double &cagnotteFin) {
  sg_host_t *hosts = sg_host_list();
  int nhosts = sg_host_count();
  double cost = 0.;
  double timeCalcTps = 0.;

  //  std::cout <<"début de get best host, cagnotte : " <<cagnotte <<"\n";
  //  double cagnotteTmp = cagnotte;
  //  volatile double cagnotteTmp = 0;
  double cagnotteTmp = cagnotte;
  // doNotOptimizeAway(cagnotteTmp);
  // cagnotteFin = 0;

  //  std::cout << "Valeur max double : " << DBL_MAX << "\n" ;
  // doNotOptimizeAway(cagnotteTmp);
  // std::cout <<"cagnotteTmp, début : " <<cagnotteTmp <<"\n";
  double min_EFT = finish_on_at(task, hosts[0]);
  double cHor = getHC_hC(mhc[hosts[0]]);
  double bw = sg_host_route_bandwidth(hosts[0], hosts[1]);

  sg_host_t best_host = hosts[0];
  // initialisation : hôte le moins cher
  for (int i = 0; i < nhosts; i++) {
    // si moins cher, devient le best host
    if (getHC_hC(mhc[hosts[i]]) <= cHor) {
      cHor = getHC_hC(mhc[hosts[i]]);
      best_host = hosts[i];
    }
  }

  // récupération du budget de la tâche
  double budgTsk = cagnotte + budgPTsk[task];
  double costTmp = 0.;
  double EFT = finish_on_at(task, best_host);

  // calcul du premier cost
  if (getHC_occTime(mhc[best_host]) <= 1e-6) {
    //    std::cout <<"nouvel host \n";
    // timeCalcTps = EFT -  SD_get_clock();
    // ici, SD_get_clock est artificiellement élevé, compte tenu de l'ordre
    // d'attribution des tâches choisi et du système choisi (on place, on fait
    // tourner ; on place, on fait tourner...) le vrai temps de calcul = temps
    // de téléchargement des données + temps de calcul
    timeCalcTps = get_amount_data_needed(task) / bw +
                  SD_task_get_amount(task) / sg_host_speed(best_host);
    // std::cout <<"get_amount_data_needed(task) : "
    // <<get_amount_data_needed(task) <<", bw : " <<bw <<", amount task : " <<
    // SD_task_get_amount(task) <<"sg_host_speed(best_host)"
    // <<sg_host_speed(best_host) <<"\n";
    //    std::cout <<"temps calc : " <<timeCalcTps <<"\n";
    // on ajoute au prix le coût lié aux temps de transferts pour les machines
    // abritant les parents
    costTmp += get_cost_data_prec_newVM(task, mhc);
    // std::cout <<"get_cost_data_prec_newVM(task, mhc) : "
    // <<get_cost_data_prec_newVM(task, mhc)<<"\n";
  } else {
    //    std::cout <<"vieil host\n";
    timeCalcTps = EFT - getHC_occTime(mhc[best_host]);
    /*
        std::cout <<"origine calc : " <<getHC_occTime(mhc[best_host]) <<"\n";
        std::cout <<"temps calc : " <<timeCalcTps <<"\n";
        std::cout <<"EFT : " <<EFT <<"\n";
    */
  } // fin calcul du premier cost
  //  std::cout <<"cost calc : " <<costTmp <<"\n";
  costTmp += timeCalcTps * getHC_hC(mhc[best_host]);
  /*
     std::cout <<"cost calc : " <<costTmp <<"\n";
     std::cout<< "cagnotteTmp : " <<cagnotteTmp <<"\n";
     std::cout<< "timeCalcTps : " <<timeCalcTps <<"\n";
     std::cout <<"getHC_hC(mhc[best_host])" <<getHC_hC(mhc[best_host]) <<"\n";
  */
  cagnotteTmp = budgTsk - costTmp;
  // doNotOptimizeAway(cagnotteTmp);
  /*
  std::cout <<" budgTsk premier placement :" <<budgTsk<<"\n";
  std::cout <<" costTmp premier placement :" <<costTmp<<"\n";
  std::cout <<" cagnotteTmp post premier placement :" <<cagnotteTmp<<"\n";
  */
  // raison pour laquelle ceci a été mis en commentaires : il y a des cas de
  // figure, couverts par l'algorithme, où l'assignation la moins chère surpasse
  // le budget alloué
  //  assert(cagnotteTmp>=0); // si ce n'est pas le cas, pas assez de budget ou
  //  bug dans le caclcul de coût

  //--------------------------------------------------------------
  // we won't test multiple times the same type of empty VM
  //--------------------------------------------------------
  int aTester = 0;
  int valVM[3] = {0, 0, 0};
  //--------------------------------------------------------------

  // on choisit l'hôte qui permet de finir le plus vite ET qui respecte le
  // budget
  for (int i = 1; i < nhosts; i++) {

    // si la machine est déjà utilisée, faire le test
    if (sg_host_get_last_scheduled_task(hosts[i]) != NULL) {
      // std::cout<< "host vieux\n";
      aTester = 1;
    }

    // si la machine est neuve, n'en prendre qu'une par type de VM
    else {

      // get the type of VM
      const char *name = sg_host_get_name(hosts[i]);
      if (strlen(name) >= 3) {
        if (name[2] == '1') {
          if (valVM[0] != 2) {
            valVM[0] = 1;
            aTester = 1;
          }
        }
        if (name[2] == '2') {
          if (valVM[1] != 2) {
            valVM[1] = 1;
            aTester = 1;
          }
        }
        if (name[2] == '3') {
          if (valVM[2] != 2) {
            valVM[2] = 1;
            aTester = 1;
          }
        }
      } // end of type of vm

    } // end of if host is new

    if (aTester == 1) {

      budgTsk = cagnotte + budgPTsk[task];
      // std::cout <<"bam cagnotte : " <<cagnotte <<"\n";

      double EFT = finish_on_at(task, hosts[i]);

      // calcul du coût induit par ce placement :
      //    timeCalcTps = EFT - sg_host_get_available_at(hosts[i]);
      if (getHC_occTime(mhc[hosts[i]]) <= 1e-6) {
        // std::cout <<"nouvel host \n";
        // timeCalcTps = EFT -  SD_get_clock();
        timeCalcTps = get_amount_data_needed(task) / bw +
                      SD_task_get_amount(task) / sg_host_speed(hosts[i]);
        cost = get_cost_data_prec_newVM(task, mhc);
      } else {
        // std::cout <<"vieil host\n";
        timeCalcTps = EFT - getHC_occTime(mhc[hosts[i]]);
        cost = 0.;
      }
      cost += timeCalcTps * getHC_hC(mhc[hosts[i]]);
      // std::cout <<"bim getHC_hC(mhc[hosts[i]]) : " <<getHC_hC(mhc[hosts[i]])
      // <<"\n"; std::cout <<"bim timeCalcTps : " <<timeCalcTps <<"\n";
      // std::cout
      // <<"bim budgTsk : " <<budgTsk <<"\n";

      //      std::cout <<"-----------avant test--------------- tâche :  "
      //      <<SD_task_get_name(task) <<", best host : EFT = " <<EFT <<" sur "
      //      <<sg_host_get_name(hosts[i]) <<" ; coût : " <<cost <<", budget : "
      //      <<budgTsk << ", cagnotte restante : " <<cagnotteTmp <<"\n";

      if ((EFT < min_EFT) && (cost <= budgTsk)) {
        min_EFT = EFT;
        best_host = hosts[i];
        timeCalc = timeCalcTps;
        cagnotteTmp = cagnotte + budgPTsk[task] - cost;
        // doNotOptimizeAway(cagnotteTmp);
        costTmp = cost;
        //	 std::cout <<"-----------gardé--------------- tâche :  "
        //<<SD_task_get_name(task) <<", best host : EFT = " <<EFT <<" sur "
        //<<sg_host_get_name(hosts[i]) <<" ; coût : " <<cost <<", budget : "
        //<<budgTsk << ", cagnotte restante : " <<cagnotteTmp <<"\n";

        // remise à zéro des variables de vérif
        int iValVM;
        for (iValVM = 0; iValVM < 3; iValVM++) {
          if (valVM[iValVM] == 1) {
            valVM[iValVM] = 0;
            // std::cout<< "host neuf\n";
          }
        }
        aTester = 0;

      } else {
        // indiquer que ce type de VM neuve n'améliore pas le résultat
        int iValVM;
        for (iValVM = 0; iValVM < 3; iValVM++) {
          if (valVM[iValVM] == 1) {
            valVM[iValVM] = 2;
          }
        }
        aTester = 0;
      }
    }
  }

  //  std::cout <<"-----------gardé--------------- tâche :  "
  //  <<SD_task_get_name(task) <<", best host : EFT = " <<EFT <<" sur "
  //  <<sg_host_get_name(best_host) <<" ; coût : " <<costTmp <<", budget perso :
  //  " <<budgPTsk[task] << ", cagnotte restante : " <<cagnotteTmp <<"\n";

  //  std::cout <<"suspect : " <<"cagnotteTmp = " <<cagnotteTmp <<"\n";

  cagnotteFin = cagnotteTmp;
  //  std::cout <<"suspect : " <<"cagnotteFin = " <<cagnotteFin <<"\n";
  //    berk = cagnotteTmp;
  // doNotOptimizeAway(cagnotteFin);

  // optiChut2((volatile double*)&cagnotteFin, cagnotteTmp);
  // doNotOptimizeAway(cagnotteTmp);

  //    optiChut( *(volatile double*) &cagnotteFin, cagnotteTmp);
  //  std::cout <<"suspect : " <<"cagnotteTmp + cagnotteFin = "
  //  <<cagnotteTmp+cagnotteFin <<"\n";
  //#pragma GCC push_options
  //#pragma GCC optimize ("O0")

  // beurk =static_cast<double>( cagnotteTmp+cagnotteFin);
  //  cagnotteFin = cagnotteTmp+cagnotteFin;
  // cagnotteFin = beurk;
  //#pragma GCC pop_options
  // std::cout <<"suspect : " <<"cagnotteFin = " <<cagnotteFin <<"\n";

  xbt_free(hosts);
  return best_host;
}
//#pragma GCC pop_options

//========================================================================================
/**
 * Fait l'ordonnancement et retourne le mapping tâche -> hôte + l'odre
 *d'attribution (vu qu'il remet sans cesse en question l'ordre d'attribution et
 *que seul
 *
 * ordo_t = tableau des tâches ordonnancées (données dans l'ordre de leur
 *ordonnancement) wf = workflow à placer m = mapping <tâche, hôte> effectué par
 *l'algorithme
 **/
void algo_offline_heft_budget(Workflow &wf, SD_task_t ordoT[],
                              std::map<SD_task_t, sg_host_t> &m,
                              double deadline, double budget, double cagnotte,
                              std::map<sg_host_t, HostCost> mhc) {
  xbt_dynar_t dag = getDagWf(wf);
  size_t cursor;
  SD_task_t task;
  worstValues(wf);
  // la bande passante est la même partout (c'est comme ça qu'on a défini le
  // modèle) :
  //  double bw = 125000;
  double bw;

  //-----------------------------------------------------------
  // pre-calculi: repartition of various budgets, for example
  //.........................................................
  std::list<SD_task_t> lT;

  // vitesse moyenne :
  double lat_m;

  size_t total_nhosts = sg_host_count();
  sg_host_t *hosts = sg_host_list();

  // bw moyenne sauvage, latence aussi (par modèle, c'est les mêmes partout)
  bw = sg_host_route_bandwidth(hosts[0], hosts[1]);
  lat_m = sg_host_route_latency(hosts[0], hosts[1]);

  //==================================================
  //-----------------------------------------------------------
  // pre-calculi: repartition of various budgets, for example
  //.........................................................

  // calcul du nombre de VMs max :
  int nbVMsMax = nbMaxVMn(dag);

  //  std::cout << "----------------------- nb VMs max : " <<nbVMsMax <<"\n";

  // coûts et vitesse moyens :
  double horCost_m = 0;
  double speed_m = 0;
  double iniCost_m = 0;

  for (cursor = 0; cursor < total_nhosts; cursor++) {
    HostCost testHc = mhc[hosts[cursor]];
    horCost_m += getHC_hC(testHc);
    speed_m += sg_host_speed(hosts[cursor]);
    iniCost_m += getHC_iniC(testHc);
  }

  horCost_m = horCost_m / total_nhosts;
  speed_m = speed_m / total_nhosts;
  iniCost_m = iniCost_m / total_nhosts;

  //  std::cout << "vitesse moyenne : " <<speed_m <<"\n";
  //  std::cout << "coût horaire moyen : " <<horCost_m <<"\n";

  // réévaluation du budget
  //  std::cout <<"budget initial : " << budget <<"\n";
  double updateB = budget - (nbVMsMax * iniCost_m + cagnotte);
  //  std::cout << "budget à jour : " <<updateB <<"\n";
  double budgetIni = budget;
  budget = updateB;

  // calcul du budget par tâche
  std::map<SD_task_t, double> budgPTsk;
  calcBudgPTsk(wf, updateB, bw, speed_m, budgPTsk);
  int i = 0;

  double timeCalc = 0;
  double cagnotteFin = 0;

  // création de la liste des tâches triées par rang décroissant
  getTasksSortedByRank(lT, dag, speed_m, bw, lat_m);

  //===========================================================
  // let's fork
  int fd[2];
  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    close(fd[0]); /* close read */
    unsigned int cursor;
    SD_task_t task;

    std::map<SD_task_t, size_t>
        nb_ord; // map associant une tâche au numéro indiquant dans quel ordre
                // elles ont été ordonnancées
    size_t nb_ordo_tmp =
        0; // compteur indiquant à quel moment la tâche a été ordonnancée

    //==================================================
    xbt_dynar_foreach(dag, cursor, task) { SD_task_watch(task, SD_DONE); }
    //==================================================
    // scheduling
    //------------
    xbt_dynar_t changed_tasks = xbt_dynar_new(sizeof(SD_task_t), NULL);
    // attribution des VMs
    for (std::list<SD_task_t>::iterator it = lT.begin(); it != lT.end(); ++it) {

      // get best host
      sg_host_t bHost = SD_task_get_best_host_heft_budget(
          *it, mhc, budgPTsk, cagnotte, timeCalc, cagnotteFin);

      // attribution
      SD_task_schedulel(*it, 1, bHost);

      m[*it] = bHost;
      // associer son numéro d'ordre d'ordonnancement
      nb_ord[*it] = nb_ordo_tmp; // vraiment pour garder le template TODO: faire
                                 // la même proprement
      // incrémenter pour avoir le numéro suivant
      nb_ordo_tmp++;

      cagnotte = cagnotteFin;

      double min_finish_time = finish_on_at(*it, bHost);
      //=======================
      // SimDag allows tasks to be executed concurrently when they can by
      // default. Yet schedulers take decisions assuming that tasks wait for
      // resource availability to start. The solution (well crude hack is to
      // keep track of the last task scheduled on a host and add a special type
      // of dependency if needed to force the sequential execution meant by the
      // scheduler. If the last schxeduled task is already done, has failed or
      // is a predecessor of the current task, no need for a new dependency

      SD_task_t last_scheduled_task = sg_host_get_last_scheduled_task(bHost);
      // pour la prise en compte du nombre de coeurs, c'est là qu'il faut taper
      // (rajouter un compteur ou un tableau, ou alors traduire le nombre de
      // coeurs dans la plateforme (un gros cluster de machine mono-coeur
      if (last_scheduled_task &&
          (SD_task_get_state(last_scheduled_task) != SD_DONE) &&
          (SD_task_get_state(last_scheduled_task) != SD_FAILED) &&
          !SD_task_dependency_exists(sg_host_get_last_scheduled_task(bHost),
                                     *it)) {
        SD_task_dependency_add("resource", NULL, last_scheduled_task, *it);
      }
      sg_host_set_last_scheduled_task(bHost, *it);
      sg_host_set_available_at(bHost, min_finish_time);

      //=======================

      // simulate with update
      SD_simulate_with_update(-1.0, changed_tasks);
    }

    //===================================================================
    const char nomAlgo[] = "heftBudg";
    credoFile(nomAlgo, budgetIni, cagnotte);
    //===================================================================

    //==================================================
    // sending results
    //---------------------
    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number;
        ordo_number = nb_ord[task];
        if (write(fd[1], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error write ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(sg_host_get_name(m[task]));
        if (write(fd[1], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error write hostname length\n");
          exit(-1);
        }

        while (hostnamelen != 0) {
          size_t nb_bytes =
              write(fd[1], sg_host_get_name(m[task]), hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de write dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        }
      }
    }

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    deleteWf(wf);

    size_t total_nhosts = sg_host_count();
    // sg_host_t *hosts = sg_host_list();

    for (cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } // end child
  else {
    // fait l'ordo
    close(fd[1]); /* close write */
    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number = 0;
        if (read(fd[0], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error read ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(SD_task_get_name(task));
        if (read(fd[0], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error read hostname length\n");
          exit(-1);
        }

        unsigned int hostLen =
            hostnamelen; // to keep the lenght of the recieved host name
                         // somewhere and be able to put the \0 at the end of
                         // the reception
        char *nomHost = (char *)malloc(sizeof(char) * (hostnamelen + 1));
        while (hostnamelen != 0) {
          size_t nb_bytes = read(fd[0], nomHost, hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de read dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        } // end of receive name of host

        nomHost[hostLen] = '\0';
        // std::cout <<"j'ai lu le nom de l'hôte : " <<nomHost <<" pour la tâche
        // : " <<SD_task_get_name(task) <<" d'indice : " <<ordo_number  <<"\n";
        m[task] = sg_host_by_name(nomHost);
        ordoT[ordo_number] = task;
        free(nomHost);
      } // end of if task is executable
    }   // end of foreach

    int stat_val;
    waitpid(childpid, &stat_val, 0);
  } // end parent
  xbt_free(hosts);
} // end of func