#include <iostream>
#include <list>
#include <map>
#include <sys/wait.h>

#include "HEFT.h"
#include "HEFTBudg.h"
#include "calcBudgPerTask.h"
#include "hostCost.h"
#include "toolsSched.h"
#include "workflow.h"

//========================================================================================
/**
 * Fait l'ordonnancement et retourne le mapping tâche -> hôte + l'odre
 *d'attribution (vu qu'il remet sans cesse en question l'ordre d'attribution et
 *que seul
 *
 * ordo_t = tableau des tâches ordonnancées (données dans l'ordre de leur
 *ordonnancement) wf = workflow à placer m = mapping <tâche, hôte> effectué par
 *l'algorithme
 **/
void algo_offline_heft_sec_rep(Workflow &wf, SD_task_t ordoT[],
                               std::map<SD_task_t, sg_host_t> &m,
                               double deadline, double budget, double cagnotte,
                               std::map<sg_host_t, HostCost> mhc) {

  //  std::cout<<"algo offline heft à seconde répartition\n";

  // we are pessimistic, we use the worst values:
  worstValues(wf);

  // first scheduling : heft with budget
  algo_offline_heft_budget(wf, ordoT, m, deadline, budget, cagnotte, mhc);

  // get the amount of not used money
  double resMakespan = -1;
  double resCost = -1;
  getMakespanCostSched(wf, ordoT, m, deadline, budget, cagnotte, mhc,
                       resMakespan, resCost);

  // std::cout <<"ICI algoheftsecrep, et je lis " <<resMakespan <<" secondes et
  // " <<resCost <<" sous.\n";

  // reattributing the not used budget for the worst case scenario
  double budgLeftovers = budget - resCost;

  // for each task, check if the result is not better with another VM, starting
  // with the last tasks it's a HEFT : I'll use the pre-calculated order

  size_t nbTask = get_nb_calc_tasks(getDagWf(wf));
  size_t nhosts = sg_host_count();
  std::cout << "nbTask =" << nbTask << "\n";

  double bestMakespan = resMakespan;
  // double costBestMakespan = resCost;
  sg_host_t *hosts = sg_host_list();

  // construct the set of used VMs
  std::set<sg_host_t> usedVM;

  for (size_t i; i < nbTask; i++) {

    // std::cout<<"BOUH : " <<SD_task_get_name(ordoT[i]) <<"\n";
    if ((strcmp(SD_task_get_name(ordoT[i]), "end") != 0) &&
        (strcmp(SD_task_get_name(ordoT[i]), "root") != 0)) {
      // we don't touch end and root, they are not real
      // make a copy of the original assignment
      std::map<SD_task_t, sg_host_t> mCopy(m);
      usedVM.empty();

      for (std::map<SD_task_t, sg_host_t>::iterator it = mCopy.begin();
           it != mCopy.end(); ++it) {
        // std::cout <<"tâche : " <<SD_task_get_name(it->first) <<"\n";
        // std::cout <<"hôte : " <<sg_host_get_name(it->second) <<"\n";
        usedVM.insert(it->second);
      }
      std::cout << "nb VM utilisées : " << usedVM.size() << "\n";

      int valVM[3] = {0, 0, 0};

      for (size_t j = 0; j < nhosts; j++) {
        int aTester = 0;

        // if the host is not used at all yet
        if (usedVM.find(hosts[j]) == usedVM.end()) {
          // std::cout <<"host tout neuf\n";

          // get the type of VM
          const char *name = sg_host_get_name(hosts[j]);
          if (strlen(name) >= 3) {
            if (name[2] == '1') {
              if (valVM[0] != 2) {
                valVM[0] = 1;
                aTester = 1;
              }
            }
            if (name[2] == '2') {
              if (valVM[1] != 2) {
                valVM[1] = 1;
                aTester = 1;
              }
            }
            if (name[2] == '3') {
              if (valVM[2] != 2) {
                valVM[2] = 1;
                aTester = 1;
              }
            }
          } // end of type of vm

        } // end of if host is new

        // if the host is one of the used VM
        else {
          // std::cout <<"host tout vieux\n";
          aTester = 1;
        }

        if (aTester == 1) {
          //===========================================================
          // test de VM
          //-------------------------

          std::cout << "hôte testé : " << sg_host_get_name(hosts[j]) << "\n";
          // try others VMs to see if you can do better
          mCopy[ordoT[i]] = hosts[j];
          getMakespanCostSched(wf, ordoT, mCopy, deadline, budget, cagnotte,
                               mhc, resMakespan, resCost);
          //	std::cout<<"resInter : \n    tâche = "
          //<<SD_task_get_name(ordoT[i]) <<"\n    hôte = "
          //<<sg_host_get_name(hosts[j]) <<"\n      --> makespan = "
          //<<resMakespan <<"\n    --> cost = " <<resCost <<"\n";

          // std::cout <<"   bestMakespan : " <<bestMakespan  <<"\n";
          // std::cout <<"   costBestMakespan : " << costBestMakespan <<"\n";
          // std::cout <<"   resMakespan : " <<resMakespan  <<"\n";
          // std::cout <<"   resCost : " << resCost <<"\n\n";

          // std::cout <<"   hôte : " << sg_host_get_name(hosts[j]) <<" j : "
          // <<j <<"tâche = " <<SD_task_get_name(ordoT[i]) <<" i : " <<i <<"\n";

          // if the makespan is better and if the budget permise it,
          // replace the old one by the new one
          if ((resMakespan < bestMakespan) && (resCost < budget)) {
            // m.clear();
            /*
            std::cout <<"changement !\n";
            std::cout <<"   resMakespan : " <<resMakespan  <<"\n";
            std::cout <<"   bestMakespan : " <<bestMakespan <<"\n";
            std::cout <<"   costBestMakespan : " <<costBestMakespan <<"\n";
            */
            m = mCopy;
            // costBestMakespan = resCost;
            bestMakespan = resMakespan;

            //	    std::cout <<"   nouveau bestMakespan : " <<bestMakespan
            //<<"\n"; 	    std::cout <<"   nouveau costBestMakespan : "
            //<<costBestMakespan <<"\n\n";

            // remise à zéro des variables de vérif
            int iValVM;
            for (iValVM = 0; iValVM < 3; iValVM++) {
              if (valVM[iValVM] == 1) {
                valVM[iValVM] = 0;
              }
            }
            aTester = 0;
          }
          //============================================================

          // passage à 2 de la variable pour VM neuve
          int iValVM;
          for (iValVM = 0; iValVM < 3; iValVM++) {
            if (valVM[iValVM] == 1) {
              valVM[iValVM] = 2;
            }
          }
          aTester = 0;
        }
      }
      //      std::cout <<"\n\n";

      mCopy.clear();
    }
    std::cout << "##################################\n";
  }
}