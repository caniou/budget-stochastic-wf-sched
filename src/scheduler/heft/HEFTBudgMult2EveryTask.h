#ifndef HEFT_BUDG_MULT_2_EVERY_TASK_H
#define HEFT_BUDG_MULT_2_EVERY_TASK_H

#include "hostCost.h"
#include "workflow.h"

void algo_offline_heft_budg_mult2_every_task(Workflow &wf, SD_task_t ordoT[],
                                             std::map<SD_task_t, sg_host_t> &m,
                                             double deadline, double budget,
                                             double cagnotte,
                                             std::map<sg_host_t, HostCost> mhc);

#endif