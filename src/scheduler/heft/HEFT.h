#ifndef HEFT_H
#define HEFT_H

// explosion des limites sup des doubles --> voir si gmp n'aurait pas un type de
// remplacement

#include "hostCost.h"
#include "workflow.h"

double dlTimeTaskOnHost(SD_task_t task, sg_host_t host,
                        std::map<sg_host_t, HostCost> mhc);

sg_host_t SD_task_get_best_host_heft(SD_task_t task);

double rank(SD_task_t s, xbt_dynar_t dag, double mSpeed, double mBw,
            double mLat);

void getTasksSortedByRank(std::list<SD_task_t> &lT, xbt_dynar_t dag,
                          double mSpeed, double mBw, double mLat);
// TODO : remove
void getTasksSortedByRankV2(std::map<SD_task_t, double> &tR,
                            std::list<SD_task_t> &lT, xbt_dynar_t dag,
                            double mSpeed, double mBw, double mLat);

void algo_offline_heft(Workflow &wf, SD_task_t ordoT[],
                       std::map<SD_task_t, sg_host_t> &m);
#endif