#include <iostream>
#include <list>
#include <map>
#include <sys/wait.h>

#include "HEFT.h"
#include "calcBudgPerTask.h"
#include "hostCost.h"
#include "outputFiles.h"
#include "toolsSched.h"
#include "workflow.h"

//==========================================
// outils
//-----------

/**
 * Return the time at which the given task is expected to be completed on the
 *given host
 **/
double finish_on_at_CG(SD_task_t task, sg_host_t host) {
  //
  double result = 0.;
  unsigned int i;
  double data_available = 0.;
  double redist_time = 0;
  double last_data_available;

  xbt_dynar_t parents = SD_task_get_parents(task);
  sg_host_t *hosts = sg_host_list();

  // si la tâche a des prédécesseurs, son heure de début (et de fin) va en
  // dépendre
  if (!xbt_dynar_is_empty(parents)) {
    // compute last_data_available

    SD_task_t parent;
    last_data_available = -1.0;
    xbt_dynar_foreach(parents, i, parent) {
      // normal case : the parent is a comm
      if (SD_task_get_kind(parent) == SD_TASK_COMM_E2E) {
        sg_host_t *parent_host = SD_task_get_workstation_list(parent);
        // Estimate the redistribution time from this parent
        if (SD_task_get_amount(parent) <= 1e-6) {
          redist_time = 0.;
        } else {
          // si la tâche a déjà été placée et exécutée, on peut récupérer les
          // iformations concernant son hôte
          if ((SD_task_get_state(parent) != SD_NOT_SCHEDULED) &&
              (SD_task_get_state(parent) != SD_SCHEDULABLE)) {
            redist_time = sg_host_route_latency(parent_host[0], host) +
                          SD_task_get_amount(parent) /
                              sg_host_route_bandwidth(parent_host[0], host);
          } else {
            double lat = 0.;
            double bw = 0.;
            if (SD_task_get_state(parent) != SD_NOT_SCHEDULED) {
              // si déjà calculée, ajouter la vraie valeur
              lat = sg_host_route_latency(parent_host[0], host);
              bw = sg_host_route_bandwidth(parent_host[0], host);
            } else {
              // sinon, calculer une estimation (récup des données pour
              // estimation)
              lat = sg_host_route_latency(hosts[0], hosts[1]);
              bw = sg_host_route_bandwidth(hosts[0], hosts[1]);
              // std::cout <<" parent : " <<SD_task_get_name(parent)<<"\n";
            }
            redist_time = lat + SD_task_get_amount(parent) / bw;
          }
        }
        redist_time = 0.;
        // les données des "tâches" de communication sont disponibles une fois
        // les données totalement envoyées
        data_available = SD_task_get_start_time(parent) + redist_time;
      }

      // no transfer, control dependency -- cas où la tâche précédente est une
      // tâche exécutable
      if (SD_task_get_kind(parent) == SD_TASK_COMP_SEQ) {
        data_available = SD_task_get_finish_time(parent);
      }

      // mise à jour du moment le plus tardif pour lequel les données d'une
      // tâche seront disponibles
      if (last_data_available < data_available)
        last_data_available = data_available;
    } // fin du foreach

    xbt_dynar_free_container(&parents);

    result = MAX(sg_host_get_available_at(host), last_data_available) +
             SD_task_get_amount(task) / sg_host_speed(host);
  }
  // si la tâche n'a pas d'ascendent, rien à attendre
  else {
    xbt_dynar_free_container(&parents);

    result = sg_host_get_available_at(host) +
             SD_task_get_amount(task) / sg_host_speed(host);
  }
  return result;
}

sg_host_t
SD_task_get_best_host_heft_critical_greedy(SD_task_t task,
                                           std::map<sg_host_t, HostCost> mhc,
                                           double &timeCalc, double gbl) {
  sg_host_t *hosts = sg_host_list();
  int nhosts = sg_host_count();
  double cost = 0;
  double timeCalcTps = 0;

  double min_EFT = finish_on_at_CG(task, hosts[1]);
  double cHor = getHC_hC(mhc[hosts[1]]);
  double bw = sg_host_route_bandwidth(hosts[1], hosts[2]);

  sg_host_t Vmax = hosts[1];
  double cHorMax = getHC_hC(mhc[hosts[1]]);
  sg_host_t best_host = hosts[1];
  // initialisation : hôte le moins cher
  for (int i = 0; i < nhosts; i++) {
    if (strcmp(sg_host_get_name(hosts[i]), "S") != 0) {
      // si moins cher, devient le best host
      if (getHC_hC(mhc[hosts[i]]) <= cHor) {
        cHor = getHC_hC(mhc[hosts[i]]);
        best_host = hosts[i];
      }

      // hôte le plus cher
      if (getHC_hC(mhc[hosts[i]]) >= cHorMax) {
        cHorMax = getHC_hC(mhc[hosts[i]]);
        Vmax = hosts[i];
      }
    }
  } // fin de sélection d'hôtes

  double costTmp = 0.;
  double EFT = finish_on_at_CG(task, best_host);

  get_cost_data_prec_newVM(task, mhc);

  // calcul de Cmin
  double Cmin = (SD_task_get_amount(task) / sg_host_speed(best_host)) *
                getHC_hC(mhc[best_host]);

  // calcul de Cmax
  double Cmax =
      (SD_task_get_amount(task) / sg_host_speed(Vmax)) * getHC_hC(mhc[Vmax]);

  double targetCost = Cmin + (Cmax - Cmin) * gbl;

  // std::cout << "get best host, gbl : " <<gbl <<"\n";
  // std::cout << "targetcost : " <<targetCost <<"\n";

  // calcul du premier cost
  if (getHC_occTime(mhc[best_host]) <= 1e-6) {
    //    std::cout <<"nouvel host \n";
    // timeCalcTps = EFT -  SD_get_clock();
    // ici, SD_get_clock est artificiellement élevé, compte tenu de l'ordre
    // d'attribution des tâches choisi et du système choisi (on place, on fait
    // tourner ; on place, on fait tourner...) le vrai temps de calcul = temps
    // de téléchargement des données + temps de calcul
    //    timeCalcTps = get_amount_data_needed(task)/bw +
    //    SD_task_get_amount(task)/sg_host_speed(best_host);
    timeCalcTps = SD_task_get_amount(task) / sg_host_speed(best_host);
    // on ajoute au prix le coût lié aux temps de transferts pour les machines
    // abritant les parents
    // costTmp+= get_cost_data_prec_newVM(task, mhc);
  } else {
    //    std::cout <<"vieil host\n";
    timeCalcTps = EFT - getHC_occTime(mhc[best_host]);
    //    std::cout <<"origine calc : " <<getHC_occTime(mhc[best_host]) <<"\n";
    //    std::cout <<"temps calc : " <<timeCalcTps <<"\n";

  } // fin calcul du premier cost
  costTmp += timeCalcTps * getHC_hC(mhc[best_host]);

  // récupération du budget de la tâche
  // double budgTsk = budgPTsk[task];

  // première diff
  double minDiff;
  targetCost = Cmin + (Cmax - Cmin) * gbl;
  if (costTmp < targetCost) {
    minDiff = targetCost - costTmp;

  } else {
    minDiff = costTmp - targetCost;
  }

  // std::cout << "minDiff : " <<minDiff <<"\n";

  //--------------------------------------------------------------
  // we won't test multiple times the same type of empty VM
  //--------------------------------------------------------
  int aTester = 0;
  int valVM[3] = {0, 0, 0};
  //--------------------------------------------------------------

  // on choisit l'hôte qui permet de finir le plus vite ET qui respecte le
  // budget
  for (int i = 1; i < nhosts; i++) {

    // si la machine est déjà utilisée, faire le test
    if (sg_host_get_last_scheduled_task(hosts[i]) != NULL) {
      // std::cout<< "host vieux\n";
      aTester = 1;
    }

    // si la machine est neuve, n'en prendre qu'une par type de VM
    else {

      // get the type of VM
      const char *name = sg_host_get_name(hosts[i]);
      if (strlen(name) >= 3) {
        if (name[2] == '1') {
          if (valVM[0] != 2) {
            valVM[0] = 1;
            aTester = 1;
          }
        }
        if (name[2] == '2') {
          if (valVM[1] != 2) {
            valVM[1] = 1;
            aTester = 1;
          }
        }
        if (name[2] == '3') {
          if (valVM[2] != 2) {
            valVM[2] = 1;
            aTester = 1;
          }
        }
      } // end of type of vm

    } // end of if host is new

    if (aTester == 1) {

      double EFT = finish_on_at_CG(task, hosts[i]);

      // calcul du coût induit par ce placement :
      //    timeCalcTps = EFT - sg_host_get_available_at(hosts[i]);
      if (getHC_occTime(mhc[hosts[i]]) <= 1e-6) {
        // std::cout <<"nouvel host \n";
        // timeCalcTps = EFT -  SD_get_clock();
        // timeCalcTps = get_amount_data_needed(task)/bw +
        // SD_task_get_amount(task)/sg_host_speed(hosts[i]);
        timeCalcTps = SD_task_get_amount(task) / sg_host_speed(hosts[i]);
        // cost= get_cost_data_prec_newVM(task, mhc);
      } else {
        // std::cout <<"vieil host\n";
        timeCalcTps = EFT - getHC_occTime(mhc[hosts[i]]);
        cost = 0.;
      }
      cost += timeCalcTps * getHC_hC(mhc[hosts[i]]);

      double diffCost;
      targetCost = Cmin + (Cmax - Cmin) * gbl;
      if (cost < targetCost) {
        diffCost = targetCost - cost;
        // std::cout << "targetCost louche : " <<targetCost <<"\n";
        // std::cout <<"cost : "<<cost <<"\n";
      } else {
        diffCost = cost - targetCost;
        // std::cout << "targetCost louche : " <<targetCost <<"\n";
      }
      // std::cout << "diffCost : " <<diffCost <<"\n";
      // std::cout << "costTmp : " <<costTmp <<"\n";

      //      if ((EFT < min_EFT) && (cost <= budgTsk)) {
      //      if((minDiff > diffCost)&&(cost <targetCost)){
      if (minDiff > diffCost) {
        //	min_EFT = EFT;
        minDiff = diffCost;
        best_host = hosts[i];
        timeCalc = timeCalcTps;

        costTmp = cost;
        // std::cout <<"-----------gardé--------------- tâche :  "
        // <<SD_task_get_name(task) <<", best host : EFT = " <<EFT <<" sur "
        // <<sg_host_get_name(hosts[i]) <<" ; coût : " <<cost <<", budget : "
        // <<budgTsk << ", cagnotte restante : " <<cagnotteTmp <<"\n";

        // remise à zéro des variables de vérif
        int iValVM;
        for (iValVM = 0; iValVM < 3; iValVM++) {
          if (valVM[iValVM] == 1) {
            valVM[iValVM] = 0;
            // std::cout<< "host neuf\n";
          }
        }
        aTester = 0;

      } else {
        // indiquer que ce type de VM neuve n'améliore pas le résultat
        int iValVM;
        for (iValVM = 0; iValVM < 3; iValVM++) {
          if (valVM[iValVM] == 1) {
            valVM[iValVM] = 2;
          }
        }
        aTester = 0;
      }
    }
  }

  //  std::cout <<"-----------gardé--------------- tâche :  "
  //  <<SD_task_get_name(task) <<", best host : EFT = " <<EFT <<" sur "
  //  <<sg_host_get_name(best_host) <<" ; coût : " <<costTmp <<", budget perso :
  //  " <<budgPTsk[task] << ", cagnotte restante : " <<cagnotteTmp <<"\n";

  xbt_free(hosts);
  return best_host;
}

//========================================================================================
/**
 * Fait l'ordonnancement et retourne le mapping tâche -> hôte + l'odre
 *d'attribution (vu qu'il remet sans cesse en question l'ordre d'attribution et
 *que seul
 *
 * ordo_t = tableau des tâches ordonnancées (données dans l'ordre de leur
 *ordonnancement) wf = workflow à placer m = mapping <tâche, hôte> effectué par
 *l'algorithme
 **/
void algo_offline_heft_critical_greedy(Workflow &wf, SD_task_t ordoT[],
                                       std::map<SD_task_t, sg_host_t> &m,
                                       double deadline, double budget,
                                       double cagnotte,
                                       std::map<sg_host_t, HostCost> mhc) {
  xbt_dynar_t dag = getDagWf(wf);
  size_t cursor;
  SD_task_t task;

  // la bande passante est la même partout (c'est comme ça qu'on a défini le
  // modèle) :
  //  double bw = 125000;
  double bw;

  //-----------------------------------------------------------
  // pre-calculi: repartition of various budgets, for example
  //.........................................................
  std::list<SD_task_t> lT;

  // vitesse moyenne :
  double lat_m;

  size_t total_nhosts = sg_host_count();
  sg_host_t *hosts = sg_host_list();

  double horCost_m = 0;
  double speed_m = 0;
  double iniCost_m = 0;

  for (cursor = 0; cursor < total_nhosts; cursor++) {
    HostCost testHc = mhc[hosts[cursor]];
    horCost_m += getHC_hC(testHc);
    speed_m += sg_host_speed(hosts[cursor]);
    iniCost_m += getHC_iniC(testHc);
  }

  horCost_m = horCost_m / total_nhosts;
  speed_m = speed_m / total_nhosts;
  iniCost_m = iniCost_m / total_nhosts;

  // bw moyenne sauvage, latence aussi (par modèle, c'est les mêmes partout)
  bw = sg_host_route_bandwidth(hosts[0], hosts[1]);
  lat_m = sg_host_route_latency(hosts[0], hosts[1]);

  //==================================================
  //-----------------------------------------------------------
  // pre-calculi: repartition of various budgets, for example
  //.........................................................
  // recherche de l'hôte le moins cher
  sg_host_t best_host = hosts[1];
  double cHor = getHC_hC(mhc[hosts[1]]);
  // initialisation : hôte le moins cher
  for (size_t i = 0; i < total_nhosts; i++) {
    // si moins cher, devient le best host
    if (strcmp(sg_host_get_name(hosts[i]), "S") != 0) {
      if (getHC_hC(mhc[hosts[i]]) <= cHor) {
        cHor = getHC_hC(mhc[hosts[i]]);
        best_host = hosts[i];
      }
    }
  } // fin recherche du moins cher

  // calcul de cmin = somme des coûts de calcul des tâches sur les machines les
  // moins coûteuses
  double Cmin = 0;
  int curs;
  xbt_dynar_foreach(dag, curs, task) {
    if (isTaskComput(task)) {
      Cmin += (SD_task_get_amount(task) / sg_host_speed(best_host)) *
              getHC_hC(mhc[best_host]);
    }
  } // fin calc Cmin

  // calcul de cmax = somme des coûts de calcul des tâches sur les machines les
  // plus coûteuses
  best_host = hosts[1];
  cHor = getHC_hC(mhc[hosts[1]]);
  // recherche de la machine la plus coûteuse
  for (size_t i = 0; i < total_nhosts; i++) {
    // si moins cher, devient le best host
    if (strcmp(sg_host_get_name(hosts[i]), "S") != 0) {
      if (getHC_hC(mhc[hosts[i]]) >= cHor) {
        cHor = getHC_hC(mhc[hosts[i]]);
        best_host = hosts[i];
      }
    }
  } // fin recherche du plus cher

  double Cmax = 0;
  xbt_dynar_foreach(dag, curs, task) {
    if (isTaskComput(task)) {
      Cmax += (SD_task_get_amount(task) / sg_host_speed(best_host)) *
              getHC_hC(mhc[best_host]);
    }
  } // fin calc Cmax

  std::cout << "Cmax = " << Cmax << "\n";
  std::cout << "budget = " << budget << "\n";

  if (budget >= Cmax) {
    // faire un heft
    std::cout << "je fais un heft\n";
    algo_offline_heft(wf, ordoT, m);
  }

  else {

    // calcul de gbl
    double gbl = (budget - Cmin) / (Cmax - Cmin);
    std::cout << " budget : " << budget << "\n";
    std::cout << " Cmin : " << Cmin << "\n";
    std::cout << " Cmax : " << Cmax << "\n";
    std::cout << "main, gbl : " << gbl << "\n";

    double timeCalc = 0;
    double cagnotteFin = 0;

    // création de la liste des tâches triées par rang décroissant
    getTasksSortedByRank(lT, dag, speed_m, bw, lat_m);

    //===========================================================
    // let's fork
    int fd[2];
    pid_t childpid;

    if (pipe(fd) == -1) {
      fprintf(stderr, "pipe: %s", strerror(errno));
      exit(EXIT_FAILURE);
    }
    if ((childpid = fork()) == -1) {
      perror("fork");
      exit(1);
    }
    if (childpid == 0) {
      close(fd[0]); /* close read */
      unsigned int cursor;
      SD_task_t task;

      std::map<SD_task_t, size_t>
          nb_ord; // map associant une tâche au numéro indiquant dans quel ordre
                  // elles ont été ordonnancées
      size_t nb_ordo_tmp =
          0; // compteur indiquant à quel moment la tâche a été ordonnancée

      //==================================================
      xbt_dynar_foreach(dag, cursor, task) { SD_task_watch(task, SD_DONE); }
      //==================================================
      // scheduling
      //------------
      xbt_dynar_t changed_tasks = xbt_dynar_new(sizeof(SD_task_t), NULL);
      // attribution des VMs
      for (std::list<SD_task_t>::iterator it = lT.begin(); it != lT.end();
           ++it) {
        sg_host_t bHost;
        //      std::cout <<"youh "<<SD_task_get_name(*it)  <<"\n\n";

        // get best host
        // bHost = SD_task_get_best_host_heft_budget(*it);
        // bHost = SD_task_get_best_host_heft_budget(SD_task_t task,
        // std::map<sg_host_t, HostCost> mhc, std::map<SD_task_t, double>
        // budgPTsk, double cagnotte, double &timeCalc, double &cagnotteFin);
        // bHost = SD_task_get_best_host_heft_critical_greedy(*it, mhc,
        // budgPTsk, cagnotte, timeCalc, cagnotteFin);

        /*
        std::cout <<" budget avant getbesthost : " <<budget <<"\n";
        std::cout <<" Cmin avant getbesthost : " <<Cmin <<"\n";
        std::cout <<" Cmax avant getbesthost : " <<Cmax <<"\n";
        std::cout << "main, gbl avant getbesthost : " <<gbl <<"\n";
        */
        bHost =
            SD_task_get_best_host_heft_critical_greedy(*it, mhc, timeCalc, gbl);
        //      std::cout << "hôte : " << sg_host_get_name(bHost) <<"\n";

        // attribution
        SD_task_schedulel(*it, 1, bHost);

        // attribution des enfants (car on ne suit pas l'ordre d'exécution pour
        // affecter les tâches, les attributions de comm automatiques ne se
        // feront pas d'elles-mêmes. On place donc ce qu'on sait : on aura au
        // moins de la comm sur l'hôte choisi, vu que la tâche va envoyer ses
        // résultats)
        xbt_dynar_t children = SD_task_get_children(task);
        if (!xbt_dynar_is_empty(children)) {
          SD_task_t child;
          unsigned int i;
          xbt_dynar_foreach(children, i, child) {
            if (SD_task_get_kind(child) == SD_TASK_COMM_E2E) {
              if (SD_task_get_state(child) == SD_NOT_SCHEDULED) {
                SD_task_schedulel(child, 1, bHost);
              }
            }
          }
        }

        m[*it] = bHost;
        // associer son numéro d'ordre d'ordonnancement
        nb_ord[*it] = nb_ordo_tmp; // vraiment pour garder le template TODO:
                                   // faire la même proprement
        // incrémenter pour avoir le numéro suivant
        nb_ordo_tmp++;

        double min_finish_time = finish_on_at_CG(*it, bHost);
        //=======================
        // SimDag allows tasks to be executed concurrently when they can by
        // default. Yet schedulers take decisions assuming that tasks wait for
        // resource availability to start. The solution (well crude hack is to
        // keep track of the last task scheduled on a host and add a special
        // type of dependency if needed to force the sequential execution meant
        // by the scheduler. If the last schxeduled task is already done, has
        // failed or is a predecessor of the current task, no need for a new
        // dependency

        SD_task_t last_scheduled_task = sg_host_get_last_scheduled_task(bHost);
        // pour la prise en compte du nombre de coeurs, c'est là qu'il faut
        // taper (rajouter un compteur ou un tableau, ou alors traduire le
        // nombre de coeurs dans la plateforme (un gros cluster de machine
        // mono-coeur
        if (last_scheduled_task &&
            (SD_task_get_state(last_scheduled_task) != SD_DONE) &&
            (SD_task_get_state(last_scheduled_task) != SD_FAILED) &&
            !SD_task_dependency_exists(sg_host_get_last_scheduled_task(bHost),
                                       *it)) {
          SD_task_dependency_add("resource", NULL, last_scheduled_task, *it);
        }
        sg_host_set_last_scheduled_task(bHost, *it);
        sg_host_set_available_at(bHost, min_finish_time);

        //=======================

        // simulate with update
        SD_simulate_with_update(-1.0, changed_tasks);
      }

      //===================================================================
      const char nomAlgo[] = "Heft Critical Greedy";
      credoFile(nomAlgo, budget, cagnotte);
      //===================================================================

      //==================================================
      // sending results
      //---------------------
      /*
         Wf is the same for father and son.
         Same order of tasks given by following foreach.
         For each task, send
         an u int for ordo
         an u int for size of hostname
         char[] for hostname
      */

      xbt_dynar_foreach(getDagWf(wf), cursor, task) {
        if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

          size_t ordo_number;
          ordo_number = nb_ord[task];
          if (write(fd[1], &ordo_number, sizeof(size_t)) <= 0) {
            printf("Error write ordo\n");
            exit(-1);
          }

          size_t hostnamelen = strlen(sg_host_get_name(m[task]));
          if (write(fd[1], &hostnamelen, sizeof(size_t)) <= 0) {
            printf("Error write hostname length\n");
            exit(-1);
          }

          while (hostnamelen != 0) {
            size_t nb_bytes =
                write(fd[1], sg_host_get_name(m[task]), hostnamelen);
            if (nb_bytes < 0) {
              char output[100];
              sprintf(output, "problème de write dans pipe\n");
              perror(output);
              exit(1);
            }
            hostnamelen -= nb_bytes;
          }
        }
      }

      //==============================================
      // cleaning...
      // tachesSrc.clear();
      //  mapInfosTask.clear();
      freeUtilsMath(); // freeing the generator
      //  xbt_dynar_free_container(&dax);
      deleteWf(wf);

      size_t total_nhosts = sg_host_count();
      sg_host_t *hosts = sg_host_list();

      for (cursor = 0; cursor < total_nhosts; cursor++) {
        free(sg_host_user(hosts[cursor]));
        sg_host_user_set(hosts[cursor], NULL);
      }

      xbt_free(hosts);
      SD_exit();
      exit(0);

    } // end child
    else {
      // fait l'ordo
      close(fd[1]); /* close write */
      /*
         Wf is the same for father and son.
         Same order of tasks given by following foreach.
         For each task, send
         an u int for ordo
         an u int for size of hostname
         char[] for hostname
      */

      xbt_dynar_foreach(getDagWf(wf), cursor, task) {
        if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

          size_t ordo_number = 0;
          if (read(fd[0], &ordo_number, sizeof(size_t)) <= 0) {
            printf("Error read ordo\n");
            exit(-1);
          }

          size_t hostnamelen = strlen(SD_task_get_name(task));
          if (read(fd[0], &hostnamelen, sizeof(size_t)) <= 0) {
            printf("Error read hostname length\n");
            exit(-1);
          }

          unsigned int hostLen =
              hostnamelen; // to keep the lenght of the recieved host name
                           // somewhere and be able to put the \0 at the end of
                           // the reception
          char *nomHost = (char *)malloc(sizeof(char) * (hostnamelen + 1));
          while (hostnamelen != 0) {
            size_t nb_bytes = read(fd[0], nomHost, hostnamelen);
            if (nb_bytes < 0) {
              char output[100];
              sprintf(output, "problème de read dans pipe\n");
              perror(output);
              exit(1);
            }
            hostnamelen -= nb_bytes;
          } // end of receive name of host

          nomHost[hostLen] = '\0';
          // std::cout <<"j'ai lu le nom de l'hôte : " <<nomHost <<" pour la
          // tâche : " <<SD_task_get_name(task) <<" d'indice : " <<ordo_number
          // <<"\n";
          m[task] = sg_host_by_name(nomHost);
          ordoT[ordo_number] = task;
          free(nomHost);
        } // end of if task is executable
      }   // end of foreach

      int stat_val;
      waitpid(childpid, &stat_val, 0);
    } // end parent
  }

} // end of func