#ifndef ALGO__ARABNEJAD_H
#define ALGO__ARABNEJAD_H

// explosion des limites sup des doubles --> voir si gmp n'aurait pas un type de
// remplacement

#include <set>

// suggestion : virer lvl de là et s'en servir dans une map de BoTlvl en tant
// que clef. Ou le garder et se faire aussi une map qui associe un niveau à
// BoTlvl
typedef struct BoTlvl_t BoTlvl;
struct BoTlvl_t {
  std::set<SD_task_t> BoT;
  double subBudget;
  int lvl;
};

std::set<SD_task_t> get_set_BoTlvl(BoTlvl BoTl);
double get_subBudget_BoTlvl(BoTlvl BoTl);
double get_lvl_BoTlvl(BoTlvl BoTl);
void add_task_BoTlvl(BoTlvl &BoTl, SD_task_t task);
void set_subBudget_BoTlvl(BoTlvl &BoTl, double newSubBudget);
void set_lvl_BoTlvl(BoTlvl &BoTl, double newLvl);
// ini : -1 car 0 est une valeur tout à fait possible
void init_BoTlvl(BoTlvl &BoTl);
void delete_task_BoTlvl(SD_task_t t, BoTlvl &Botl);
// pour les tests : affiche le BoT
void affBoT(BoTlvl BoTl);

//==================================================
// méthodes propres à arabnejad
//-----------------------------

sg_host_t SD_task_get_best_host_arabnejad(SD_task_t task, BoTlvl &BoTAct,
                                          std::map<sg_host_t, HostCost> mhc,
                                          std::map<int, BoTlvl> &mLvlSet,
                                          double &selected_cost);

/**
 * Calcule l'earliest start time selon la méthode indiquée dans l'article
 *d'Arabnejad (pas de latence, coût de comm systématique -- on ne sait pas où se
 *déroule la tâche fille, pas trop le choix si on veut suivre la formule qu'ils
 *donnent)
 **/
double get_EST_arabnejad(SD_task_t task);

/**
 * répartit le budget entre les niveaux en mode "All in" :
 * le niveau le plus prio commence avec tout le budget, les autres zéro
 **/
void repAllIn(std::map<int, BoTlvl> &mB, int lvlMax, double budget);

// calcule le rang de la tâche demandée, celui de ses enfants, stocke le tout
// dans la map donnée, et renvoie le nombre de niveaux trouvés
int calc_rank_arabnejad(std::map<SD_task_t, int> &mTsklvl, SD_task_t task);

/**
 * Remplit la map associant un niveau à un ensemble de tâches (et crée ledit
 *ensemble), retourne le nombre de niveaux créés Les budgets sont, par défaut,
 *mis à zéro
 *
 * mTmp : map temporaire pour un accès simple et rapide + check de si solution
 *déjà calculée pendant le calcul mLvlSet : map au cas où un article sorte pour
 *proposer une variante changeant le calcul des iveaux, mais un tableau aurait
 *pu faire l'affaire
 **/
int get_map_rank_arabnejad(std::map<int, BoTlvl> &mLvlSet,
                           std::map<SD_task_t, int> &mTmp, xbt_dynar_t dag);

SD_task_t get_selected_task_arabnejad(BoTlvl BoTAct);

double calc_index_time(double ECTmax, double ECTmin, double ECTAct);
double calc_index_cost(double subBudget, double Cbest, double CAct);

void algo_offline_arabnejad(Workflow &wf, SD_task_t ordoT[],
                            std::map<SD_task_t, sg_host_t> &m, double deadline,
                            double budget, double cagnotte,
                            std::map<sg_host_t, HostCost> mhc);

#endif
