#include <iostream>
#include <list>
#include <map>
#include <sys/wait.h>

#include "algo_CGFair_sec_rep.h"
#include "algo_critical_greedy_extended_total_fair.h"
#include "calcBudgPerTask.h"
#include "hostCost.h"
#include "toolsSched.h"
#include "workflow.h"

//========================================================================================
/**
 * Fait l'ordonnancement et retourne le mapping tâche -> hôte + l'odre
 *d'attribution (vu qu'il remet sans cesse en question l'ordre d'attribution et
 *que seul
 *
 * ordo_t = tableau des tâches ordonnancées (données dans l'ordre de leur
 *ordonnancement) wf = workflow à placer m = mapping <tâche, hôte> effectué par
 *l'algorithme copyWf : copie du workflow (les connexions entre tâches sont
 *détruites pendant la simulation, et faire une copie d'un dynar est une
 *abomination sans nom (rien de déjà fait, semble-t-il), plus simple de charger
 *le dax directement deux fois
 **/
void algo_offline_cgfair_sec_rep(Workflow &wf, Workflow &copyWf,
                                 SD_task_t ordoT[],
                                 std::map<SD_task_t, sg_host_t> &m,
                                 double deadline, double budget,
                                 double cagnotte,
                                 std::map<sg_host_t, HostCost> mhc, double Cmin,
                                 double Cmax) {

  std::list<SD_task_t> critPath;

  // we are pessimistic, we use the worst values:
  worstValues(wf);

  // bricoler une map <nom de tâche, tâche du vrai wf> // but : raccourcir les
  // temps d'accès aux tâches
  size_t compt;
  SD_task_t task;
  std::map<std::string, SD_task_t> dicoTsk;
  xbt_dynar_foreach(getDagWf(wf), compt, task) {
    dicoTsk[SD_task_get_name(task)] = task;
  }

  // first scheduling : critical greedy fair
  //  algo_offline_heft_budget(wf, ordoT, m, deadline, budget, cagnotte, mhc);
  algo_offline_CG_total_fair(wf, ordoT, m, deadline, budget, cagnotte, mhc,
                             Cmin, Cmax);

  // get the amount of not used money
  double resMakespan = -1;
  double resCost = -1;

  // getMakespanCostSched(wf, ordoT, m, deadline, budget, cagnotte, mhc,
  // resMakespan, resCost);
  getMakespanCostCPSched(wf, ordoT, m, deadline, budget, cagnotte, mhc,
                         resMakespan, resCost, critPath, copyWf, dicoTsk);

  // std::cout <<"ICI algoheftsecrep, et je lis " <<resMakespan <<" secondes et
  // " <<resCost <<" sous.\n";

  // reattributing the not used budget for the worst case scenario
  double budgLeftovers = budget - resCost;

  // for each task, check if the result is not better with another VM, starting
  // with the last tasks it's a HEFT : I'll use the pre-calculated order

  size_t nbTask = get_nb_calc_tasks(getDagWf(wf));
  size_t nhosts = sg_host_count();
  // std::cout <<"nbTask =" <<nbTask <<"\n";
  size_t i = 0;
  size_t j = 0;

  double bestMakespan = resMakespan;
  double costBestMakespan = resCost;
  sg_host_t *hosts = sg_host_list();
  double oldMakespan = resMakespan;
  double oldCost = resCost;

  SD_task_t bestKeptTask = NULL;
  sg_host_t bestKeptHost = NULL;

  // construct the set of used VMs
  std::set<sg_host_t> usedVM;
  std::cout << "\t\t reste du budget : " << budgLeftovers << "\n";
  critPath.clear();
  //==================================================================
  // tant que budget :
  while (budgLeftovers >= 0) {
    std::cout << " lalilaloula\n";
    // récupérer le critical path
    getMakespanCostCPSched(wf, ordoT, m, deadline, budget, cagnotte, mhc,
                           resMakespan, resCost, critPath, copyWf, dicoTsk);
    oldMakespan = resMakespan;
    oldCost = resCost;
    bool tskChanged = false;
    std::cout << " taille du critPath : " << critPath.size() << "\n";

    // check : contenu du critical path
    for (std::list<SD_task_t>::iterator it = critPath.begin();
         it != critPath.end(); it++) {
      std::cout << "\tcrit path tâche : " << SD_task_get_name(*it) << "\n";
    }
    // std::cout <<"crit path hôte : " <<sg_host_get_name(it2->second) <<"\n";

    double rappCost = 0;
    double bestRappCost = 0;

    // pour chaque tâche du critical path, parcourir toutes les VMs et prendre
    // celle qui permet d'avoir le meilleur rapport temps/cost
    for (std::list<SD_task_t>::iterator it = critPath.begin();
         it != critPath.end(); it++) {

      if ((strcmp(SD_task_get_name(*it), "end") != 0) &&
          (strcmp(SD_task_get_name(*it), "root") != 0)) {
        // we don't touch end and root, they are not real
        // make a copy of the original assignment
        std::map<SD_task_t, sg_host_t> mCopy(m);
        usedVM.empty();

        for (std::map<SD_task_t, sg_host_t>::iterator it2 = mCopy.begin();
             it2 != mCopy.end(); ++it2) {
          // std::cout <<"crit path tâche : " <<SD_task_get_name(it2->first) <<"
          // ; "; std::cout <<"crit path hôte : "
          // <<sg_host_get_name(it2->second)
          // <<"\n";
          usedVM.insert(it2->second);
        }
        std::cout << "nb VM utilisées : " << usedVM.size() << "\n";

        int valVM[3] = {0, 0, 0};

        for (j = 0; j < nhosts; j++) {
          int aTester = 0;

          // if the host is not used at all yet
          if (usedVM.find(hosts[j]) == usedVM.end()) {
            // std::cout <<"host tout neuf\n";

            // get the type of VM
            const char *name = sg_host_get_name(hosts[j]);
            if (strlen(name) >= 3) {
              //	    std::cout <<"   host en cours d'évaluation : "
              //<<sg_host_get_name(hosts[j]) <<"\n"; std::cout<<"3 ème char : "
              // <<name[2] <<"\n";
              if (name[2] == '1') {
                if (valVM[0] != 2) {
                  valVM[0] = 1;
                  aTester = 1;
                  // std::cout <<"\t\t\t1 !\n";
                }
              }
              if (name[2] == '2') {
                if (valVM[1] != 2) {
                  valVM[1] = 1;
                  aTester = 1;
                  // std::cout <<"\t\t\t2 !\n";
                }
              }
              if (name[2] == '3') {
                if (valVM[2] != 2) {
                  valVM[2] = 1;
                  aTester = 1;
                  // std::cout <<"\t\t\t3 !\n";
                }
              }
            } // end of type of vm

          } // end of if host is new

          // if the host is one of the used VM
          else {
            // std::cout <<"host tout vieux\n";
            // std::cout <<"   host en cours d'évaluation : "
            // <<sg_host_get_name(hosts[j]) <<"\n";
            aTester = 1;
          }

          if (aTester == 1) {
            //===========================================================
            // test de VM
            //-------------------------

            std::cout << "    check du nom de la tâche : "
                      << SD_task_get_name(*it) << "\n";
            std::cout << "hôte testé : " << sg_host_get_name(hosts[j]) << "\n";
            // try others VMs to see if you can do better

            for (size_t recupTsk = 0; recupTsk < nbTask; recupTsk++) {
              if (strcmp(SD_task_get_name(ordoT[recupTsk]),
                         SD_task_get_name(*it)) == 0) {
                mCopy[ordoT[recupTsk]] = hosts[j];
                std::cout << " resInter, tâche = "
                          << SD_task_get_name(ordoT[recupTsk]) << "\n";
                break;
              }
            }
            getMakespanCostSched(wf, ordoT, mCopy, deadline, budget, cagnotte,
                                 mhc, resMakespan, resCost);
            rappCost = (oldMakespan - resMakespan) / (resCost - oldCost);
            double gainMakespan = oldMakespan - resMakespan;
            double prixChangement = resCost - oldCost;
            std::cout << "rappCost = " << rappCost
                      << ", gain sur le makespan = " << gainMakespan
                      << ", prix du changement = " << prixChangement << "\n";
            //	  double rappCost = 0;
            //	  double bestRappCost = 0;
            //	  double bestMakespan = resMakespan;
            //	  double costBestMakespan = resCost;
            //	  sg_host_t *hosts = sg_host_list();
            //	  double oldMakespan = resMakespan;
            //	  double oldCost = resCost;
            /*
            // !!!!!!!!!!!!!!!!!!!! TODO penser à vider ce truc !!!

            std::cout <<" debug, critical path: \n
            ------------------------------------------\n";
            getMakespanCostCPSched(wf, ordoT, mCopy, deadline, budget, cagnotte,
            mhc, resMakespan, resCost, critPath, copyWf, dicoTsk);

            std::cout <<" debug, finCrit: \n
            ------------------------------------------\n";
            */
            // std::cout<<"resInter : \n    tâche = "
            // <<SD_task_get_name(ordoT[i]) <<"\n    hôte = "
            // <<sg_host_get_name(hosts[j]) <<"\n      --> makespan = "
            // <<resMakespan <<"\n    --> cost = " <<resCost <<"\n";

            // std::cout <<"   bestMakespan : " <<bestMakespan  <<"\n";
            // std::cout <<"   costBestMakespan : " << costBestMakespan <<"\n";
            // std::cout <<"   resMakespan : " <<resMakespan  <<"\n";
            // std::cout <<"   resCost : " << resCost <<"\n\n";

            // std::cout <<"   hôte : " << sg_host_get_name(hosts[j]) <<" j : "
            // <<j <<"tâche = " <<SD_task_get_name(ordoT[i]) <<" i : " <<i
            // <<"\n";

            // if the makespan is better and if the budget allows it,
            // replace the old one by the new one
            if ((rappCost > bestRappCost) && (resMakespan < oldMakespan)) {
              // m.clear();
              tskChanged = true;
              /*
              std::cout <<"changement !\n";
              std::cout <<"   rappCost : " <<rappCost <<"\n";
              std::cout <<"   bestrappCost : " <<bestRappCost <<"\n";
              std::cout <<"   old Makespan : " <<oldMakespan <<"\n";
              std::cout <<"   resMakespan : " <<resMakespan  <<"\n";
              std::cout <<"   nouvel host : " <<sg_host_get_name(hosts[j])
              <<"\n";
              */
              /*std::cout <<"   bestMakespan : " <<bestMakespan <<"\n";
              std::cout <<"   costBestMakespan : " <<costBestMakespan <<"\n";
              */
              // m = mCopy;

              bestKeptTask = *it;
              bestKeptHost = hosts[j];

              // bestRappCost = rappCost;
              // oldMakespan = resMakespan;
              // oldCost = resCost;

              // remise à zéro des variables de vérif
              int iValVM;
              for (iValVM = 0; iValVM < 3; iValVM++) {
                if (valVM[iValVM] == 1) {
                  valVM[iValVM] = 0;
                }
              }
              aTester = 0;

            } // end if better VM
            /*
            else{
              std::cout <<"host : " <<sg_host_get_name(hosts[j]) <<"\n";
              std::cout <<"   pas de modif, rappCost : " <<rappCost <<"\n";
              std::cout <<"   pas de modif, bestrappCost : " <<bestRappCost
            <<"\n"; std::cout <<"   pas de modif, old Makespan : " <<oldMakespan
            <<"\n"; std::cout <<"   pas de modif, resMakespan : " <<resMakespan
            <<"\n";
            }
            */
            //============================================================

            // passage à 2 de la variable pour VM neuve
            int iValVM;
            for (iValVM = 0; iValVM < 3; iValVM++) {
              if (valVM[iValVM] == 1) {
                valVM[iValVM] = 2;
              }
            }
            aTester = 0;

          } // end of aTester

        } // end of for each possible host

      } // end if not end nor root

    } // end of for each task of the critical path
    // si aucune tâche du CP n'a été changée, sortir de la boucle
    if (!tskChanged) {
      std::cout << "Aucune machine trouvée, on sort de la boucle\n";
      break;
    }

    if ((bestKeptTask != NULL) && (bestKeptHost != NULL)) {
      m[bestKeptTask] = bestKeptHost;
      getMakespanCostSched(wf, ordoT, m, deadline, budget, cagnotte, mhc,
                           resMakespan, resCost);
      budgLeftovers = budget - resCost;
    }

    // on vide le critical path avant de calculer le nouveau
    critPath.clear();
    //    std::cout <<"coucou, nouvelle boucle !\n";
  } // end of if there are leftovers

  //=================================================================

  /*
  for(i; i<nbTask; i++){ // TODO: changer la condition de boucle ( --> tant
  qu'il y a du budget)

    //std::cout<<"BOUH : " <<SD_task_get_name(ordoT[i]) <<"\n";
    if((strcmp(SD_task_get_name(ordoT[i]), "end") != 0)
  &&(strcmp(SD_task_get_name(ordoT[i]), "root") != 0)){
      // we don't touch end and root, they are not real
      // make a copy of the original assignment
      std::map<SD_task_t, sg_host_t> mCopy(m);
      usedVM.empty();

      for(std::map<SD_task_t, sg_host_t>::iterator it=mCopy.begin() ;
  it!=mCopy.end() ; ++it){
        //std::cout <<"tâche : " <<SD_task_get_name(it->first) <<"\n";
        //std::cout <<"hôte : " <<sg_host_get_name(it->second) <<"\n";
        usedVM.insert(it->second);
      }
      std::cout <<"nb VM utilisées : " <<usedVM.size() <<"\n";


      int valVM[3] = {0,0,0};

      for(j=0; j<nhosts; j++){
        int aTester = 0;

        // if the host is not used at all yet
        if(usedVM.find(hosts[j]) == usedVM.end()){
          //std::cout <<"host tout neuf\n";

          // get the type of VM
          const char * name = sg_host_get_name(hosts[j]);
          if(strlen(name) >= 3){
            if(name[2] == '1'){
              if(valVM[0] != 2){
                valVM[0] = 1;
                aTester = 1;
              }
            }
            if(name[2] == '2'){
              if(valVM[1] != 2){
                valVM[1] = 1;
                aTester = 1;
              }
            }
            if(name[2] == '3'){
              if(valVM[2] != 2){
                valVM[2] = 1;
                aTester = 1;
              }
            }
          } // end of type of vm

        }// end of if host is new

        // if the host is one of the used VM
        else{
          //std::cout <<"host tout vieux\n";
          aTester = 1;
        }


        if(aTester == 1){
          //===========================================================
          // test de VM
          //-------------------------

          // !!!!!!!!!!!!!!!!!!!! TODO penser à vider ce truc !!!

          std::cout <<"hôte testé : " <<sg_host_get_name(hosts[j]) <<"\n";
          // try others VMs to see if you can do better
          mCopy[ordoT[i]] = hosts[j];
          //getMakespanCostSched(wf, ordoT, mCopy, deadline, budget, cagnotte,
  mhc, resMakespan, resCost);

          std::cout <<" debug, critical path: \n
  ------------------------------------------\n"; getMakespanCostCPSched(wf,
  ordoT, mCopy, deadline, budget, cagnotte, mhc, resMakespan, resCost, critPath,
  copyWf, dicoTsk);

          std::cout <<" debug, finCrit: \n
  ------------------------------------------\n";

          //	std::cout<<"resInter : \n    tâche = "
  <<SD_task_get_name(ordoT[i]) <<"\n    hôte = " <<sg_host_get_name(hosts[j])
  <<"\n      --> makespan = " <<resMakespan <<"\n    --> cost = " <<resCost
  <<"\n";


          //std::cout <<"   bestMakespan : " <<bestMakespan  <<"\n";
          //std::cout <<"   costBestMakespan : " << costBestMakespan <<"\n";
          //std::cout <<"   resMakespan : " <<resMakespan  <<"\n";
          //std::cout <<"   resCost : " << resCost <<"\n\n";

          //std::cout <<"   hôte : " << sg_host_get_name(hosts[j]) <<" j : " <<j
  <<"tâche = " <<SD_task_get_name(ordoT[i]) <<" i : " <<i <<"\n";

          // if the makespan is better and if the budget allows it,
          // replace the old one by the new one
          if((resMakespan < bestMakespan)&&(resCost < budget)){
            // m.clear();
            std::cout <<"changement !\n";
            std::cout <<"   resMakespan : " <<resMakespan  <<"\n";
            std::cout <<"   bestMakespan : " <<bestMakespan <<"\n";
            std::cout <<"   costBestMakespan : " <<costBestMakespan <<"\n";

            m = mCopy;
            costBestMakespan = resCost;
            bestMakespan = resMakespan;

            std::cout <<"   nouveau bestMakespan : " <<bestMakespan <<"\n";
            std::cout <<"   nouveau costBestMakespan : " <<costBestMakespan
  <<"\n\n";

            // remise à zéro des variables de vérif
            int iValVM;
            for(iValVM = 0; iValVM <3; iValVM++){
              if(valVM[iValVM] == 1){
                valVM[iValVM] = 0;
              }
            }
            aTester = 0;

          }
          //============================================================

          // passage à 2 de la variable pour VM neuve
          int iValVM;
          for(iValVM = 0; iValVM <3; iValVM++){
            if(valVM[iValVM] == 1){
                valVM[iValVM] = 2;
            }
          }
          aTester = 0;

        }
      }
      //      std::cout <<"\n\n";

      mCopy.clear();

    }
    std::cout << "##################################\n";
  }
  */
}