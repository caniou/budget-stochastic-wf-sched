#ifndef TOOLS_SCHED_H
#define TOOLS_SCHED_H

#include "hostCost.h"
#include "stochTasks.h"
#include "workflow.h"

// structure initialy from
// https://github.com/simgrid/simgrid/blob/master/examples/simdag

typedef struct _HostAttribute *HostAttribute;
struct _HostAttribute {
  /* Earliest time at which a host is ready to execute a task */
  double available_at;
  SD_task_t last_scheduled_task;
};

/**
 * Return true if the given task is the dummy task root, false otherwise
 **/
bool isRoot(SD_task_t task);

SD_task_t get_root(xbt_dynar_t dag);

//===================================================================

/**
 * Get the earliest time at which a host is ready to execute a task
 **/
double sg_host_get_available_at(sg_host_t host);

/**
 * Set the earliest time at which a host is ready to execute a task
 **/
void sg_host_set_available_at(sg_host_t host, double time);

/**
 * Get the last scheduled task of a host
 **/
SD_task_t sg_host_get_last_scheduled_task(sg_host_t host);

/**
 * Set the last scheduled task of a host
 **/
void sg_host_set_last_scheduled_task(sg_host_t host, SD_task_t task);

/**
 * Get the tasks which are ready to be scheduled
 **/
xbt_dynar_t get_ready_tasks(xbt_dynar_t dax);

/**
 * Return the time at which the given task is expected to be completed on the
 *given host
 **/
double finish_on_at(SD_task_t task, sg_host_t host);

/**
 * Return true if the given task is the dummy task end, false otherwise
 **/
bool isEnd(SD_task_t task);

/**
 * alloue la tâche t à l'hôte h et met à jour les variables impliquées
 * et retourne la quantité de temps résiduelle
 **/
void allocate(SD_task_t &t, sg_host_t &h, std::map<sg_host_t, HostCost> &mhc,
              double timeTsk);

/**
 * Simule un ordonnancement et en retourne le makespan et le coût
 **/
void getMakespanCostSched(Workflow &wf, SD_task_t ordoT[],
                          std::map<SD_task_t, sg_host_t> &m, double deadline,
                          double budget, double cagnotte,
                          std::map<sg_host_t, HostCost> mhc,
                          double &resMakespan, double &resCost);

void getMakespanCostSched_avec_tps(Workflow &wf, SD_task_t ordoTasks[],
                                   std::map<SD_task_t, sg_host_t> &m,
                                   double deadline, double budget,
                                   double cagnotte,
                                   std::map<sg_host_t, HostCost> mhc,
                                   double &resMakespan, double &resCost,
                                   std::map<SD_task_t, double> &list_T);

//===============================================================================

size_t get_nb_calc_tasks(xbt_dynar_t dag);

double get_amount_data_needed(SD_task_t task);

double get_cost_data_prec_newVM(SD_task_t task,
                                std::map<sg_host_t, HostCost> mhc);

void getRunnableChildren(xbt_dynar_t dag, SD_task_t task,
                         std::list<SD_task_t> &l);

void getRunnableParents(xbt_dynar_t dag, SD_task_t task,
                        std::list<SD_task_t> &l);

int nbMaxVMn(xbt_dynar_t dag);

sg_host_t getMostExpensiveFreeHost(std::map<sg_host_t, HostCost> &mhc);

void costTimeResults(std::map<sg_host_t, HostCost> mhc,
                     std::map<SD_task_t, SD_task_t> &mCommRootDest,
                     std::map<sg_host_t, SD_task_t> &starts,
                     std::map<sg_host_t, SD_task_t> &ends, Workflow wf,
                     double &finWf, double &hStart, long &nbTsk,
                     std::set<sg_host_t> &usedHosts, sg_host_t *hosts,
                     std::map<sg_host_t, double> &tpsVMCalc, double &nbVM,
                     double &costTotVM, double &pourcentUsed);

void costTimeResultsVerbose(std::map<sg_host_t, HostCost> mhc,
                            std::map<SD_task_t, SD_task_t> &mCommRootDest,
                            std::map<sg_host_t, SD_task_t> &starts,
                            std::map<sg_host_t, SD_task_t> &ends, Workflow wf,
                            double &finWf, double &hStart, long &nbTsk,
                            std::set<sg_host_t> &usedHosts, sg_host_t *hosts,
                            std::map<sg_host_t, double> &tpsVMCalc,
                            double &nbVM, double &costTotVM,
                            double &pourcentUsed,
                            std::map<sg_host_t, double> &tpsVMTot);

void SD_simulate_with_update(double how_long, xbt_dynar_t changed_tasks_dynar);

void createTopoOrder(xbt_dynar_t dag, SD_task_t task,
                     std::list<SD_task_t> &topoOrder,
                     std::map<SD_task_t, bool> &visited);

void getCPTopoOrder(xbt_dynar_t dag, std::list<SD_task_t> &critPath,
                    xbt_dynar_t copyDag,
                    std::map<std::string, SD_task_t> dicoTsk,
                    std::list<SD_task_t> &topoOrder);

void getMakespanCostCPSched(Workflow &wf, SD_task_t ordoTasks[],
                            std::map<SD_task_t, sg_host_t> &m, double deadline,
                            double budget, double cagnotte,
                            std::map<sg_host_t, HostCost> mhc,
                            double &resMakespan, double &resCost,
                            std::list<SD_task_t> &critPath, Workflow &copyWf,
                            std::map<std::string, SD_task_t> dicoTsk);

/******************************************************************************
*******************************************************************************
******************************************************************************/

double calcMeanSpeed(const sg_host_t *hosts, const size_t nb_hosts);

double calcMeanInitialCost(const sg_host_t *hosts, const size_t nb_hosts,
                           std::map<sg_host_t, HostCost> mhc);

double calcMeanBandwidht(const sg_host_t *hosts);

double calcMeanLatency(const sg_host_t *hosts);

double getAmountInOutData(const SD_task_t &T);

bool minOneComputableTask(const xbt_dynar_t &tasks);

xbt_dynar_t getNotDucplicateGrdChild(const SD_task_t task);

#endif
