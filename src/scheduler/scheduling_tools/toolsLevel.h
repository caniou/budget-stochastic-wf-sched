#ifndef TOOLS_LEVEL_H
#define TOOLS_LEVEL_H

void displayLevels(
    const std::map<unsigned int, std::list<SD_task_t>> &listTaskPLvl);

void getTskLvlRec(std::map<SD_task_t, unsigned int> &lvlPTsk, SD_task_t T,
                  unsigned int lvl);

std::map<unsigned int, std::list<SD_task_t>> getTskLvl(const Workflow &wf);

#endif