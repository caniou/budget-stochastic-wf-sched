#ifndef CALC_BUDG_PER_TASK_LEVEL_H
#define CALC_BUDG_PER_TASK_LEVEL_H

void getDataLvl(const std::map<unsigned int, std::list<SD_task_t>> listTaskPLvl,
                std::map<unsigned int, double> &WmaxPLvl,
                std::map<unsigned int, double> &transfPLvl, double &WmaxLvl,
                double &transfLvl, std::map<SD_task_t, double> &WmaxPTask,
                std::map<SD_task_t, double> &transfPTask);

void calcBudgPTskLvl(
    Workflow &wf, double budget, double bw, double speed_m,
    std::map<SD_task_t, double> &budgPTsk,
    const std::map<unsigned int, std::list<SD_task_t>> listTaskPLvl);
    
#endif