#include "simgrid/simdag.h"
#include "xbt.h"
#include <algorithm>
#include <cfloat>
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <stack>
#include <string>
#include <sys/wait.h>
#include <vector>

#include "hostCost.h"
#include "stochTasks.h"
#include "toolsSched.h"
#include "workflow.h"
/**
 * Return true if the given task is the dummy task root, false otherwise
 **/
bool isRoot(SD_task_t task) {
  return (strcmp(SD_task_get_name(task), "root") == 0);
}

/**
 * Return true if the given task is the dummy task end, false otherwise
 **/
bool isEnd(SD_task_t task) {
  return (strcmp(SD_task_get_name(task), "end") == 0);
}

/*=========================================================
 * Functions from
 * https://github.com/simgrid/simgrid/blob/master/examples/simdag
 *
 * -------------------------------------------------------*/

// set of functions to :
//    - get the ready tasks
//    - calculate the earliest time at which an host is ready to execute a task
//    - get the last scheduled task of an host

/**
 * Get the earliest time at which a host is ready to execute a task
 **/
double sg_host_get_available_at(sg_host_t host) {
  HostAttribute attr = (HostAttribute)sg_host_user(host);
  return attr->available_at;
}

/**
 * Set the earliest time at which a host is ready to execute a task
 **/
void sg_host_set_available_at(sg_host_t host, double time) {
  HostAttribute attr = (HostAttribute)sg_host_user(host);
  attr->available_at = time;
  sg_host_user_set(host, attr);
}

/**
 * Get the last scheduled task of a host
 **/
SD_task_t sg_host_get_last_scheduled_task(sg_host_t host) {
  HostAttribute attr = (HostAttribute)sg_host_user(host);
  return attr->last_scheduled_task;
}

/**
 * Set the last scheduled task of a host
 **/
void sg_host_set_last_scheduled_task(sg_host_t host, SD_task_t task) {
  HostAttribute attr = (HostAttribute)sg_host_user(host);
  attr->last_scheduled_task = task;
  sg_host_user_set(host, attr);
}

/**
 * Get the tasks which are ready to be scheduled
 **/
xbt_dynar_t get_ready_tasks(xbt_dynar_t dax) {
  unsigned int i;
  xbt_dynar_t ready_tasks;
  SD_task_t task;

  ready_tasks = xbt_dynar_new(sizeof(SD_task_t), NULL);
  xbt_dynar_foreach(dax, i, task) {
    if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ &&
        SD_task_get_state(task) == SD_SCHEDULABLE) {
      xbt_dynar_push(ready_tasks, &task);
    }
  }
  // XBT_DEBUG("There are %lu ready tasks", xbt_dynar_length(ready_tasks));

  return ready_tasks;
}

size_t get_nb_calc_tasks(xbt_dynar_t dag) {
  size_t i;
  size_t res = 0;
  SD_task_t task;

  xbt_dynar_foreach(dag, i, task) {
    if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {
      res++;
    }
  }
  return res;
}

double get_amount_data_needed(SD_task_t task) {
  double amount = 0.;
  size_t i;
  xbt_dynar_t parents = SD_task_get_parents(task);
  sg_host_t *hosts = sg_host_list();

  // on veut la taille des fichiers parents
  if (!xbt_dynar_is_empty(parents)) {
    SD_task_t parent;
    xbt_dynar_foreach(parents, i, parent) {
      if (SD_task_get_kind(parent) == SD_TASK_COMM_E2E) {
        amount += SD_task_get_amount(parent);
      }
    } // fin du foreach
  }
  xbt_dynar_free_container(&parents);
  xbt_free(hosts);
  return amount;
}

/**
 * calcul du coût dû à un changement de VM pour la tâche donnée en ce qui
 *concerne les tâches parentes de cette dernière
 **/
double get_cost_data_prec_newVM(SD_task_t task,
                                std::map<sg_host_t, HostCost> mhc) {
  double amount = 0.;
  size_t i, j;
  xbt_dynar_t parents = SD_task_get_parents(task);
  sg_host_t *hosts = sg_host_list();
  double bw = sg_host_route_bandwidth(hosts[0], hosts[1]);
  double costHostSide = 0.;
  bool rootian = false;
  double costH = 0.;

  // on veut la taille des fichiers parents, et le prix qui va avec
  if (!xbt_dynar_is_empty(parents)) {
    SD_task_t parent;
    xbt_dynar_foreach(parents, i, parent) {
      sg_host_t *parent_host = SD_task_get_workstation_list(parent);

      if (SD_task_get_kind(parent) == SD_TASK_COMM_E2E) {
        // std::cout<< "            TEST get_cost_data_prec_newVM, host : "
        // <<sg_host_get_name(parent_host[0]) <<"\n";
        xbt_dynar_t gParents = SD_task_get_parents(parent);
        // si le grand parent est root, l'ajout d'amount = 0 (cette tâche
        // n'EXISTE PAS)
        SD_task_t gParent;

        if (!xbt_dynar_is_empty(gParents)) { // ça ne devrait jamais être le
                                             // cas, soit dit en passant
          xbt_dynar_foreach(gParents, j, gParent) {
            if (strcmp(SD_task_get_name(gParent), "root") == 0) {
              rootian = true;
            }
          }
        }
        if (rootian) {
          costHostSide = 0.;
        } else {
          costHostSide =
              (SD_task_get_amount(parent) / bw) * getHC_hC(mhc[parent_host[0]]);
        }

        amount += costHostSide;
        xbt_dynar_free_container(&gParents);
      }
    } // fin du foreach
  }
  xbt_dynar_free_container(&parents);
  xbt_free(hosts);
  //  std::cout << "            TEST get_cost_data_prec_newVM, cost tot : "
  //  <<amount <<"\n";
  return amount;
}

/**
 * Return the time at which the given task is expected to be completed on the
 *given host
 **/
double finish_on_at(SD_task_t task, sg_host_t host) {
  //
  double result = 0.;
  unsigned int i;
  double data_available = 0.;
  double redist_time = 0;
  double last_data_available;

  xbt_dynar_t parents = SD_task_get_parents(task);
  sg_host_t *hosts = sg_host_list();

  // si la tâche a des prédécesseurs, son heure de début (et de fin) va en
  // dépendre
  if (!xbt_dynar_is_empty(parents)) {
    // compute last_data_available

    SD_task_t parent;
    last_data_available = -1.0;
    xbt_dynar_foreach(parents, i, parent) {
      // normal case : the parent is a comm
      if (SD_task_get_kind(parent) == SD_TASK_COMM_E2E) {
        sg_host_t *parent_host = SD_task_get_workstation_list(parent);
        // Estimate the redistribution time from this parent
        if (SD_task_get_amount(parent) <= 1e-6) {
          redist_time = 0.;
        } else {
          // si la tâche a déjà été placée et exécutée, on peut récupérer les
          // iformations concernant son hôte
          if ((SD_task_get_state(parent) != SD_NOT_SCHEDULED) &&
              (SD_task_get_state(parent) != SD_SCHEDULABLE)) {
            redist_time = sg_host_route_latency(parent_host[0], host) +
                          SD_task_get_amount(parent) /
                              sg_host_route_bandwidth(parent_host[0], host);
          } else {
            double lat = 0.;
            double bw = 0.;
            if (SD_task_get_state(parent) != SD_NOT_SCHEDULED) {
              // si déjà calculée, ajouter la vraie valeur
              lat = sg_host_route_latency(parent_host[0], host);
              bw = sg_host_route_bandwidth(parent_host[0], host);
            } else {
              // sinon, calculer une estimation (récup des données pour
              // estimation)
              lat = sg_host_route_latency(hosts[0], hosts[1]);
              bw = sg_host_route_bandwidth(hosts[0], hosts[1]);
              // std::cout <<" parent : " <<SD_task_get_name(parent)<<"\n";
            }
            redist_time = lat + SD_task_get_amount(parent) / bw;
          }
        }
        // les données des "tâches" de communication sont disponibles une fois
        // les données totalement envoyées
        data_available = SD_task_get_start_time(parent) + redist_time;
      }

      // no transfer, control dependency -- cas où la tâche précédente est une
      // tâche exécutable
      if (SD_task_get_kind(parent) == SD_TASK_COMP_SEQ) {
        data_available = SD_task_get_finish_time(parent);
      }

      // mise à jour du moment le plus tardif pour lequel les données d'une
      // tâche seront disponibles
      if (last_data_available < data_available)
        last_data_available = data_available;
    } // fin du foreach

    //    xbt_dynar_free_container(&parents);

    result = MAX(sg_host_get_available_at(host), last_data_available) +
             SD_task_get_amount(task) / sg_host_speed(host);
  }
  // si la tâche n'a pas d'ascendent, rien à attendre
  else {
    //    xbt_dynar_free_container(&parents);

    result = sg_host_get_available_at(host) +
             SD_task_get_amount(task) / sg_host_speed(host);
  }

  xbt_dynar_free_container(&parents);
  xbt_free(hosts);
  return result;
}

/**
 * Get the list of the tasks depending of the present task
 **/
void getRunnableChildren(xbt_dynar_t dag, SD_task_t task,
                         std::list<SD_task_t> &l) {
  xbt_dynar_t dynTmp, dynTmp2;
  SD_task_t task2, task3;
  unsigned int cpt, cpt2;

  if (!isEnd(task)) {
    dynTmp = SD_task_get_children(task);

    xbt_dynar_foreach(dynTmp, cpt, task2) {
      //      std::cout <<task2 <<" hup1\n";
      //      std::cout <<SD_task_get_name(task2) <<" hup1\n";
      if (SD_task_get_kind(task2) == SD_TASK_COMP_SEQ) {
        // tache calculable, on rajoute
        l.push_front(task2);
      } else {
        if (!isEnd(task2)) {
          //	  std::cout <<task3 <<"hup2\n";
          dynTmp2 = SD_task_get_children(task2);
          xbt_dynar_foreach(dynTmp2, cpt2, task3) {
            if (SD_task_get_kind(task3) == SD_TASK_COMP_SEQ) {
              l.push_front(task3);
            }
          }
          xbt_dynar_free(&dynTmp2);
        }
      }
    }

    xbt_dynar_free(&dynTmp);
  }
}

/**
 * Get the list of the tasks parents of the present task
 **/
void getRunnableParents(xbt_dynar_t dag, SD_task_t task,
                        std::list<SD_task_t> &l) {
  xbt_dynar_t dynTmp, dynTmp2;
  SD_task_t task2, task3;
  unsigned int cpt, cpt2;

  if (!isRoot(task)) {
    dynTmp = SD_task_get_parents(task);
    xbt_dynar_foreach(dynTmp, cpt, task2) {
      if (SD_task_get_kind(task2) == SD_TASK_COMP_SEQ) {
        // tache calculable, on rajoute
        l.push_front(task2);
      } else {
        if (!isRoot(task2)) {
          dynTmp2 = SD_task_get_parents(task2);
          xbt_dynar_foreach(dynTmp2, cpt2, task3) {
            if (SD_task_get_kind(task3) == SD_TASK_COMP_SEQ) {
              l.push_front(task3);
            }
          }
        }
      }
    }

    xbt_dynar_free(&dynTmp);
    xbt_dynar_free(&dynTmp2);
  }
}

/**
 * alloue la tâche t à l'hôte h et met à jour les variables impliquées
 * et retourne la quantité de temps résiduelle
 **/
void allocate(SD_task_t &t, sg_host_t &h, std::map<sg_host_t, HostCost> &mhc,
              double timeTsk) {
  if ((SD_task_get_state(t) == SD_NOT_SCHEDULED) ||
      (SD_task_get_state(t) == SD_SCHEDULABLE)) {
    // nouvelle valeur occupée
    addHC_occTime(mhc[h], timeTsk);

    // calculer la nouvelle valeur allouée :
    // val entiere de la val occupée
    //  ceil(timeTsk);
    setHC_resTime(mhc[h], ceil(getHC_occTime(mhc[h])));
    // std::cout <<"---------------schedulons, tâche : " << SD_task_get_name(t)
    // <<" ; dans l'état : " <<SD_task_get_state(t) <<"\n";
    /*
      std::cout <<" pour info : " <<"SD_NOT_SCHEDULED = " << SD_NOT_SCHEDULED
      <<"\n"; std::cout <<" pour info : " <<"SD_SCHEDULED = " << SD_SCHEDULED
      <<"\n"; std::cout <<" pour info : " <<"SD_SCHEDULABLE = " <<
      SD_SCHEDULABLE <<"\n"; std::cout <<" pour info : " <<"SD_RUNNABLE = " <<
      SD_RUNNABLE <<"\n"; std::cout <<" pour info : " <<"SD_RUNNING = " <<
      SD_RUNNING <<"\n"; std::cout <<" pour info : " <<"SD_DONE = " << SD_DONE
      <<"\n"; std::cout <<" pour info : " <<"SD_FAILED = " << SD_FAILED <<"\n";
    */
    SD_task_schedulel(t, 1, h);
  }
  /*
    else{
    std::cout <<"--------------- ne schedulons pas, tâche : " <<
    SD_task_get_name(t) <<"\n";
    }
  */
}

/**
 * Evaluation du nombre maximum de VMs à louer pour le workflow
 * -- servira à générer l'infrastructure
 **/
int nbMaxVMn(xbt_dynar_t dag) {
  int res = 0;
  SD_task_t task;
  unsigned int cpt;
  std::map<SD_task_t, char> mapVu;
  // initialisation de la map : tout à 0
  xbt_dynar_foreach(dag, cpt, task) { mapVu[task] = 0; }

  // nbmax VM si 1DMesh  = nbsrc
  // si arbre = nb feuilles
  // sinon : tricky moche
  res = 0;
  xbt_dynar_foreach(dag, cpt, task) {
    if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {
      res++;
    }
  }

  res -= 2;

  return res;
}

/**
 * Retourne un des hôtes libres les plus chers, ou à défaut, le premier hôte
 **/
sg_host_t getMostExpensiveFreeHost(std::map<sg_host_t, HostCost> &mhc) {
  sg_host_t *hosts = sg_host_list();

  // ini
  sg_host_t res = hosts[0];
  size_t nhosts = sg_host_count();
  double cHor = getHC_hC(mhc[hosts[0]]);

  for (size_t i = 0; i < nhosts; i++) {
    // si plus cher et libre, devient le best host
    if ((getHC_hC(mhc[hosts[i]]) >= cHor) &&
        (getHC_resTime(mhc[hosts[i]]) != 10)) {
      cHor = getHC_hC(mhc[hosts[i]]);
      res = hosts[i];
    }
  }

  xbt_free(hosts);
  return res;
}

/**
 * Calcule divers indicateurs concernant le wf exécuté donné
 * tpsVMCalc : temps de calcul pur par machine
 * tpsVMH : temps de calcul total de la VM (utilisé pour le calcul du coût)
 * nbVM : nombre de VMs utilisées
 * pourcentUsed : pourcentage total des VMs utilisées ayant servi uniquement au
 *calcul costTotVM : coût dû à l'utilisation des VMs pour l'ensemble des VMs
 **/
void costTimeResults(std::map<sg_host_t, HostCost> mhc,
                     std::map<SD_task_t, SD_task_t> &mCommRootDest,
                     std::map<sg_host_t, SD_task_t> &starts,
                     std::map<sg_host_t, SD_task_t> &ends, Workflow wf,
                     double &finWf, double &hStart, long &nbTsk,
                     std::set<sg_host_t> &usedHosts, sg_host_t *hosts,
                     std::map<sg_host_t, double> &tpsVMCalc, double &nbVM,
                     double &costTotVM, double &pourcentUsed) {

  // ATTENTION : le code de cette procédure a été dupliqué dans la procédure
  // costTimeResultsVerbose dans l'objectif de bricoler un hotfix pour chopper
  // plus d'info sur les VMs. cette solution a été préférée à un appel à cette
  // fonction afin d'éviter de multiplier les parcours de listes et les
  // duplications de calculs
  unsigned int cursor;
  SD_task_t task;
  double amountDataIO = 0;

  // récupération des premières et dernières tâches pour chaque host : conservé
  // (de toute façon, on a besoin de faire cette boucle pour d'autres calculs
  xbt_dynar_foreach(getDagWf(wf), cursor, task) {

    sg_host_t *task_host = SD_task_get_workstation_list(
        task); // !!!! the comparison with the first host of the list works only
               // because there is at most one VM for one task

    // tache de fin = heure de fin
    if (strcmp(SD_task_get_name(task), "end") == 0) {
      //      std::cout <<"tâche end !\n";
      //      std::cout <<"début de la tâche end : "
      //      <<SD_task_get_start_time(task) <<"\n";
      finWf = SD_task_get_start_time(task);

      // transfert de fichiers du cloud vers l'extérieur
      xbt_dynar_t endParents = SD_task_get_parents(task);
      size_t cursorPar;
      SD_task_t ePar;
      xbt_dynar_foreach(endParents, cursorPar, ePar) {
        // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
        // <<SD_task_get_name(sChild) <<"\n";
        amountDataIO += SD_task_get_amount(ePar);
      }
      xbt_dynar_free_container(&endParents);
    }
    if (strcmp(SD_task_get_name(task), "root") == 0) {
      //      std::cout <<"tâche root !\n";
      hStart = SD_task_get_start_time(task);
      // std::cout <<"       récolte, tâche : " <<SD_task_get_name(task) <<" sur
      // la machine : " <<sg_host_get_name(task_host[0]) <<"qui débute à "
      // <<SD_task_get_start_time(task) <<", qui finit à "
      // <<SD_task_get_finish_time(task) <<"\n";

      // transferts de fichiers de l'extérieur du cloud vers le cloud
      xbt_dynar_t startChildren = SD_task_get_children(task);
      size_t cursorChild;
      SD_task_t sChild;
      xbt_dynar_foreach(startChildren, cursorChild, sChild) {
        // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
        // <<SD_task_get_name(sChild) <<"\n";
        amountDataIO += SD_task_get_amount(sChild);
      }
      xbt_dynar_free_container(&startChildren);
    }

    //-----------------------------------------------------------
    // S'il s'agit des tâches fantômes root et end, NE PAS LES PRENDRE EN
    // COMPTE, ce ne sont pas de "vraies" tâches, idem si la tâche est une des
    // comm de root
    if ((strcmp(SD_task_get_name(task), "root") != 0) &&
        (strcmp(SD_task_get_name(task), "end") != 0) &&
        (mCommRootDest.find(task) == mCommRootDest.end())) {
      // récupération des tâches de départ : si l'hôte n'a pas encore été
      // répertorié comme ayant une première tâche, l'ajouter
      if (starts.find(task_host[0]) == starts.end()) {
        starts[task_host[0]] = task;
      } else {
        if (SD_task_get_start_time(starts[task_host[0]]) >
            SD_task_get_start_time(task)) {
          starts[task_host[0]] = task;
        }
      }

      // récupération des tâches de fin
      if (ends.find(task_host[0]) == ends.end()) {
        ends[task_host[0]] = task;
      } else {
        // if(SD_task_get_finish_time(ends[task_host[0]]) <
        // SD_task_get_finish_time(task)){
        if ((strcmp(SD_task_get_name(task), "end") != 0) &&
            (SD_task_get_finish_time(ends[task_host[0]]) <
             SD_task_get_finish_time(task))) {
          ends[task_host[0]] = task;
        }
      }

      // récupération du nombre de tâches total
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {
        nbTsk++;
      }

      // récupération du nombre d'hôtes utilisés
      if (tpsVMCalc.find(task_host[0]) == tpsVMCalc.end()) {
        // initialisation du temps de calcul
        tpsVMCalc[task_host[0]] = 0.;

        // ajout de l'hôte à la liste des hôtes
        usedHosts.insert(task_host[0]);
      }

      // récupération du temps de calcul pur par machine (sans compter les
      // transferts et les attentes)
      tpsVMCalc[task_host[0]] +=
          SD_task_get_amount(task) / sg_host_speed(task_host[0]);
    } // end not root, not end
      //-----------------------------------------------

  } // end of the foreach task

  for (std::set<sg_host_t>::iterator it = usedHosts.begin();
       it != usedHosts.end(); ++it) {
    nbVM++;
    //    std::cout << ' ' << sg_host_get_name(*it);
    // récupération de la première tâche de chaque machine
    // SD_task_t tmpTsk = starts[*it];
    //            std::cout <<"première tâche : " <<SD_task_get_name(tmpTsk)
    //            <<", heure de début : " <<SD_task_get_start_time(tmpTsk)
    //            <<"\n";
    // fin d'exec de machine = tous les fichiers ont été envoyés, envoi
    // séquentiel

    //            std::cout<< "dernière tâche schedulée : "
    //            <<SD_task_get_name(sg_host_get_last_scheduled_task(*it)) <<",
    //            heure de fin de calcul de ladite tâche : "
    //            <<SD_task_get_finish_time(sg_host_get_last_scheduled_task(*it))
    //            <<"\n"; std::cout<< "dernière tâche effectuée : "
    //            <<SD_task_get_name(ends[*it]) <<", heure de fin de calcul de
    //            ladite tâche : " <<SD_task_get_finish_time(ends[*it]) <<"\n";

    // quantité de données à transférer avant fermeture :
    xbt_dynar_t children =
        SD_task_get_children(sg_host_get_last_scheduled_task(*it));
    double transfTime = 0.;
    double amountData = 0.;
    // truandage sur la bande passante, vu que je sais que c'est la même partout
    // et que j'ai plusieurs hôtes :
    double bwGlob = sg_host_route_bandwidth(hosts[0], hosts[1]);

    std::map<sg_host_t, double> tpsVMH;

    if (!xbt_dynar_is_empty(children)) {
      SD_task_t child;
      int iChild;

      xbt_dynar_foreach(children, iChild, child) {
        if (SD_task_get_kind(child) == SD_TASK_COMM_E2E) {
          amountData += SD_task_get_amount(child);
          //	    std::cout<< "data à transférer avant fermeture : "
          //<<amountData << "\n";
        } // endif comm

      } // end foreach child

    } // endif there are children

    xbt_dynar_free_container(&children);

    // récupération des temps liés aux transferts provenant de root (comptés
    // dans le prix, essentiel pour avoir les vrais coûts par VMs mais qui ont
    // eu le mauvais goût d'être artificiellement placés sur la VM de root)
    size_t cursorComm;

    for (std::map<SD_task_t, SD_task_t>::iterator it2 = mCommRootDest.begin();
         it2 != mCommRootDest.end(); ++it2) {
      // std::cout <<"tâche : " <<SD_task_get_name(it2->first) <<"\n";

      task = it2->first;
      sg_host_t *inter = SD_task_get_workstation_list(mCommRootDest[task]);
      // si l'hôte concerné est l'hôte qu'on est en train de regarder, ajouter
      // le fichier à la quantité de fichiers à transmettre
      if (inter[0] == *it) {
        amountData += SD_task_get_amount(task);

        // std::cout <<"       récolte, tâche : " <<SD_task_get_name(task)
        // <<"qui débute à " <<SD_task_get_start_time(task) <<", qui finit à "
        // <<SD_task_get_finish_time(task) << " ; elle a pour destination "
        // <<SD_task_get_name(mCommRootDest[task]) <<" qui se trouve sur : "
        // <<sg_host_get_name(inter[0]) <<" et a une quantité de : "
        // <<SD_task_get_amount(task) <<"\n";
      }
    }

    transfTime = amountData / bwGlob;

    //     std::cout<< "Temps de transfert avant fermeture : " <<transfTime <<
    //     "\n";

    // temps de calcul total :
    // heure de fin + transfTime - heure de début
    // double totalTimeCalcVM =
    // SD_task_get_finish_time(sg_host_get_last_scheduled_task(*it)) +
    // transfTime
    // - SD_task_get_start_time(tmpTsk); // faux : ce qui est schedulé en
    // premier n'est pas nécessairement ce qui est lancé en premier
    double totalTimeCalcVM =
        SD_task_get_finish_time(ends[*it]) + transfTime -
        SD_task_get_start_time(starts[*it]); // + temps de transfert depuis root

    std::cout << "temps de calcul total : " << totalTimeCalcVM << "\n";
    tpsVMH[*it] = totalTimeCalcVM;

    // pourcentage d'utilisation de la VM :
    if (tpsVMH[*it] != 0) {
      //     std::cout<<"pourcentage utilisation : " <<100* tpsVMCalc[*it] /
      //     tpsVMH[*it] <<"\n";
      pourcentUsed += tpsVMCalc[*it] / tpsVMH[*it];
    } else {
      // si ça arrive et que la VM est utilisée, c'est qu'on est dans le cas où
      // il n'y a que les tâches vides end et start pourcentUsed += 1;
      pourcentUsed += 0;
    }

    // coût associé : temps de calcul * coût horaire + coût ini
    double costVM = getHC_iniC(mhc[*it]) + getHC_hC(mhc[*it]) * totalTimeCalcVM;
    //      std::cout <<"cout total VM : " <<costVM <<"\n";
    costTotVM += costVM;
    //      std::cout <<"cout total actuel : " <<costTotVM <<"\n";
    //      std::cout <<"\n";
  } // end of the for on the used hosts

  // cost for the data
  size_t cursorComm;
  // plus le prix des données :
  // pour chaque mois, payer quantité d'infos * coût de stockage par Go
  int nbMonths = 1 + (finWf - hStart) / (3600 * 24 * 30);
  //======================================================

  double costData = nbMonths * amountDataIO / 1000000000 * STORE_COST +
                    amountDataIO / 1000000000 * TRANSF_COST;
  costTotVM += costData;
}

/**
 * Calcule divers indicateurs concernant le wf exécuté donné + donne des
 *informations précises pour chaque type de VM tpsVMCalc : temps de calcul pur
 *par machine tpsVMH : temps de calcul total de la VM (utilisé pour le calcul du
 *coût) nbVM : nombre de VMs utilisées pourcentUsed : pourcentage total des VMs
 *utilisées ayant servi uniquement au calcul costTotVM : coût dû à l'utilisation
 *des VMs pour l'ensemble des VMs
 **/
void costTimeResultsVerbose(std::map<sg_host_t, HostCost> mhc,
                            std::map<SD_task_t, SD_task_t> &mCommRootDest,
                            std::map<sg_host_t, SD_task_t> &starts,
                            std::map<sg_host_t, SD_task_t> &ends, Workflow wf,
                            double &finWf, double &hStart, long &nbTsk,
                            std::set<sg_host_t> &usedHosts, sg_host_t *hosts,
                            std::map<sg_host_t, double> &tpsVMCalc,
                            double &nbVM, double &costTotVM,
                            double &pourcentUsed,
                            std::map<sg_host_t, double> &tpsVMTot) {
  // ATTENTION : le code de cette procédure a été repris de la procédure
  // costTimeResults dans l'objectif de bricoler un hotfix pour chopper plus
  // d'info sur les VMs. cette solution a été préférée à un appel à cette
  // fonction afin d'éviter de multiplier les parcours de listes et les
  // duplications de calculs

  unsigned int cursor;
  SD_task_t task;
  double amountDataIO = 0;

  // récupération des premières et dernières tâches pour chaque host : conservé
  // (de toute façon, on a besoin de faire cette boucle pour d'autres calculs
  xbt_dynar_foreach(getDagWf(wf), cursor, task) {

    sg_host_t *task_host = SD_task_get_workstation_list(
        task); // !!!! the comparison with the first host of the list works only
               // because there is at most one VM for one task

    // tache de fin = heure de fin
    if (strcmp(SD_task_get_name(task), "end") == 0) {
      //      std::cout <<"tâche end !\n";
      //      std::cout <<"début de la tâche end : "
      //      <<SD_task_get_start_time(task) <<"\n";
      finWf = SD_task_get_start_time(task);

      // transfert de fichiers du cloud vers l'extérieur
      xbt_dynar_t endParents = SD_task_get_parents(task);
      size_t cursorPar;
      SD_task_t ePar;
      xbt_dynar_foreach(endParents, cursorPar, ePar) {
        // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
        // <<SD_task_get_name(sChild) <<"\n";
        amountDataIO += SD_task_get_amount(ePar);
      }
      xbt_dynar_free_container(&endParents);
    }
    if (strcmp(SD_task_get_name(task), "root") == 0) {
      //      std::cout <<"tâche root !\n";
      hStart = SD_task_get_start_time(task);
      // std::cout <<"       récolte, tâche : " <<SD_task_get_name(task) <<" sur
      // la machine : " <<sg_host_get_name(task_host[0]) <<"qui débute à "
      // <<SD_task_get_start_time(task) <<", qui finit à "
      // <<SD_task_get_finish_time(task) <<"\n";

      // transferts de fichiers de l'extérieur du cloud vers le cloud
      xbt_dynar_t startChildren = SD_task_get_children(task);
      size_t cursorChild;
      SD_task_t sChild;
      xbt_dynar_foreach(startChildren, cursorChild, sChild) {
        // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
        // <<SD_task_get_name(sChild) <<"\n";
        amountDataIO += SD_task_get_amount(sChild);
      }
      xbt_dynar_free_container(&startChildren);
    }

    //-----------------------------------------------------------
    // S'il s'agit des tâches fantômes root et end, NE PAS LES PRENDRE EN
    // COMPTE, ce ne sont pas de "vraies" tâches, idem si la tâche est une des
    // comm de root
    if ((strcmp(SD_task_get_name(task), "root") != 0) &&
        (strcmp(SD_task_get_name(task), "end") != 0) &&
        (mCommRootDest.find(task) == mCommRootDest.end())) {
      // récupération des tâches de départ : si l'hôte n'a pas encore été
      // répertorié comme ayant une première tâche, l'ajouter
      if (starts.find(task_host[0]) == starts.end()) {
        starts[task_host[0]] = task;
      } else {
        if (SD_task_get_start_time(starts[task_host[0]]) >
            SD_task_get_start_time(task)) {
          starts[task_host[0]] = task;
        }
      }

      // récupération des tâches de fin
      if (ends.find(task_host[0]) == ends.end()) {
        ends[task_host[0]] = task;
      } else {
        // if(SD_task_get_finish_time(ends[task_host[0]]) <
        // SD_task_get_finish_time(task)){
        if ((strcmp(SD_task_get_name(task), "end") != 0) &&
            (SD_task_get_finish_time(ends[task_host[0]]) <
             SD_task_get_finish_time(task))) {
          ends[task_host[0]] = task;
        }
      }

      // récupération du nombre de tâches total
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {
        nbTsk++;
      }

      // récupération du nombre d'hôtes utilisés
      if (tpsVMCalc.find(task_host[0]) == tpsVMCalc.end()) {
        // initialisation du temps de calcul
        tpsVMCalc[task_host[0]] = 0.;

        // ajout de l'hôte à la liste des hôtes
        usedHosts.insert(task_host[0]);
      }

      // récupération du temps de calcul pur par machine (sans compter les
      // transferts et les attentes)
      tpsVMCalc[task_host[0]] +=
          SD_task_get_amount(task) / sg_host_speed(task_host[0]);
    } // end not root, not end
      //-----------------------------------------------

  } // end of the foreach task

  for (std::set<sg_host_t>::iterator it = usedHosts.begin();
       it != usedHosts.end(); ++it) {
    nbVM++;
    //    std::cout << ' ' << sg_host_get_name(*it);
    // récupération de la première tâche de chaque machine
    // SD_task_t tmpTsk = starts[*it];
    //            std::cout <<"première tâche : " <<SD_task_get_name(tmpTsk)
    //            <<", heure de début : " <<SD_task_get_start_time(tmpTsk)
    //            <<"\n";
    // fin d'exec de machine = tous les fichiers ont été envoyés, envoi
    // séquentiel

    //            std::cout<< "dernière tâche schedulée : "
    //            <<SD_task_get_name(sg_host_get_last_scheduled_task(*it)) <<",
    //            heure de fin de calcul de ladite tâche : "
    //            <<SD_task_get_finish_time(sg_host_get_last_scheduled_task(*it))
    //            <<"\n"; std::cout<< "dernière tâche effectuée : "
    //            <<SD_task_get_name(ends[*it]) <<", heure de fin de calcul de
    //            ladite tâche : " <<SD_task_get_finish_time(ends[*it]) <<"\n";

    // quantité de données à transférer avant fermeture :
    xbt_dynar_t children =
        SD_task_get_children(sg_host_get_last_scheduled_task(*it));
    double transfTime = 0.;
    double amountData = 0.;
    // truandage sur la bande passante, vu que je sais que c'est la même partout
    // et que j'ai plusieurs hôtes :
    double bwGlob = sg_host_route_bandwidth(hosts[0], hosts[1]);

    std::map<sg_host_t, double> tpsVMH;

    if (!xbt_dynar_is_empty(children)) {
      SD_task_t child;
      int iChild;

      xbt_dynar_foreach(children, iChild, child) {
        if (SD_task_get_kind(child) == SD_TASK_COMM_E2E) {
          amountData += SD_task_get_amount(child);
          //	    std::cout<< "data à transférer avant fermeture : "
          //<<amountData << "\n";
        } // endif comm

      } // end foreach child

    } // endif there are children

    xbt_dynar_free_container(&children);

    // récupération des temps liés aux transferts provenant de root (comptés
    // dans le prix, essentiel pour avoir les vrais coûts par VMs mais qui ont
    // eu le mauvais goût d'être artificiellement placés sur la VM de root)
    size_t cursorComm;

    for (std::map<SD_task_t, SD_task_t>::iterator it2 = mCommRootDest.begin();
         it2 != mCommRootDest.end(); ++it2) {
      // std::cout <<"tâche : " <<SD_task_get_name(it2->first) <<"\n";

      task = it2->first;
      sg_host_t *inter = SD_task_get_workstation_list(mCommRootDest[task]);
      // si l'hôte concerné est l'hôte qu'on est en train de regarder, ajouter
      // le fichier à la quantité de fichiers à transmettre
      if (inter[0] == *it) {
        amountData += SD_task_get_amount(task);

        // std::cout <<"       récolte, tâche : " <<SD_task_get_name(task)
        // <<"qui débute à " <<SD_task_get_start_time(task) <<", qui finit à "
        // <<SD_task_get_finish_time(task) << " ; elle a pour destination "
        // <<SD_task_get_name(mCommRootDest[task]) <<" qui se trouve sur : "
        // <<sg_host_get_name(inter[0]) <<" et a une quantité de : "
        // <<SD_task_get_amount(task) <<"\n";
      }
    }

    transfTime = amountData / bwGlob;

    //     std::cout<< "Temps de transfert avant fermeture : " <<transfTime <<
    //     "\n";

    // temps de calcul total :
    // heure de fin + transfTime - heure de début
    // double totalTimeCalcVM =
    // SD_task_get_finish_time(sg_host_get_last_scheduled_task(*it)) +
    // transfTime
    // - SD_task_get_start_time(tmpTsk); // faux : ce qui est schedulé en
    // premier n'est pas nécessairement ce qui est lancé en premier
    double totalTimeCalcVM =
        SD_task_get_finish_time(ends[*it]) + transfTime -
        SD_task_get_start_time(starts[*it]); // + temps de transfert depuis root

    std::cout << "temps de calcul total : " << totalTimeCalcVM << "\n";
    tpsVMH[*it] = totalTimeCalcVM;

    // pourcentage d'utilisation de la VM :
    if (tpsVMH[*it] != 0) {
      //     std::cout<<"pourcentage utilisation : " <<100* tpsVMCalc[*it] /
      //     tpsVMH[*it] <<"\n";
      pourcentUsed += tpsVMCalc[*it] / tpsVMH[*it];
    } else {
      // si ça arrive et que la VM est utilisée, c'est qu'on est dans le cas où
      // il n'y a que les tâches vides end et start pourcentUsed += 1;
      pourcentUsed += 0;
    }

    // coût associé : temps de calcul * coût horaire + coût ini
    double costVM = getHC_iniC(mhc[*it]) + getHC_hC(mhc[*it]) * totalTimeCalcVM;
    //      std::cout <<"cout total VM : " <<costVM <<"\n";
    costTotVM += costVM;
    //      std::cout <<"cout total actuel : " <<costTotVM <<"\n";
    //      std::cout <<"\n";

    // temps d'utilisation de l'hôte : remplissage de la map
    tpsVMTot[*it] = totalTimeCalcVM;

  } // end of the for on the used hosts

  // cost for the data
  size_t cursorComm;
  // plus le prix des données :
  // pour chaque mois, payer quantité d'infos * coût de stockage par Go
  int nbMonths = 1 + (finWf - hStart) / (3600 * 24 * 30);
  //======================================================

  double costData = nbMonths * amountDataIO / 1000000000 * STORE_COST +
                    amountDataIO / 1000000000 * TRANSF_COST;
  costTotVM += costData;
}

/**
 * Simule un ordonnancement et en retourne le makespan et le coût
 **/
void getMakespanCostSched(Workflow &wf, SD_task_t ordoTasks[],
                          std::map<SD_task_t, sg_host_t> &m, double deadline,
                          double budget, double cagnotte,
                          std::map<sg_host_t, HostCost> mhc,
                          double &resMakespan, double &resCost) {

  //===============================================================================
  // to keep which communications are sent by root and where to count them
  //-----------------------------------------------------------------------
  std::map<SD_task_t, SD_task_t>
      mCommRootDest; // map <communication from root, dest task>
  SD_task_t task;
  size_t cursorComm;
  xbt_dynar_foreach(getDagWf(wf), cursorComm, task) {
    if (strcmp(SD_task_get_name(task), "root") == 0) {
      xbt_dynar_t startChildren = SD_task_get_children(task);
      SD_task_t sChild, sGChild;
      size_t cursorChild, cursorGC;
      xbt_dynar_foreach(startChildren, cursorChild, sChild) {
        // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
        // <<SD_task_get_name(sChild) <<"\n";
        xbt_dynar_t startGC = SD_task_get_children(sChild);

        xbt_dynar_foreach(startGC, cursorGC, sGChild) {
          // std::cout <<"             petit enfant de "<<SD_task_get_name(task)
          // <<" et enfant de "<<SD_task_get_name(sChild) <<" : "
          // <<SD_task_get_name(sGChild) <<"\n";

          mCommRootDest[sChild] = sGChild;

          xbt_dynar_free_container(&startGC);
        }
      }
      xbt_dynar_free_container(&startChildren);
    }
  }
  size_t nbTasks =
      get_nb_calc_tasks(getDagWf(wf)); // number of tasks in the workflow

  //=============================================================================================
  // let's fork
  //------------
  int fd[2];
  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    close(fd[0]); /* close read */

    //==========================================================================================
    // allocation of the tasks to their respective host in the order given by
    // the algorithm
    //--------------------------------------------------------------------------------------
    double iniComp;   // equivalent in number of instructions of the time needed
                      // to start the VM
    double newAmount; // new amount of work for the task (start time included)
    for (size_t i = 0; i < nbTasks; i++) {
      //      std::cout <<"      tâche : " <<SD_task_get_name(ordoTasks[i]) <<"
      //      d'indice : " <<i <<"  sur l'hôte : "
      //      <<sg_host_get_name(m[ordoTasks[i]])  <<"\n";

      // If the scheduled task start a new VM, add the start time
      if (sg_host_get_last_scheduled_task(m[ordoTasks[i]]) == NULL) {
        iniComp =
            getHC_tmpIni(mhc[m[ordoTasks[i]]]) * sg_host_speed(m[ordoTasks[i]]);
        newAmount = SD_task_get_amount(ordoTasks[i]) + iniComp;
        SD_task_set_amount(ordoTasks[i], newAmount);
      }

      //=============================================================
      // si on ne commence pas une nouvelle vm, lier la dernière tâche de la VM
      // à la nouvelle tâche qu'on lui assigne
      else {
        // mini-patch: keeps the order given by the scheduling algorithm ?
        //  TODO: checker les minminbudg : apparemment
        if (!SD_task_dependency_exists(
                sg_host_get_last_scheduled_task(m[ordoTasks[i]]),
                ordoTasks[i])) {
          SD_task_dependency_add(
              "orderConservation", NULL,
              sg_host_get_last_scheduled_task(m[ordoTasks[i]]), ordoTasks[i]);
        }
      }
      //=============================================================

      SD_task_schedulel(ordoTasks[i], 1, m[ordoTasks[i]]);
      sg_host_set_last_scheduled_task(m[ordoTasks[i]], ordoTasks[i]);
    }

    // worstValues(wf);
    SD_simulate(-1);

    //-----------------------------------------
    // calculus of the costs in time and money
    double costTotVM = 0.;
    double pourcentUsed = 0.;
    std::set<sg_host_t> usedHosts;         // liste des hôtes utilisés
    std::map<sg_host_t, double> tpsVMCalc; // récupération des temps de calcul
    double nbVM = 0.;
    long nbTsk = 0;
    double finWf = 0.;
    double hStart = 0.;
    std::map<sg_host_t, SD_task_t>
        starts; // tâches d'initialisation : pour récupérer les heures de début
                // de lancement de machine + début de temps de premier
                // téléchargement
    std::map<sg_host_t, SD_task_t> ends; // tâches de fin de VM
    sg_host_t *hosts = sg_host_list();

    costTimeResults(mhc, mCommRootDest, starts, ends, wf, finWf, hStart, nbTsk,
                    usedHosts, hosts, tpsVMCalc, nbVM, costTotVM, pourcentUsed);

    double time = finWf - hStart;

    //-----------------------------------------
    // sending the result to the parent
    if (write(fd[1], &time, sizeof(double)) <= 0) {
      printf("Error write time for cheapest ordo\n");
      exit(-1);
    }

    if (write(fd[1], &costTotVM, sizeof(double)) <= 0) {
      printf("Error write cost for cheapest ordo\n");
      exit(-1);
    }

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    // freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    // deleteWf(wf);
    size_t total_nhosts = sg_host_count();

    for (size_t cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } // end child
  else {
    close(fd[1]); /* close write */
    double dTest = -2;

    // receving the result
    if (read(fd[0], &dTest, sizeof(double)) <= 0) {
      printf("Error read time for first attribution\n");
      exit(-1);
    }
    resMakespan = dTest;

    if (read(fd[0], &dTest, sizeof(double)) <= 0) {
      printf("Error read cost for  first attribution\n");
      exit(-1);
    }

    resCost = dTest;

    //    std::cout << "j'ai makespan = " <<resMakespan <<" et cost = "
    //    <<resCost <<"\n";

    int stat_val;
    waitpid(childpid, &stat_val, 0);

  } // end parent

  close(fd[0]);
}

void getMakespanCostSched_avec_tps(Workflow &wf, SD_task_t ordoTasks[],
                                   std::map<SD_task_t, sg_host_t> &m,
                                   double deadline, double budget,
                                   double cagnotte,
                                   std::map<sg_host_t, HostCost> mhc,
                                   double &resMakespan, double &resCost,
                                   std::map<SD_task_t, double> &list_T) {

  //===============================================================================
  // to keep which communications are sent by root and where to count them
  //-----------------------------------------------------------------------
  std::map<SD_task_t, SD_task_t>
      mCommRootDest; // map <communication from root, dest task>

  SD_task_t task;
  size_t cursorComm;
  xbt_dynar_foreach(getDagWf(wf), cursorComm, task) {
    if (strcmp(SD_task_get_name(task), "root") == 0) {
      xbt_dynar_t startChildren = SD_task_get_children(task);
      SD_task_t sChild, sGChild;
      size_t cursorChild, cursorGC;
      xbt_dynar_foreach(startChildren, cursorChild, sChild) {
        // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
        // <<SD_task_get_name(sChild) <<"\n";
        xbt_dynar_t startGC = SD_task_get_children(sChild);

        xbt_dynar_foreach(startGC, cursorGC, sGChild) {
          // std::cout <<"             petit enfant de "<<SD_task_get_name(task)
          // <<" et enfant de "<<SD_task_get_name(sChild) <<" : "
          // <<SD_task_get_name(sGChild) <<"\n";

          mCommRootDest[sChild] = sGChild;

          xbt_dynar_free_container(&startGC);
        }
      }
      xbt_dynar_free_container(&startChildren);
    }
  }
  size_t nbTasks =
      get_nb_calc_tasks(getDagWf(wf)); // number of tasks in the workflow

  //=============================================================================================
  // let's fork
  //------------
  int fd[2];

  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    close(fd[0]); /* close read */

    //==========================================================================================
    // allocation of the tasks to their respective host in the order given by
    // the algorithm
    //--------------------------------------------------------------------------------------
    double iniComp;   // equivalent in number of instructions of the time needed
                      // to start the VM
    double newAmount; // new amount of work for the task (start time included)
    for (size_t i = 0; i < nbTasks; i++) {
      //      std::cout <<"      tâche : " <<SD_task_get_name(ordoTasks[i]) <<"
      //      d'indice : " <<i <<"  sur l'hôte : "
      //      <<sg_host_get_name(m[ordoTasks[i]])  <<"\n";

      // If the scheduled task start a new VM, add the start time
      if (sg_host_get_last_scheduled_task(m[ordoTasks[i]]) == NULL) {
        iniComp =
            getHC_tmpIni(mhc[m[ordoTasks[i]]]) * sg_host_speed(m[ordoTasks[i]]);
        newAmount = SD_task_get_amount(ordoTasks[i]) + iniComp;
        SD_task_set_amount(ordoTasks[i], newAmount);
      }

      //=============================================================
      // si on ne commence pas une nouvelle vm, lier la dernière tâche de la VM
      // à la nouvelle tâche qu'on lui assigne
      else {
        // mini-patch: keeps the order given by the scheduling algorithm ?
        //  TODO: checker les minminbudg : apparemment
        if (!SD_task_dependency_exists(
                sg_host_get_last_scheduled_task(m[ordoTasks[i]]),
                ordoTasks[i])) {
          SD_task_dependency_add(
              "orderConservation", NULL,
              sg_host_get_last_scheduled_task(m[ordoTasks[i]]), ordoTasks[i]);
        }
      }
      //=============================================================

      SD_task_schedulel(ordoTasks[i], 1, m[ordoTasks[i]]);
      sg_host_set_last_scheduled_task(m[ordoTasks[i]], ordoTasks[i]);
    }

    // worstValues(wf);
    SD_simulate(-1);

    //-----------------------------------------
    // calculus of the costs in time and money
    double costTotVM = 0.;
    double pourcentUsed = 0.;
    std::set<sg_host_t> usedHosts;         // liste des hôtes utilisés
    std::map<sg_host_t, double> tpsVMCalc; // récupération des temps de calcul
    double nbVM = 0.;
    long nbTsk = 0;
    double finWf = 0.;
    double hStart = 0.;
    std::map<sg_host_t, SD_task_t>
        starts; // tâches d'initialisation : pour récupérer les heures de début
                // de lancement de machine + début de temps de premier
                // téléchargement
    std::map<sg_host_t, SD_task_t> ends; // tâches de fin de VM
    sg_host_t *hosts = sg_host_list();

    costTimeResults(mhc, mCommRootDest, starts, ends, wf, finWf, hStart, nbTsk,
                    usedHosts, hosts, tpsVMCalc, nbVM, costTotVM, pourcentUsed);

    double time = finWf - hStart;

    unsigned int cursor;
    int nb = 0;
    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      /*if(isTaskComput(task)){
     std::cout<<"___________________________________________________________________________"<<std::endl;
     std::cout<<"TACHE : "<<SD_task_get_name(task)<<" ANCIEN TPS D'EXEC : "<<
     SD_task_get_finish_time(task) - SD_task_get_start_time(task) <<std::endl;
      }*/
      double _tps =
          (SD_task_get_finish_time(task) - SD_task_get_start_time(task));
      nb++;

      write(fd[1], &_tps, sizeof(double));
    }

    /*xbt_dynar_foreach(getDagWf(wf), cursorComm, task){
      std::cout<<"___________________________________________________________________________"<<std::endl;
      std::cout<<"TACHE : "<<SD_task_get_name(task)<<std::endl;
      std::cout<<"OOOOOOOOOOOOOOOOOOOOOOOO   STARTTIME :
    "<<SD_task_get_start_time(task)<<std::endl;
      std::cout<<"OOOOOOOOOOOOOOOOOOOOOOOO   FINISHTIME :
    "<<SD_task_get_finish_time(task)<<std::endl; std::cout<<"TPS ATTENDU :
    "<<SD_task_get_finish_time(task)-SD_task_get_start_time(task)<<std::endl;
      tps_dexec.insert(std::pair<SD_task_t, double>(task,
    SD_task_get_finish_time(task)-SD_task_get_start_time(task))) ;
      std::cout<<"TPS ATTENDU2 : "<<tps_dexec[task]<<std::endl;
    }*/

    //-----------------------------------------
    // sending the result to the parent
    if (write(fd[1], &time, sizeof(double)) <= 0) {
      printf("Error write time for cheapest ordo\n");
      exit(-1);
    }

    if (write(fd[1], &costTotVM, sizeof(double)) <= 0) {
      printf("Error write cost for cheapest ordo\n");
      exit(-1);
    }

    std::cout << " On se prépare à écrire" << std::endl;

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    // freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    // deleteWf(wf);
    size_t total_nhosts = sg_host_count();

    for (size_t cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } // end child
  else {
    close(fd[1]); /* close write */
    std::cout << "Close write" << std::endl;
    double dTest = -2;

    // receving the result

    std::map<SD_task_t, double> list_T2;
    unsigned int cursor;
    xbt_dynar_t dag = getDagWf(wf);
    // int nb = 0;
    xbt_dynar_foreach(dag, cursor, task) {
      if (read(fd[0], &dTest, sizeof(double)) <= 0) {
        printf("Error read time for first attribution\n");
        exit(-1);
      }
      // nb ++;
      // std::cout<<" On est à la "<<nb<< " tâche"<<std::endl;
      // std::cout<<"Je lis : "<< dTest << std::endl;
      list_T2[task] = dTest;
    }
    xbt_dynar_foreach(dag, cursor, task) {
      if (isTaskComput(task)) {
        double max = 0.0;
        xbt_dynar_t children = SD_task_get_children(task);
        SD_task_t child;
        size_t cursorChild;
        xbt_dynar_foreach(children, cursorChild, child) {
          if (SD_task_get_kind(child) == SD_TASK_COMM_E2E) {
            max = MAX(max, list_T2[child]);
          }
          // std::cout<<"ENFANT : "<<SD_task_get_name(child)<<"    ::     PARENT
          // :"<<SD_task_get_name(task)  <<"   MAX = "<<max<<std::endl;
        }
        xbt_dynar_free_container(&children);

        list_T[task] = list_T2[task] + max;
        // std::cout<<"TPS de comm total : "<<list_T[task] <<"  =   tps de comm
        // : " <<list_T2[task]<< "   +     max : "<<max<<std::endl;;
      }
    }

    std::cout << "Fini de lire le tableau" << std::endl;

    if (read(fd[0], &dTest, sizeof(double)) <= 0) {
      printf("Error read time for first attribution\n");
      exit(-1);
    }
    resMakespan = dTest;

    if (read(fd[0], &dTest, sizeof(double)) <= 0) {
      printf("Error read cost for  first attribution\n");
      exit(-1);
    }

    resCost = dTest;
    //    std::cout << "j'ai makespan = " <<resMakespan <<" et cost = "
    //    <<resCost <<"\n";

    int stat_val;
    waitpid(childpid, &stat_val, 0);

  } // end parent
  close(fd[0]);
}

SD_task_t get_root(xbt_dynar_t dag) {
  SD_task_t task, root;
  int i;
  xbt_dynar_foreach(dag, i, task) {
    if (strcmp(SD_task_get_name(task), "root") == 0) {
      root = task;
      break;
    }
  }
  return root;
}

// TODO : virer
// issu d'un global qui devrait en toute logique être dans la lib de base mais
// n'est, pour une mystérieuse raison, pas reconnu... Trouvé ! Elle n'est pas
// reconnue car il s'agit de la procédure de la version 3.16 (changement de
// signature)
void SD_simulate_with_update(double how_long, xbt_dynar_t changed_tasks_dynar) {
  std::set<SD_task_t> *changed_tasks = simgrid::sd::simulate(how_long);
  for (const auto &task : *changed_tasks) {
    xbt_dynar_push(changed_tasks_dynar, &task);
  }
}

//===========================================================================
// variations sur la thématique du chemin critique
//--------------------------------------------------

/**
 * create the list containing the tasks of the given dag, sorted in topological
 * order
 **/
void createTopoOrder(xbt_dynar_t dag, SD_task_t task,
                     std::list<SD_task_t> &topoOrder,
                     std::map<SD_task_t, bool> &visited) {
  // task visited
  visited[task] = true;

  // for all the childre of the task:
  std::list<SD_task_t> lChildren;
  getRunnableChildren(dag, task, lChildren);
  for (std::list<SD_task_t>::iterator it = lChildren.begin();
       it != lChildren.end(); ++it) {

    if (!visited[*it]) {
      createTopoOrder(dag, *it, topoOrder, visited);
    }
  }
  if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {
    topoOrder.push_front(task);
  }
}

/**
 * Get the list containing the tasks of the critical path of the given dag,
 * sorted in topological order
 * dag : a wf which has been calculated
 * topoOrder : the list which will contain the tasks of the critical path
 * copyDag : copie non exécutée du dag à analyser
 * dicoTsk : map nom -> tâche du dag à analyser
 **/
void getCPTopoOrder(xbt_dynar_t dag, std::list<SD_task_t> &critPath,
                    xbt_dynar_t copyDag,
                    std::map<std::string, SD_task_t> dicoTsk,
                    std::list<SD_task_t> &topoOrder) {

  std::cout << "\ttopological order !!!!! "
            << "\n";
  int cpt;
  SD_task_t task, taskRoot;
  std::map<SD_task_t, bool> visited;

  // dicoCopyDag :
  std::map<std::string, SD_task_t> dicoCopyTsk;

  // ini of the map
  xbt_dynar_foreach(copyDag, cpt, task) {
    visited[task] = false;
    dicoCopyTsk[SD_task_get_name(task)] = task;
  }

  // TODO ==============================================================
  /* Restauration de l'ordre d'exécution des tâches et application au
   * workflow copié qu'on parcourt
   */

  std::map<sg_host_t, int> mVMTsk;
  std::vector<std::vector<SD_task_t>> hostTsk;
  SD_task_t tsk;
  size_t comptMVMTsk;

  // std::cout <<"\n ================ parcours pour map VM tasks
  // ======================= \n";

  // trier en lisant : faire une map <host, list<Tsk>>
  // comparer les get_start_time à fin de liste et début de liste, et insérer là
  // où il faut (ordre croissant)
  int nbH = 0;
  xbt_dynar_foreach(dag, comptMVMTsk, tsk) {
    if (SD_task_get_kind(tsk) == SD_TASK_COMP_SEQ) {
      if ((SD_task_get_state(tsk) != SD_SCHEDULABLE) &&
          (SD_task_get_state(tsk) != SD_NOT_SCHEDULED)) {
        sg_host_t *lh = SD_task_get_workstation_list(tsk);
        // std::cout<<"map host --> tâches de l'host : tâche "
        // <<SD_task_get_name(tsk) <<", sur l'hôte " <<sg_host_get_name(lh[0])
        // <<"\n";
        // si liste vide, ajouter l'élément
        // sinon, l'ajouter au début ou à la fin (si heure début tsk < heure
        // début premiere tache de la liste, ajouter devant, sinon tester la
        // tâche suivante dans la liste

        // si l'hôte n'a jamais été vu, l'ajouter
        if (mVMTsk.find(lh[0]) == mVMTsk.end()) {
          // std::cout << "hôte tout neuf, nbh = " <<nbH<<"\n";
          mVMTsk[lh[0]] = nbH;

          hostTsk.push_back(std::vector<SD_task_t>());
          hostTsk[nbH].push_back(tsk);
          nbH++;
        } else {
          // récupérer l'indice de l'hôte
          int indiceH = mVMTsk[lh[0]];

          // std::cout << "hôte déjà vu\n";
          // l'hôte qui nous intéresse, c'est hostTsk[indiceH]

          // si l'élément commence plus tard que le dernier élément de la liste,
          // le mettre à la fin, sinon il va falloir se préparer à parcourir
          // tout le bazar
          if (SD_task_get_start_time(tsk) >
              SD_task_get_start_time(hostTsk[indiceH].back())) {
            // mVMTsk[lh[0]].push_back(tsk);
            // std::cout <<"plus tardif que le plus tardif, tsk : " <<
            // SD_task_get_name(tsk) <<" à temps " <<SD_task_get_start_time(tsk)
            // <<" ; ex dernière : " <<SD_task_get_name(hostTsk[indiceH].back())
            // << " à temps : "
            // <<SD_task_get_start_time(hostTsk[indiceH].back())
            // <<"\n";
            hostTsk[indiceH].push_back(tsk);
          } else {
            for (std::vector<SD_task_t>::iterator ita =
                     hostTsk[indiceH].begin();
                 ita != hostTsk[indiceH].end(); ++ita) {
              // si temps inférieur, le mettre devant : décrémenter de 1 et
              // insert, break;
              if (SD_task_get_start_time(tsk) < SD_task_get_start_time(*ita)) {

                // cas où la liste est vide
                // if(ita == hostTsk[indiceH].begin()){
                if (hostTsk[indiceH].size() == 0) {
                  hostTsk[indiceH].push_back(tsk);
                  break;
                }
                // cas où il y a au moins un élément dans la liste
                else {

                  // faire le lien entre la dernière tâche du vector et la tâche
                  // actuelle
                  /*
                    // s'il n'existe pas de lien entre, dans le copyDag, la
                    tâche déjà existante et la tâche en cours, ajouter un lien

                    // tâche concernée dans le copyDag :
                    //dicoCopyTsk[SD_task_get_name(tsk)]

                    // dernière tâche du vector :
                    //dicoCopyTsk[SD_task_get_name(hostTsk[indiceH].back())]

                  */

                  if ((!SD_task_dependency_exists(
                          dicoCopyTsk[SD_task_get_name(
                              hostTsk[indiceH].back())],
                          dicoCopyTsk[SD_task_get_name(tsk)])) &&
                      (strcmp("end", SD_task_get_name(tsk)) != 0)) {
                    SD_task_dependency_add(
                        "orderConservationCalcTopo", NULL,
                        dicoCopyTsk[SD_task_get_name(hostTsk[indiceH].back())],
                        dicoCopyTsk[SD_task_get_name(tsk)]);
                    // std::cout <<"lien ajouté !\n";
                    // ... ou pas. check :
                    /*
                     xbt_dynar_t childrenTest =
                     SD_task_get_children(dicoCopyTsk[SD_task_get_name(hostTsk[indiceH].back())]);
                     size_t compteurTest;
                     SD_task_t childTest;
                     xbt_dynar_foreach(childrenTest, compteurTest, childTest){
                       std::cout <<"\t\t~~~~~~~ check de liens, tâche prec : "
                     <<SD_task_get_name(hostTsk[indiceH].back()) <<" à lier avec
                     : " <<SD_task_get_name(tsk) <<" fils : "
                     <<SD_task_get_name(childTest) <<"\n";
                     }
                     */
                  }

                  // std::cout << "tâche " <<SD_task_get_name(tsk) <<", de temps
                  // : " <<SD_task_get_start_time(tsk) <<", placée avant : "
                  // <<SD_task_get_name(*ita) <<", de temps : "<<
                  // SD_task_get_start_time(*ita)<<"\n";
                  ita = hostTsk[indiceH].insert(ita, tsk);
                  break;
                }
              } // fin de si la tâche commence avant l'it
            } // fin du parcours de la liste des tâches qu'on sait être sur la
              // VM de tsk
          }   // fin de si la tâche n'est pas la dernière connue de la VM

        } // fin du else	(liste non vide, insertion à faire)
      }   // fin de si la tâche a été schédulée
    }     // fin de si la tâche est une tâche de calcul

  } // fin du parcours du dag

  // puis parcourir les map VM par VM et créer les liens associés
  for (long unsigned int g = 0; g < hostTsk.size(); g++) {
    // std::cout <<" nouvel host, d'indice :" <<g <<"\n";
    // parcours des tâches associées à l'host
    for (std::vector<SD_task_t>::iterator it = hostTsk[g].begin();
         it != hostTsk[g].end(); ++it) {
      // récupération de l'hôte
      sg_host_t *hst = SD_task_get_workstation_list(*it);
      // std::cout <<"\tvoilà la tâche : " <<SD_task_get_name(*it) <<", sur
      // l'hôte : " <<sg_host_get_name(hst[0]) <<" et qui commence au temps : "
      // <<SD_task_get_start_time(*it) <<"\n";
    }
  }

  // distance = time of calculus + comm

  // Call the recursive helper function to store Topological Sort
  // starting from all vertices one by one
  xbt_dynar_foreach(copyDag, cpt, task) {
    if (!visited[task]) {
      createTopoOrder(copyDag, task, topoOrder, visited);
    }
  }

  // minitest : affichage

  /*
      std::cout <<"\ttopological order : " <<"\n";
      for (std::list<SD_task_t>::iterator it=topoOrder.begin(); it !=
     topoOrder.end(); ++it){ std::cout <<" " <<SD_task_get_name(*it) <<"\n";
      }
  */

  // créer une map <SD_task_t, distance la plus longue>, initialisée à - inf
  // et créer une map <task, last task on longuest path>
  std::map<SD_task_t, double> distance;
  std::map<SD_task_t, std::list<SD_task_t>>
      longuestPaths; // plus long chemin entre la source et la tâche

  xbt_dynar_foreach(dag, cpt, task) {
    distance[task] = DBL_MIN;
    if (strcmp(SD_task_get_name(task), "root") == 0) {
      taskRoot = task;
    }
  }
  distance[taskRoot] = 0;

  /*
    for(std::map<sg_host_t, std::list<SD_task_t>>::iterator it=mVMTsk.begin() ;
    it!=mVMTsk.end() ; ++it){ std::cout <<"\tparcours de map, VM : "
    <<sg_host_get_name(it->first) <<"\n";

    for (std::list<SD_task_t>::iterator ita=(it->second).begin();
    ita!=(it->second).end(); ++ita){ std::cout <<"\t\ttâche "
    <<SD_task_get_name(*ita) <<"\n";
    }

    }
  */
  // END TODO ==========================================================

  SD_task_t child;
  // parcourir topoOrder
  // pour chaque tâche de topoOrder, mettre à jour les distances des tâches
  // suivantes: calculer poids du noeud + arête, et si > à ancienne valeur,
  // stocker :
  // - la nouvelle distance max par rapport au noeud root
  // - le noeud par lequel on y parvient (virer l'ancien, mettre le nouveau)

  // itérer sur la copie, calculer avec les valeurs du vrai
  for (std::list<SD_task_t>::iterator it = topoOrder.begin();
       it != topoOrder.end(); ++it) {

    bool newAdj = true; // booléen indiquant si on a ou non déjà mis une valeur
                        // pour les tâches filles de la tâche actuelle
    xbt_dynar_t children = SD_task_get_children(*it);
    size_t compt1;
    SD_task_t child;
    xbt_dynar_foreach(children, compt1, child) {
      double intermLg = 0.;

      // si on en est bien à cette tâche
      if (distance[dicoTsk[SD_task_get_name(*it)]] != DBL_MIN) {

        // pour stocker la tâche qu'on peut ou non ajouter comme faisant partie
        // du chemin critique :
        SD_task_t taskAEval;
        // si le fils est une tâche de communication, la taille suivante vaudra
        // comm + calc fils de comm
        if (SD_task_get_kind(child) == SD_TASK_COMM_E2E) {
          // récup du fils (obligé de passer par un xbt_dynar_t, mais il ne
          // contient qu'une tâche)
          xbt_dynar_t gChildren = SD_task_get_children(child);
          size_t compt2;
          SD_task_t gChild;
          xbt_dynar_foreach(gChildren, compt2, gChild) {
            // std::cout <<"fille de la tâche "<<SD_task_get_name(*it) <<" : "
            // <<SD_task_get_name(child) <<", petite fille : "
            // <<SD_task_get_name(gChild) <<"\n"; calcul de la "longueur". Ici,
            // on récupère directement le temps de calcul (heure de fin - heure
            // de début) et on y ajoute le temps de comm (heure de fin de la
            // comm - heure de fin de la tâche précédente) tâche réelle *it :
            // dicoTsk[SD_task_get_name(*it)] comm réelle fille :
            // dicoTsk[SD_task_get_name(child)] tâche réelle fille :
            // dicoTsk[SD_task_get_name(gChild)]

            // si *it est root, on fait grâce du temps idle (la VM n'est pas
            // encore réservée)
            if (strcmp(SD_task_get_name(*it), "root") == 0) {
              intermLg +=
                  (SD_task_get_finish_time(dicoTsk[SD_task_get_name(child)]) -
                   SD_task_get_start_time(dicoTsk[SD_task_get_name(child)])) +
                  (SD_task_get_finish_time(dicoTsk[SD_task_get_name(gChild)]) -
                   SD_task_get_start_time(dicoTsk[SD_task_get_name(gChild)]));
            } else {
              // sinon, calcul normal
              intermLg +=
                  (SD_task_get_finish_time(dicoTsk[SD_task_get_name(child)]) -
                   SD_task_get_finish_time(dicoTsk[SD_task_get_name(*it)])) +
                  (SD_task_get_finish_time(dicoTsk[SD_task_get_name(gChild)]) -
                   SD_task_get_start_time(dicoTsk[SD_task_get_name(gChild)]));
            }

            // std::cout <<"\tchild : " <<SD_task_get_name(child) <<" on a une
            // tâche de comm, intermLg = " <<intermLg <<"\n";
            taskAEval = dicoTsk[SD_task_get_name(gChild)];
          }

          xbt_dynar_free_container(&gChildren);
        }      // fin child = tâche de comm
        else { // sinon, on a directement une tâche de calcul et comm = 0
          intermLg +=
              SD_task_get_finish_time(dicoTsk[SD_task_get_name(child)]) -
              SD_task_get_start_time(dicoTsk[SD_task_get_name(child)]);
          // std::cout <<"\tchild : " <<SD_task_get_name(child) <<", on a une
          // tâche de calcul sans comm, intermLg = " <<intermLg <<"\n";
          taskAEval = dicoTsk[SD_task_get_name(child)];
        } // fin child = calc

        // mise à jour de la distance si nécessaire
        if (distance[taskAEval] <
            (distance[dicoTsk[SD_task_get_name(*it)]] + intermLg)) {
          // std::cout << "chemin plus long trouvé, par :"
          // <<SD_task_get_name(*it) <<"\n";
          // remplacer la valeur de distance
          distance[taskAEval] =
              distance[dicoTsk[SD_task_get_name(*it)]] + intermLg;

          // ajouter ou remplacer la tâche dans le chemin critique
          if (newAdj) {
            // ajouter la tâche
            longuestPaths[taskAEval].push_back(dicoTsk[SD_task_get_name(*it)]);

            // mettre newAdj à faux
            newAdj = false;
          } else {
            // remplacer la dernière tâche par celle-ci
            // longuestPaths[taskAEval].pop_back();
            longuestPaths[taskAEval].remove(longuestPaths[taskAEval].back());
            longuestPaths[taskAEval].push_back(dicoTsk[SD_task_get_name(*it)]);
          }
          /*
          std::cout << "mise à jour de distance effectuée, tâche "
          <<SD_task_get_name(taskAEval) <<"\n"; std::cout <<"\t nouvelle plus
          longue distance : " <<distance[taskAEval] <<"\n"; std::cout <<"\t
          nouveau plus long chemin : \n";
          */
          std::list<SD_task_t> lTmp = longuestPaths[taskAEval];
          /*
          for (std::list<SD_task_t>::iterator itTmp=lTmp.begin(); itTmp !=
          lTmp.end(); ++itTmp){ std::cout << "\t\t" <<SD_task_get_name(*itTmp)
          <<"\n";
          }
          */

        } // fin de si une mise à jour de la distance max est à faire

      } // fin de si on en est bien à cette tâche
    }   // fin du passage en revue des fils directs

    xbt_dynar_free_container(&children);

  } // fin de pour chaque tâche dans l'ordre topo

  // tous les plus longs chemins à la source ayant été calculés, récupération du
  // critical path
  SD_task_t longPathTask;
  double bestDist = 0;
  //  std::cout << "taille de la map : " <<distance.size() <<"\n";

  for (std::map<SD_task_t, double>::iterator it = distance.begin();
       it != distance.end(); ++it) {
    if ((SD_task_get_kind(it->first) == SD_TASK_COMP_SEQ) &&
        (strcmp(SD_task_get_name(it->first), "end") != 0) &&
        (strcmp(SD_task_get_name(it->first), "root") != 0)) {
      //      std::cout << SD_task_get_name(it->first) <<", " <<it->first << "
      //      => " << it->second << '\n';
      // mettre la plus grande distance de côté
      if (it->second >= bestDist) {
        bestDist = it->second;
        longPathTask = it->first;
      }
    }
  }

  // copier dans critPath le chemin critique trouvé :
  // longuestPaths[longPathTask] (map <task, list<task>>) parcours de la liste
  // de la tâche longPathTask et ajout des tâches correspondantes à critPath
  for (std::list<SD_task_t>::iterator it = longuestPaths[longPathTask].begin();
       it != longuestPaths[longPathTask].end(); it++) {
    critPath.push_back(*it);
  }
  //  longuestPaths[taskAEval].push_back(dicoTsk[SD_task_get_name(*it)]);
}

/**
 * Simule un ordonnancement et en retourne le makespan, le coût et le chemin
 *critique dans une pile, première tâche de la pile = tâche la plus proche de
 *root appartenant au chemin critique Petit ajout pour parer à la disparition
 *des dépendances entre tâches durant la simulation : copyWf (une copie non
 *exécutée du wf) map <string, SD_task_t> dicoTsk (une map permettant de
 *retrouver les tâches du wf exécuté à partir de leur nom)
 *
 **/
void getMakespanCostCPSched(Workflow &wf, SD_task_t ordoTasks[],
                            std::map<SD_task_t, sg_host_t> &m, double deadline,
                            double budget, double cagnotte,
                            std::map<sg_host_t, HostCost> mhc,
                            double &resMakespan, double &resCost,
                            std::list<SD_task_t> &critPath, Workflow &copyWf,
                            std::map<std::string, SD_task_t> dicoTsk) {

  //===============================================================================
  // to keep which communications are sent by root and where to count them
  //-----------------------------------------------------------------------
  std::map<SD_task_t, SD_task_t>
      mCommRootDest; // map <communication from root, dest task>
  SD_task_t task;
  size_t cursorComm;
  xbt_dynar_foreach(getDagWf(wf), cursorComm, task) {
    if (strcmp(SD_task_get_name(task), "root") == 0) {
      xbt_dynar_t startChildren = SD_task_get_children(task);
      SD_task_t sChild, sGChild;
      size_t cursorChild, cursorGC;
      xbt_dynar_foreach(startChildren, cursorChild, sChild) {
        // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
        // <<SD_task_get_name(sChild) <<"\n";
        xbt_dynar_t startGC = SD_task_get_children(sChild);

        xbt_dynar_foreach(startGC, cursorGC, sGChild) {
          // std::cout <<"             petit enfant de "<<SD_task_get_name(task)
          // <<" et enfant de "<<SD_task_get_name(sChild) <<" : "
          // <<SD_task_get_name(sGChild) <<"\n";

          mCommRootDest[sChild] = sGChild;

          xbt_dynar_free_container(&startGC);
        }
      }
      xbt_dynar_free_container(&startChildren);
    }
  }
  size_t nbTasks =
      get_nb_calc_tasks(getDagWf(wf)); // number of tasks in the workflow

  //=============================================================================================
  // let's fork
  //------------
  int fd[2];
  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    close(fd[0]); /* close read */

    //==========================================================================================
    // allocation of the tasks to their respective host in the order given by
    // the algorithm
    //--------------------------------------------------------------------------------------
    double iniComp;   // equivalent in number of instructions of the time needed
                      // to start the VM
    double newAmount; // new amount of work for the task (start time included)
    for (size_t i = 0; i < nbTasks; i++) {
      //      std::cout <<"      tâche : " <<SD_task_get_name(ordoTasks[i]) <<"
      //      d'indice : " <<i <<"  sur l'hôte : "
      //      <<sg_host_get_name(m[ordoTasks[i]])  <<"\n";

      // If the scheduled task start a new VM, add the start time
      if (sg_host_get_last_scheduled_task(m[ordoTasks[i]]) == NULL) {
        iniComp =
            getHC_tmpIni(mhc[m[ordoTasks[i]]]) * sg_host_speed(m[ordoTasks[i]]);
        newAmount = SD_task_get_amount(ordoTasks[i]) + iniComp;
        SD_task_set_amount(ordoTasks[i], newAmount);
      }

      //=============================================================
      // si on ne commence pas une nouvelle vm, lier la dernière tâche de la VM
      // à la nouvelle tâche qu'on lui assigne
      else {
        // mini-patch: keeps the order given by the scheduling algorithm ?
        //  TODO: checker les minminbudg : apparemment
        if (!SD_task_dependency_exists(
                sg_host_get_last_scheduled_task(m[ordoTasks[i]]),
                ordoTasks[i])) {
          SD_task_dependency_add(
              "orderConservation", NULL,
              sg_host_get_last_scheduled_task(m[ordoTasks[i]]), ordoTasks[i]);
        }
      }
      //=============================================================

      SD_task_schedulel(ordoTasks[i], 1, m[ordoTasks[i]]);
      sg_host_set_last_scheduled_task(m[ordoTasks[i]], ordoTasks[i]);
    }

    // worstValues(wf);
    SD_simulate(-1);

    //-----------------------------------------
    // calculus of the costs in time and money
    double costTotVM = 0.;
    double pourcentUsed = 0.;
    std::set<sg_host_t> usedHosts;         // liste des hôtes utilisés
    std::map<sg_host_t, double> tpsVMCalc; // récupération des temps de calcul
    double nbVM = 0.;
    long nbTsk = 0;
    double finWf = 0.;
    double hStart = 0.;
    std::map<sg_host_t, SD_task_t>
        starts; // tâches d'initialisation : pour récupérer les heures de début
                // de lancement de machine + début de temps de premier
                // téléchargement
    std::map<sg_host_t, SD_task_t> ends; // tâches de fin de VM
    sg_host_t *hosts = sg_host_list();

    costTimeResults(mhc, mCommRootDest, starts, ends, wf, finWf, hStart, nbTsk,
                    usedHosts, hosts, tpsVMCalc, nbVM, costTotVM, pourcentUsed);

    double time = finWf - hStart;

    //-----------------------------------------
    // calc of the critical path
    //---------------------------
    // SD_task_t task, endTask;
    // size_t cursorComm;

    // // getting the ending task
    // xbt_dynar_foreach(getDagWf(wf), cursorComm, task) {
    //   if (strcmp(SD_task_get_name(task), "end") == 0) {
    //     endTask = task;
    //   }
    // }

    // calc the longest path

    // void getCPTopoOrder(xbt_dynar_t dag, std::list<SD_task_t>
    // &topoOrder, xbt_dynar_t copyDag, std::map<std::string, SD_task_t>
    // dicoTsk)
    // getCPTopoOrder(getDagWf(wf), critPath);
    critPath.empty();
    std::list<SD_task_t> topoOrder;
    getCPTopoOrder(getDagWf(wf), critPath, getDagWf(copyWf), dicoTsk,
                   topoOrder);

    // get the number of tasks in the critical path
    size_t nbTasks = critPath.size();

    //-----------------------------------------
    // sending the result to the parent
    if (write(fd[1], &time, sizeof(double)) <= 0) {
      printf("Error write time for cheapest ordo\n");
      exit(-1);
    }

    if (write(fd[1], &costTotVM, sizeof(double)) <= 0) {
      printf("Error write cost for cheapest ordo\n");
      exit(-1);
    }

    std::cout << "nbTasks pré envoi : " << nbTasks << "\n";
    // TODO: send the number of tasks in the critical path
    if (write(fd[1], &nbTasks, sizeof(size_t)) <= 0) {
      printf("Error write time for nbtasks\n");
      exit(-1);
    }

    // TODO: send the names of the tasks involved in the critical path
    for (std::list<SD_task_t>::iterator it = critPath.begin();
         it != critPath.end(); ++it) {
      size_t tasknamelen = strlen(SD_task_get_name(*it));
      if (write(fd[1], &tasknamelen, sizeof(size_t)) <= 0) {
        printf("Error write taskname length\n");
        exit(-1);
      }

      while (tasknamelen != 0) {
        size_t nb_bytes = write(fd[1], SD_task_get_name(*it), tasknamelen);
        if (nb_bytes < 0) {
          char output[100];
          sprintf(output, "problème de write dans pipe\n");
          perror(output);
          exit(1);
        }
        tasknamelen -= nb_bytes;
      }
    }

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    // freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    // deleteWf(wf);
    size_t total_nhosts = sg_host_count();

    for (size_t cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } // end child
  else {
    close(fd[1]); /* close write */
    double dTest = -2;

    // receving the result
    if (read(fd[0], &dTest, sizeof(double)) <= 0) {
      printf("Error read time for first attribution\n");
      exit(-1);
    }
    resMakespan = dTest;

    if (read(fd[0], &dTest, sizeof(double)) <= 0) {
      printf("Error read cost for  first attribution\n");
      exit(-1);
    }
    resCost = dTest;

    //    std::cout << "j'ai makespan = " <<resMakespan <<" et cost = "
    //    <<resCost <<"\n";
    size_t nbTasks = 0;
    if (read(fd[0], &nbTasks, sizeof(size_t)) <= 0) {
      printf("Error read cost for  nbTasks\n");
      exit(-1);
    }

    for (size_t i = 0; i < nbTasks; i++) {
      // réception de la taille du nom de la tâche
      size_t tasknamelen = 0;
      if (read(fd[0], &tasknamelen, sizeof(size_t)) <= 0) {
        printf("Error read taskname length --------\n");
        exit(-1);
      }

      // réception du nom de la tâche
      unsigned int taskLen =
          tasknamelen; // to keep the lenght of the recieved host name somewhere
                       // and be able to put the \0 at the end of the reception
      char *nomTask = (char *)malloc(sizeof(char) * (tasknamelen + 1));
      while (tasknamelen != 0) {
        size_t nb_bytes = read(fd[0], nomTask, tasknamelen);
        if (nb_bytes < 0) {
          char output[100];
          sprintf(output, "problème de read dans pipe\n");
          perror(output);
          exit(1);
        }
        tasknamelen -= nb_bytes;
      } // end of receive name of host

      nomTask[taskLen] = '\0';

      std::cout << " critical path, j'ai lu : " << nomTask << "\n";
      critPath.push_back(dicoTsk[nomTask]);
    }

    int stat_val;
    waitpid(childpid, &stat_val, 0);

  } // end parent

  close(fd[0]);
}

/******************************************************************************
*******************************************************************************
******************************************************************************/

/**
 * @brief cacul the mean of speed of all hosts
 * @param hosts: hosts array
 * @param nb_host: number of hosts
 * @return the mean of speed
 */
double calcMeanSpeed(const sg_host_t *hosts, const size_t nb_hosts) {
  double speed_m = 0;

  for (size_t cursor = 0; cursor < nb_hosts; cursor++) {
    speed_m += sg_host_speed(hosts[cursor]);
  }
  return speed_m / nb_hosts;
}

/**
 * @brief cacul the mean of initial cost of all hosts
 * @param hosts: hosts array
 * @param nb_host: number of hosts
 * @return the mean of initial cost
 */
double calcMeanInitialCost(const sg_host_t *hosts, const size_t nb_hosts,
                           std::map<sg_host_t, HostCost> mhc) {
  double initCost_m = 0;

  for (size_t cursor = 0; cursor < nb_hosts; cursor++) {
    HostCost hc = mhc[hosts[cursor]];
    initCost_m += getHC_iniC(hc);
  }
  return initCost_m / nb_hosts;
}

/**
 * @brief cacul the mean bandwidht of all hosts
 * @param hosts: hosts array
 * @return the mean bandwidht
 */
double calcMeanBandwidht(const sg_host_t *hosts) {
  // in our modele, it's the same everyhere
  return sg_host_route_bandwidth(hosts[0], hosts[1]);
}

/**
 * @brief cacul the mean latency of all hosts
 * @param hosts*: hosts array
 * @return the mean latency
 */
double calcMeanLatency(const sg_host_t *hosts) {
  // in our modele, it's the same everyhere
  return sg_host_route_latency(hosts[0], hosts[1]);
}

/**
 * @brief get  the amount of input and output data for the task
 * @param T, the task
 * @return the amoutn of input and output data for the task
 */
double getAmountInOutData(const SD_task_t &T) {
  // variable to return
  double amount = 0;
  // variables use in the loop
  unsigned int cursor = 0;
  SD_task_t task;
  // process input data
  xbt_dynar_foreach(SD_task_get_parents(T), cursor, task) {
    if (SD_task_get_kind(task) == SD_TASK_COMM_E2E) {
      if (SD_task_get_amount(task) <= 1e-6) {
        amount += 0;
      } else {
        amount += SD_task_get_amount(task);
      }
    }
  }
  // process output data
  xbt_dynar_foreach(SD_task_get_children(T), cursor, task) {
    if (SD_task_get_kind(task) == SD_TASK_COMM_E2E) {
      if (SD_task_get_amount(task) <= 1e-6) {
        amount += 0;
      } else {
        amount += SD_task_get_amount(task);
      }
    }
  }
  // time to return
  return amount;
}

/**
 * @brief return true if at least one task is computable
 * @param tasks : list of task
 * @return a boolean, true if at least  one task is computable
 */
bool minOneComputableTask(const xbt_dynar_t &tasks) {
  bool minOneComputableTask = false;
  size_t cursor = 0;
  SD_task_t task;
  xbt_dynar_foreach(tasks, cursor, task) {
    if (isTaskComput(task)) {
      minOneComputableTask = true;
    }
  }
  return minOneComputableTask;
}

/**
 * @brief this function return all grand children of a task without duplicate
 * @param task : task
 * @return xbt_dynar_t container all grand childrens tasks without duplicate
 */
xbt_dynar_t getNotDucplicateGrdChild(const SD_task_t task) {
  xbt_dynar_t grdChildrens = xbt_dynar_new(sizeof(SD_task_t), NULL);
  // this list is to stock the task's name already in the container
  std::list<std::string> nameTasks;

  // process all child and grand child
  xbt_dynar_t childrens = SD_task_get_children(task);
  size_t cursorChild, cursorGrdChild;
  SD_task_t child, grdChild;

  // process childs
  xbt_dynar_foreach(childrens, cursorChild, child) {
    xbt_dynar_t grdChildrendDuplicate = SD_task_get_children(child);

    // process grd childs
    xbt_dynar_foreach(grdChildrendDuplicate, cursorGrdChild, grdChild) {
      std::string name(SD_task_get_name(grdChild));
      auto it = std::find(nameTasks.begin(), nameTasks.end(), name);
      if (it == nameTasks.end()) {
        // then we don't have the task's name on the list and the container
        nameTasks.push_back(name);
        xbt_dynar_push(grdChildrens, &grdChild);
      }
    }
  }
  return grdChildrens;
}
