#ifndef CALC_BUDG_PER_TASK_PATH_H
#define CALC_BUDG_PER_TASK_PATH_H

#include "workflow.h"

void getDataPath(const std::vector<std::list<SD_task_t>> listPath,
                 std::map<std::list<SD_task_t>, double> &WmaxPPath,
                 std::map<std::list<SD_task_t>, double> &transfPPath,
                 double &WmaxPath, double &transfPath,
                 std::map<SD_task_t, double> &WmaxPTask,
                 std::map<SD_task_t, double> &transfPTask);

void calcBudgPTskPath(Workflow &wf, double budget, double bw, double speed_m,
                      std::map<SD_task_t, double> &budgPTsk,
                      std::vector<std::list<SD_task_t>> listPath);
#endif
