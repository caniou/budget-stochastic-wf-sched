#include "simgrid/simdag.h"
#include "xbt.h"
#include <iostream>
#include <list>
#include <map>

#include "toolsSched.h"
#include "workflow.h"

/**
 * @brief Display all path
 * @param listPath : list of all wf's path
 */
void displayPaths(std::vector<std::list<SD_task_t>> &arrPath) {
  std::cout << "-----Number of Paths : " << arrPath.size() << std::endl;
  unsigned int numList = 0;
  for (std::vector<std::list<SD_task_t>>::iterator itList = arrPath.begin();
       itList != arrPath.end(); itList++) {
    std::cout << "-----The list : " << numList << std::endl
              << "----------Contain the following taskd : " << std::flush;

    for (std::list<SD_task_t>::iterator itTask = itList->begin();
         itTask != itList->end(); itTask++) {
      std::cout << SD_task_get_name(*itTask) << " - " << std::flush;
    }
    std::cout << std::endl;
    numList++;
  }
}

/**
 * @brief calcul all path of the wf, this function is recursif
 * @param arrPath : the list of path
 * @param T : the current task
 * @param numPath : the current path of the task
 * @return a vector of path list
 */
void getPathsRec(std::vector<std::list<SD_task_t>> &arrPath, const SD_task_t &T,
                 unsigned int numPath) {
  // the last task of the path
  if (strcmp(SD_task_get_name(T), "end") == 0) {
    arrPath[numPath].push_back(T);
  }

  else {
    arrPath[numPath].push_back(T);

    std::list<SD_task_t> currentPath = arrPath[numPath];
    // use to konw if we need to create a new path
    unsigned int firstChildComputable = true;
    // process all childrens
    xbt_dynar_t childrens = SD_task_get_children(T);

    // if at least one child is computable (normally it's all tasks without root
    // and all tasks before end)
    if (minOneComputableTask(childrens)) {
      size_t cursorChild;
      SD_task_t child;
      // we search every children who is computable
      xbt_dynar_foreach(childrens, cursorChild, child) {
        // test if the child isn't a transfere task
        if (isTaskComput(child)) {
          // in the case where the task have more than one child, we need to
          // duplicate the current path
          if (!firstChildComputable) {
            arrPath.push_back(currentPath);
            numPath = arrPath.size() - 1;
          }
          // get the path of the children
          getPathsRec(arrPath, child, numPath);
          firstChildComputable = false;
        }
      }
    }
    // no computable child we search in grd-child (normally it's for root and
    // the last task of one path to find end)
    else {
      // we search directly the grd Child, we want every taks just one time. In
      // fact, if the task have more than one transfere taks to the grdChild, it
      // will be appeare multiple time and as a result we have duplicate path.
      // This is why we take the grand Child but not duplicate
      xbt_dynar_t grdchildrens = getNotDucplicateGrdChild(T);
      size_t cursorGrdChild;
      SD_task_t grdChild;

      xbt_dynar_foreach(grdchildrens, cursorGrdChild, grdChild) {
        if (isTaskComput(grdChild)) {
          // In the case where the current task is root, we need to test if his
          // grd Child is really the first task of the wf.
          // In fact, if a task in the wf have an input file coming from no task
          // this input file will be considered like a root children, and as a
          // result the grand children considered like a first wf task whereas
          // this is not the case
          if (strcmp(SD_task_get_name(T), "root") == 0) {
            bool firstTask = false;
            xbt_dynar_t parentGrdChild = SD_task_get_parents(grdChild);
            // if on parent is computable the task is in the middle of the wf
            // and isn't possible to have it in first
            if (!minOneComputableTask(parentGrdChild)) {
              firstTask = true;
            }
            xbt_dynar_free_container(&parentGrdChild);
            if (firstTask) {
              if (!firstChildComputable) {
                arrPath.push_back(currentPath);
                numPath = arrPath.size() - 1;
              }
              // get the path of the children
              getPathsRec(arrPath, grdChild, numPath);
              firstChildComputable = false;
            }
            xbt_dynar_free_container(&parentGrdChild);

          }
          // this a normal task (a task before the task end)
          else {
            if (!firstChildComputable) {
              arrPath.push_back(currentPath);
              numPath = arrPath.size() - 1;
            }
            // get the path of the children
            getPathsRec(arrPath, grdChild, numPath);
            firstChildComputable = false;
          } // else
        }   // if grdChild is computable
      }     // for each grdChild
      xbt_dynar_free_container(&grdchildrens);
    } // else (no computable child)
    xbt_dynar_free_container(&childrens);
  } // if the task isn't end
} // function getPathsRec

/**
 * @brief calcul all path of the wf
 * @param wf : a workflow
 * @return a vector of path list
 */
std::vector<std::list<SD_task_t>> getPathsFrom(const Workflow &wf,
                                               const SD_task_t &T) {
  // the variable to return
  std::vector<std::list<SD_task_t>> arrPath;
  // get pahts
  std::list<SD_task_t> firstList;
  arrPath.push_back(firstList);
  getPathsRec(arrPath, T, 0);
  // It's time to return
  return arrPath;
}

/**
 * @brief calcul all path of the wf
 * @param wf : a workflow
 * @return a vector of path list
 */
std::vector<std::list<SD_task_t>> getPaths(const Workflow &wf) {
  // variable to return
  std::vector<std::list<SD_task_t>> arrPath;
  // variables use in the loop
  size_t cursor;
  SD_task_t task;
  // search the first simgrid task to find paths since it
  xbt_dynar_foreach(getDagWf(wf), cursor, task) {
    if (strcmp(SD_task_get_name(task), "root") == 0) {
      arrPath = getPathsFrom(wf, task);
    }
  }
  // It's time to return
  return arrPath;
}

// #endif