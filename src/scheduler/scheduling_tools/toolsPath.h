#ifndef TOOLS_PATH_H
#define TOOLS_PATH_H

#include "workflow.h"

void displayPaths(std::vector<std::list<SD_task_t>> &arrPath);

std::vector<std::list<SD_task_t>> getPathsFrom(const Workflow &wf,
                                               const SD_task_t &T);

std::vector<std::list<SD_task_t>> getPaths(const Workflow &wf);

#endif