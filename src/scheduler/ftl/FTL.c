#include <algorithm>
#include <iostream>
#include <list>
#include <map>
#include <sys/wait.h>

#include "FTL.h"
#include "HEFTBudg.h"
#include "calcBudgPerTask.h"
#include "hostCost.h"
#include "outputFiles.h"
#include "toolsLevel.h"
#include "toolsSched.h"
#include "workflow.h"

/**
 * @brief For one task, return the best host which terminate the task before the
 * finish time of the current lvl
 *
 * @param task: task
 * @param tskBudg: map between a task and it budget
 * @param cagnotte: the budget non allocated
 * @param finishTime: the max time that is according to finish to task
 * @param mCostPHost: mapping (host -> hostCost)
 *
 * @return the mapping task -> host + the allocation order
 */

sg_host_t
SD_task_get_best_host_time(const SD_task_t &task, const double taskBudg,
                           double &cagnotte, const double finishTime,
                           std::map<sg_host_t, HostCost> &mCostPHost) {

  sg_host_t best_host;

  sg_host_t *hosts = sg_host_list();
  const unsigned int nbHosts = sg_host_count();
  const double bw = calcMeanBandwidht(hosts);
  const double realTskBudg = taskBudg + cagnotte;
  // first we process all host and store it in two distingue list : one for
  // all host which permetted the task to finish before the FT and the other
  // for all other host
  std::vector<sg_host_t> hostsBeforeFT;
  std::vector<sg_host_t> hostsAfterFT;
  // map to store the cost of the taks on a specific host
  std::map<sg_host_t, double> costPHost;
  // map to store the finish time of the task on a specific host
  std::map<sg_host_t, double> ftPHost;

  //--------------------------------------------------------------
  // we won't test multiple times the same type of empty VM
  //--------------------------------------------------------
  bool toTest = false;
  bool isTestVM[3] = {false, false, false};
  //--------------------------------------------------------------

  /****************************************************************************
  *************************** DIVIDE HOSTS IN 2 LISTS *************************
  ****************************************************************************/
  for (unsigned int i = 0; i < nbHosts; i++) {
    // if the host is already use, do the test
    if (sg_host_get_last_scheduled_task(hosts[i]) != NULL) {
      toTest = true;
    }
    // if it's a new host, take juste one per VM type
    else {
      // get the type of VM
      const char *name = sg_host_get_name(hosts[i]);
      if (strlen(name) >= 3) {
        if (name[2] == '1') {
          if (!isTestVM[0]) {
            isTestVM[0] = true;
            toTest = true;
          }
        }
        if (name[2] == '2') {
          if (!isTestVM[1]) {
            isTestVM[1] = true;
            toTest = true;
          }
        }
        if (name[2] == '3') {
          if (!isTestVM[2]) {
            isTestVM[2] = true;
            toTest = true;
          }
        }
      } // end of type of vm
    }   // end of if host is new

    if (toTest) {
      ftPHost[hosts[i]] = finish_on_at(task, hosts[i]);
      double timeCalcTps = 0.;

      // if is new host
      if (getHC_occTime(mCostPHost[hosts[i]]) <= 1e-6) {
        timeCalcTps = get_amount_data_needed(task) / bw +
                      SD_task_get_amount(task) / sg_host_speed(hosts[i]);
        costPHost[hosts[i]] = get_cost_data_prec_newVM(task, mCostPHost);
      }
      // old host
      else {
        // TODO : vérifier s'il ne faut pas tester si l'ajout des data entrainte
        // si les prédécesseur n'on pas été éxécutés sur cet host
        timeCalcTps = ftPHost[hosts[i]] - getHC_occTime(mCostPHost[hosts[i]]);
        costPHost[hosts[i]] = 0.0;
      }
      costPHost[hosts[i]] += timeCalcTps * getHC_hC(mCostPHost[hosts[i]]);

      // now put the host in one categorie
      if (ftPHost[hosts[i]] <= finishTime) {
        hostsBeforeFT.push_back(hosts[i]);
      } else {
        hostsAfterFT.push_back(hosts[i]);
      }
    }
  }
  /****************************************************************************
  *****************************************************************************
  ****************************************************************************/
  // find the host which minimize the budget in all host witch finish the task
  // before the FT
  sg_host_t minHostBefore = hostsBeforeFT[0];
  for (unsigned int i = 1; i < hostsBeforeFT.size(); i++) {
    if (costPHost[minHostBefore] > costPHost[hostsBeforeFT[i]]) {
      minHostBefore = hostsBeforeFT[i];
    }
  }

  // if the cost of the task on the host which minimize the budget and finish
  // the task before the FT, have a correct budget
  if (costPHost[minHostBefore] <= realTskBudg) {
    best_host = minHostBefore;
  }

  else {

    sg_host_t bestHostAfter = nullptr;
    // in the case where nothing host have a cost less than the task's budget
    sg_host_t minHostAfter = hostsAfterFT[0];
    for (unsigned int i = 0; i < hostsAfterFT.size(); i++) {
      if (bestHostAfter == nullptr) {
        if (costPHost[hostsAfterFT[i]] <= realTskBudg) {
          bestHostAfter = hostsAfterFT[i];
        }
      }
      // if task have a correct budget in this host and if the host finish
      // before the currente best host after
      else if ((costPHost[hostsAfterFT[i]] <= realTskBudg) &&
               (ftPHost[hostsAfterFT[i]] < ftPHost[bestHostAfter])) {
        bestHostAfter = hostsAfterFT[i];
      }

      // here we set the host which minimize the budget
      if (costPHost[minHostAfter] > costPHost[hostsAfterFT[i]]) {
        minHostAfter = hostsAfterFT[i];
      }
    }

    /**************************************************************************
    ***************************** CHOOSE BEST HOST ****************************
    **************************************************************************/
    // if nothing host have a budget for the task less than the task's budget,
    // then, we chose the host wich minimize the cost
    if (bestHostAfter == nullptr) {
      if (costPHost[minHostBefore] > costPHost[minHostAfter]) {
        best_host = minHostAfter;
      } else {
        best_host = minHostBefore;
      }
    }
    // if we have find a host witch has a correct budget
    else {
      best_host = bestHostAfter;
    }
  }

  // update the pot
  cagnotte = realTskBudg - costPHost[best_host];
  // clear data
  xbt_free(hosts);
  // it's time to return
  return best_host;
}

/**
 * @brief Generic function to find if an element of any type exists in list
 */
template <typename T>
bool contains(const std::list<T> &listOfElements, const T &element) {
  // Find the iterator if element in list
  const auto it =
      std::find(listOfElements.begin(), listOfElements.end(), element);
  // return if iterator points to end or not. It points to end then it means
  // element does not exists in list
  return it != listOfElements.end();
}

/**
 * @brief Made the scheduling, minimize the budget for each task witch begin
 * before it level and finish before the finish time of the level
 *
 * @param wf: workflow
 * @param ordoT: the allocation order of schedule tasks
 * @param m: mapping (task -> host)
 * @param budget: the initial budget whitout the transfere cost
 * @param cagnotte: the budget non allocated
 * @param mhc: mapping (host -> hostCost)
 * @param eftPTsk: mapping (task -> early finish time)
 * @param newBudgPTsk: mapping (task -> budget allocate)
 *
 * @return the mapping task -> host + the allocation order AND the mapping
 * (task
 * -> early finish time) AND mapping (task -> budget allocate)
 */

void algo_offline_ftl(Workflow &wf, SD_task_t ordoT[],
                      std::map<SD_task_t, sg_host_t> &m, double budget,
                      double cagnotte, std::map<sg_host_t, HostCost> mhc,
                      std::map<SD_task_t, unsigned int> &eftPTsk,
                      std::map<SD_task_t, double> &newBudgPTsk) {

  //===========================================================
  // let's fork
  int fd[2];
  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    close(fd[0]); /* close read */

    /**************************************************************************
     ***** pre-calculi: repartition of various budgets
     *************************************************************************/
    // we are pessimistic, we use the worst values:
    worstValues(wf);

    xbt_dynar_t dag = getDagWf(wf);
    size_t total_nhosts = sg_host_count();
    sg_host_t *hosts = sg_host_list();

    // bw, latency, initial cost and mean speed
    const double bw = calcMeanBandwidht(hosts);
    // const double lat_m = calcMeanLatency(hosts); not use
    const double speed_m = calcMeanSpeed(hosts, total_nhosts);
    const double iniCost_m = calcMeanInitialCost(hosts, total_nhosts, mhc);

    // reassessment of the budget
    const int nbVMsMax = nbMaxVMn(dag); // nb max of VMs
    const double Bcalc = budget - (nbVMsMax * iniCost_m + cagnotte);
    // this is the initial budget of the simulation
    const double budgetIni = budget;
    std::cout << "------ FTL : budget calculé : " << Bcalc << std::endl;

    // get lvl list
    std::map<unsigned int, std::list<SD_task_t>> mapLvlTask = getTskLvl(wf);
    // displayLevels(mapLvlTask);
    // budget calcul per task
    std::map<SD_task_t, double> budgPTsk;
    calcBudgPTsk(wf, Bcalc, bw, speed_m, budgPTsk);

    double cTot = 0;
    for (const auto budg : budgPTsk) {
      cTot += budg.second;
    }
    std::cout << "------ FTL : budget   " << budget << std::endl;
    std::cout << "------ FTL : budgPTsk " << cTot << std::endl;

    // création de la liste des tâches triées par rang décroissant

    /**************************************************************************
     ***** scheduling
     *************************************************************************/
    double timeCalc = 0;
    double cagnotteFin = 0;
    unsigned int cursor;
    SD_task_t task;

    // map associant une tâche au numéro indiquant dans quel ordre elles ont
    // été ordonnancées
    std::map<SD_task_t, size_t> nb_ord;
    // compteur indiquant à quel moment la tâche a été ordonnancée
    size_t nb_ordo_tmp = 0;

    // place "stop points" on the simulation
    xbt_dynar_foreach(dag, cursor, task) { SD_task_watch(task, SD_DONE); }

    xbt_dynar_t changed_tasks = xbt_dynar_new(sizeof(SD_task_t), NULL);

    // map between a task and the finish time of the previous lvl (only set
    // if the task is ready before it lvl)
    std::map<SD_task_t, bool> tskReadyBeforeLvl;
    xbt_dynar_foreach(dag, cursor, task) {
      if (isTaskComput(task)) {
        tskReadyBeforeLvl[task] = false;
      }
    }

    // represent the finish time of the lvl
    std::map<unsigned int, double> finishTimeLvl;
    for (unsigned int i = 0; i <= mapLvlTask.size(); i++) {
      finishTimeLvl[i] = SD_get_clock(); // it's zero
    }

    // process all lvl
    for (int i = mapLvlTask.size() - 1; i >= 0; i--) {
      const std::list<SD_task_t> currentLvl = mapLvlTask[i];

      xbt_dynar_t ready_tasks = get_ready_tasks(dag);

      // process all ready task to find if one is not in the current level
      xbt_dynar_foreach(ready_tasks, cursor, task) {
        if (!contains(currentLvl, task)) { // the task isn't the current lvl
          tskReadyBeforeLvl[task] = true;
        }
      } // xbt_dynar_foreach(ready_tasks)

      // first, we want to schedule the task which don't have a map with any
      // previous lvl
      for (const auto tsk : currentLvl) {
        // test if the task was not ready instead
        if (!tskReadyBeforeLvl[tsk]) {
          // get best host
          sg_host_t bHost = SD_task_get_best_host_heft_budget(
              tsk, mhc, budgPTsk, cagnotte, timeCalc, cagnotteFin);

          // attribution
          SD_task_schedulel(tsk, 1, bHost);

          m[tsk] = bHost;
          // associer son numéro d'ordre d'ordonnancement
          // vraiment pour garder le template TODO: faire la même proprement
          nb_ord[tsk] = nb_ordo_tmp;
          // incrémenter pour avoir le numéro suivant
          nb_ordo_tmp++;

          double min_finish_time = finish_on_at(tsk, bHost);

          //=======================
          SD_task_t last_scheduled_task =
              sg_host_get_last_scheduled_task(bHost);

          if (last_scheduled_task &&
              (SD_task_get_state(last_scheduled_task) != SD_DONE) &&
              (SD_task_get_state(last_scheduled_task) != SD_FAILED) &&
              !SD_task_dependency_exists(sg_host_get_last_scheduled_task(bHost),
                                         tsk)) {
            SD_task_dependency_add("resource", NULL, last_scheduled_task, tsk);
          }
          sg_host_set_last_scheduled_task(bHost, tsk);
          sg_host_set_available_at(bHost, min_finish_time);
          //=======================

          // simulate with update
          SD_simulate_with_update(-1.0, changed_tasks);

          // update the lvl finish time for the level
          if (finishTimeLvl[i] < min_finish_time) {
            finishTimeLvl[i] = min_finish_time;
          }

          newBudgPTsk[tsk] = budgPTsk[tsk] + cagnotte - cagnotteFin;
          cagnotte = cagnotteFin;
          eftPTsk[tsk] = min_finish_time;
        }
      }

      // secondly, I want to schedule the task which have a map with any
      // previous lvl
      for (const auto tsk : currentLvl) {
        // test if the task was ready instead
        if (tskReadyBeforeLvl[tsk]) {
          // get best host
          const double cagnotteBegin = cagnotte;

          sg_host_t bHost = SD_task_get_best_host_time(
              tsk, budgPTsk[tsk], cagnotte, finishTimeLvl[i], mhc);

          // attribution
          SD_task_schedulel(task, 1, bHost);

          m[tsk] = bHost;
          // associer son numéro d'ordre d'ordonnancement
          // vraiment pour garder le template TODO: faire la même proprement
          nb_ord[tsk] = nb_ordo_tmp;
          // incrémenter pour avoir le numéro suivant
          nb_ordo_tmp++;
          // newBudgPTsk[tsk] = budgPTsk[tsk] - cagnotte;

          double min_finish_time = finish_on_at(tsk, bHost);

          //=======================
          SD_task_t last_scheduled_task =
              sg_host_get_last_scheduled_task(bHost);

          if (last_scheduled_task &&
              (SD_task_get_state(last_scheduled_task) != SD_DONE) &&
              (SD_task_get_state(last_scheduled_task) != SD_FAILED) &&
              !SD_task_dependency_exists(sg_host_get_last_scheduled_task(bHost),
                                         tsk)) {
            SD_task_dependency_add("resource", NULL, last_scheduled_task, tsk);
          }
          sg_host_set_last_scheduled_task(bHost, tsk);
          sg_host_set_available_at(bHost, min_finish_time);
          //=======================

          // simulate with update
          SD_simulate_with_update(-1.0, changed_tasks);
          finishTimeLvl[i] = std::max(finishTimeLvl[i], min_finish_time);

          // update the lvl finish time for the level
          if (finishTimeLvl[i] < min_finish_time) {
            finishTimeLvl[i] = min_finish_time;
          }
          newBudgPTsk[tsk] = budgPTsk[tsk] + cagnotteBegin - cagnotte;
          eftPTsk[tsk] = min_finish_time;
        }
      }

      xbt_dynar_free_container(&ready_tasks);
    }

    std::cout << "---------------------------[level] nouveaux budget "
                 "nomber de tache : "
              << newBudgPTsk.size() << std::endl;

    /**************************************************************************
     ***** demande expresse de Yves R., à refaire proprement
     *************************************************************************/
    const char nomAlgo[] = "FTL";
    credoFile(nomAlgo, budgetIni, cagnotte);

    /**************************************************************************
     ***** sending results
     *************************************************************************/

    /*
     * Wf is the same for father and son. Same order of tasks given by
     * following foreach. For each task, send an u int for ordo an u int for
     * size of hostname char[] for hostname
     */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number;
        ordo_number = nb_ord[task];
        if (write(fd[1], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error write ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(sg_host_get_name(m[task]));
        if (write(fd[1], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error write hostname length\n");
          exit(-1);
        }

        while (hostnamelen != 0) {
          size_t nb_bytes =
              write(fd[1], sg_host_get_name(m[task]), hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de write dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        }
        // write the eft per task
        size_t eftTask = eftPTsk[task];
        if (write(fd[1], &eftTask, sizeof(size_t)) <= 0) {
          printf("Error write eftTask\n");
          exit(-1);
        }

        // write the new budgPTask per task
        const double budgTsk = newBudgPTsk[task];
        if (write(fd[1], &budgTsk, sizeof(budgTsk)) <= 0) {
          printf("Error write newBudgetTask\n");
          exit(-1);
        }
      }
    }

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    deleteWf(wf);

    // size_t total_nhosts = sg_host_count();
    // sg_host_t *hosts = sg_host_list();

    for (cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } // end child
  else {
    // fait l'ordo
    close(fd[1]); /* close write */
    /*
     * Wf is the same for father and son. Same order of tasks given by
     * following foreach. For each task, send an u int for ordo an u int for
     * size of hostname char[] for hostname
     */
    unsigned int cursor;
    SD_task_t task;
    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number = 0;
        if (read(fd[0], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error read ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(SD_task_get_name(task));
        if (read(fd[0], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error read hostname length\n");
          exit(-1);
        }

        unsigned int hostLen =
            hostnamelen; // to keep the lenght of the recieved host name
                         // somewhere and be able to put the \0 at the end
                         // of the reception
        char *nomHost = (char *)malloc(sizeof(char) * (hostnamelen + 1));
        while (hostnamelen != 0) {
          size_t nb_bytes = read(fd[0], nomHost, hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de read dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        } // end of receive name of host

        nomHost[hostLen] = '\0';
        // std::cout <<"j'ai lu le nom de l'hôte : " <<nomHost <<" pour la
        // tâche : " <<SD_task_get_name(task) <<" d'indice : "
        // <<ordo_number
        // <<"\n";
        m[task] = sg_host_by_name(nomHost);
        ordoT[ordo_number] = task;
        free(nomHost);

        // read the eft per task
        size_t eftTask = 0;
        if (read(fd[0], &eftTask, sizeof(size_t)) <= 0) {
          printf("Error read eftTask\n");
          exit(-1);
        }
        eftPTsk[task] = eftTask;

        // read the newBudgetPTsk per task
        double budgTsk = 0;
        if (read(fd[0], &budgTsk, sizeof(budgTsk)) <= 0) {
          printf("Error read new budgTsk\n");
          exit(-1);
        }
        newBudgPTsk[task] = budgTsk;
      } // end of if task is executable
    }   // end of foreach

    int stat_val;
    waitpid(childpid, &stat_val, 0);
  } // end parent
}