#include <iostream>
#include <list>
#include <map>
#include <sys/wait.h>

#include "FTL.h"
#include "FTLMultFirstTask.h"
#include "HEFTBudg.h"
#include "calcBudgPerTask.h"
#include "outputFiles.h"
#include "toolsLevel.h"
#include "toolsSched.h"
#include "workflow.h"

/**
 * @brief Made the scheduling, it is the same algo than FTL but it use the cost
 * per task calculte with FTL and add the budget leftover to the first task
 *
 * @param wf: workflow
 * @param ordoT: the allocation order of schedule tasks
 * @param m: mapping (task -> host)
 * @param deadline: the deadline
 * @param budget: the initial budget whitout the transfere cost
 * @param cagnotte: the budget non allocated
 * @param mhc: mapping (host -> hostCost)
 *
 * @return the mapping task -> host + the allocation order
 */

void algo_offline_ftl_mult_first_task(
    Workflow &wf, SD_task_t ordoT[], std::map<SD_task_t, sg_host_t> &m,
    double deadline, double budget, double cagnotte,
    std::map<sg_host_t, HostCost> mhc,
    void (*fctDivPot)(const Workflow &, const SD_task_t &, const double,
                      std::map<SD_task_t, double> &)) {
  //===========================================================
  // let's fork
  int fd[2];
  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    close(fd[0]); /* close read */

    /**************************************************************************
     ***** pre-calculi: repartition of various budgets
     *************************************************************************/
    // we are pessimistic, we use the worst values:
    worstValues(wf);

    unsigned int cursor;
    SD_task_t task;
    xbt_dynar_t dag = getDagWf(wf);
    sg_host_t *hosts = sg_host_list();
    const size_t total_nhosts = sg_host_count();

    /********************************** FTL **********************************/
    std::map<SD_task_t, unsigned int> eftPTsk;
    std::map<SD_task_t, double> budgPTsk;

    algo_offline_ftl(wf, ordoT, m, budget, cagnotte, mhc, eftPTsk, budgPTsk);

    // get the amount of not used money
    double makespanCalc = -1;
    double costCalc = -1;
    getMakespanCostSched(wf , ordoT, m, deadline, budget, cagnotte, mhc,
                         makespanCalc, costCalc);

    /********************************* BUDGET *********************************/
    // calcule cTot and the budget leftover
    double cTot = 0;
    for (const auto budg : budgPTsk) {
      cTot += budg.second;
    }

    // reassessment of the budget
    const double iniCost_m = calcMeanInitialCost(hosts, total_nhosts, mhc);
    const int nbVMsMax = nbMaxVMn(dag); // nb max of VMs
    const double Bcalc = budget - (nbVMsMax * iniCost_m + cagnotte);

    // const double budgLeftovers = budget - cTot;

    const double budgLeftovers = budget - cTot;
    std::cout << "------ FTLMult : budget " << budget << std::endl;
    std::cout << "------ FTLMult : coût calculé " << costCalc << std::endl;
    std::cout << "------ FTLMult : coût totatl " << cTot << std::endl;

    std::cout <<  "------ FTLMult : leftover " << budgLeftovers << std::endl;
    const double budgetIni = budget;  
    // add leftovers to the first task !
    budgPTsk[get_root(dag)] += budgLeftovers;

    /******************************** LVL LIST *******************************/
    std::map<unsigned int, std::list<SD_task_t>> mapLvlTask = getTskLvl(wf);
    // displayLevels(mapLvlTask);

    /**************************************************************************
     ***** scheduling
     *************************************************************************/
    double timeCalc = 0;
    double cagnotteFin = 0;
    cagnotte = 0;

    // map associant une tâche au numéro indiquant dans quel ordre elles ont
    // été ordonnancées
    std::map<SD_task_t, size_t> nb_ord;
    // compteur indiquant à quel moment la tâche a été ordonnancée
    size_t nb_ordo_tmp = 0;

    // place "stop points" on the simulation
    xbt_dynar_foreach(dag, cursor, task) { SD_task_watch(task, SD_DONE); }

    xbt_dynar_t changed_tasks = xbt_dynar_new(sizeof(SD_task_t), NULL);

    // map between a task and the finish time of the previous lvl (only set if
    // the task is ready before it lvl)
    std::map<SD_task_t, bool> beginTsk;
    // represent the finish time of the lvl
    std::map<unsigned int, double> finishTimeLvl;
    finishTimeLvl[mapLvlTask.size()] = SD_get_clock();

    xbt_dynar_foreach(dag, cursor, task) {
      if (isTaskComput(task)) {
        beginTsk[task] = false;
      }
    }

    std::cout << "début for all lvl " << std::endl;
    // process all lvl
    for (int i = mapLvlTask.size() - 1; i >= 0; i--) {
      const std::list<SD_task_t> currentLvl = mapLvlTask[i];

      xbt_dynar_t ready_tasks = get_ready_tasks(dag);

      // process all ready task to find if one is not in the current level
      xbt_dynar_foreach(ready_tasks, cursor, task) {
        if (!contains(currentLvl, task)) { // the task isn't the current lvl
          beginTsk[task] = true;
        }
      } // xbt_dynar_foreach(ready_tasks)

      // first, we want to schedule the task which don't have a map with any
      // previous lvl
      for (const auto tsk : currentLvl) {
        // test if the task was not ready instead
        if (!beginTsk[tsk]) {
          // get best host
          sg_host_t bHost = SD_task_get_best_host_heft_budget(
              tsk, mhc, budgPTsk, cagnotte, timeCalc, cagnotteFin);

          // attribution
          SD_task_schedulel(tsk, 1, bHost);

          m[tsk] = bHost;
          // associer son numéro d'ordre d'ordonnancement
          // vraiment pour garder le template TODO: faire la même proprement
          nb_ord[tsk] = nb_ordo_tmp;
          // incrémenter pour avoir le numéro suivant
          nb_ordo_tmp++;

          cagnotte = cagnotteFin;

          fctDivPot(wf, tsk, cagnotte, budgPTsk);

          double min_finish_time = finish_on_at(tsk, bHost);

          //=======================
          SD_task_t last_scheduled_task =
              sg_host_get_last_scheduled_task(bHost);

          if (last_scheduled_task &&
              (SD_task_get_state(last_scheduled_task) != SD_DONE) &&
              (SD_task_get_state(last_scheduled_task) != SD_FAILED) &&
              !SD_task_dependency_exists(sg_host_get_last_scheduled_task(bHost),
                                         tsk)) {
            SD_task_dependency_add("resource", NULL, last_scheduled_task, tsk);
          }
          sg_host_set_last_scheduled_task(bHost, tsk);
          sg_host_set_available_at(bHost, min_finish_time);
          //=======================

          // simulate with update
          SD_simulate_with_update(-1.0, changed_tasks);
          finishTimeLvl[i] = std::max(finishTimeLvl[i], min_finish_time);
        }
      }

      // secondly, I want to schedule the task which have a map with any
      // previous lvl
      for (const auto tsk : currentLvl) {
        // test if the task was ready instead
        if (beginTsk[tsk]) {
          // get best host
          sg_host_t bHost = SD_task_get_best_host_time(
              tsk, budgPTsk[tsk], cagnotte, finishTimeLvl[i], mhc);

          // attribution
          SD_task_schedulel(task, 1, bHost);

          m[tsk] = bHost;
          // associer son numéro d'ordre d'ordonnancement
          // vraiment pour garder le template TODO: faire la même proprement
          nb_ord[tsk] = nb_ordo_tmp;
          // incrémenter pour avoir le numéro suivant
          nb_ordo_tmp++;

          cagnotte = cagnotteFin;
          fctDivPot(wf, tsk, cagnotte, budgPTsk);

          double min_finish_time = finish_on_at(tsk, bHost);

          //=======================
          SD_task_t last_scheduled_task =
              sg_host_get_last_scheduled_task(bHost);

          if (last_scheduled_task &&
              (SD_task_get_state(last_scheduled_task) != SD_DONE) &&
              (SD_task_get_state(last_scheduled_task) != SD_FAILED) &&
              !SD_task_dependency_exists(sg_host_get_last_scheduled_task(bHost),
                                         tsk)) {
            SD_task_dependency_add("resource", NULL, last_scheduled_task, tsk);
          }
          sg_host_set_last_scheduled_task(bHost, tsk);
          sg_host_set_available_at(bHost, min_finish_time);
          //=======================

          // simulate with update
          SD_simulate_with_update(-1.0, changed_tasks);
          finishTimeLvl[i] = std::max(finishTimeLvl[i], min_finish_time);
        }
      }

      xbt_dynar_free_container(&ready_tasks);
    }
    std::cout << "fin  for all lvl" << std::endl;

    /**************************************************************************
     ***** demande expresse de Yves R., à refaire proprement
     *************************************************************************/
    const char nomAlgo[] = "FTLMultFirstTask";
    credoFile(nomAlgo, budgetIni, cagnotte);

    /**************************************************************************
     ***** sending results
     *************************************************************************/

    /*
     * Wf is the same for father and son. Same order of tasks given by following
     * foreach. For each task, send an u int for ordo an u int for size of
     * hostname char[] for hostname
     */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number;
        ordo_number = nb_ord[task];
        if (write(fd[1], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error write ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(sg_host_get_name(m[task]));
        if (write(fd[1], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error write hostname length\n");
          exit(-1);
        }

        while (hostnamelen != 0) {
          size_t nb_bytes =
              write(fd[1], sg_host_get_name(m[task]), hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de write dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        }
      }
    }

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    deleteWf(wf);

    // sg_host_t *hosts = sg_host_list();

    for (cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } // end child
  else {
    // fait l'ordo
    close(fd[1]); /* close write */
    /*
     * Wf is the same for father and son. Same order of tasks given by following
     * foreach. For each task, send an u int for ordo an u int for size of
     * hostname char[] for hostname
     */
    unsigned int cursor;
    SD_task_t task;
    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number = 0;
        if (read(fd[0], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error read ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(SD_task_get_name(task));
        if (read(fd[0], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error read hostname length\n");
          exit(-1);
        }

        unsigned int hostLen =
            hostnamelen; // to keep the lenght of the recieved host name
                         // somewhere and be able to put the \0 at the end
                         // of the reception
        char *nomHost = (char *)malloc(sizeof(char) * (hostnamelen + 1));
        while (hostnamelen != 0) {
          size_t nb_bytes = read(fd[0], nomHost, hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de read dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        } // end of receive name of host

        nomHost[hostLen] = '\0';
        // std::cout <<"j'ai lu le nom de l'hôte : " <<nomHost <<" pour la
        // tâche : " <<SD_task_get_name(task) <<" d'indice : "
        // <<ordo_number
        // <<"\n";
        m[task] = sg_host_by_name(nomHost);
        ordoT[ordo_number] = task;
        free(nomHost);
      } // end of if task is executable
    }   // end of foreach

    int stat_val;
    waitpid(childpid, &stat_val, 0);
  } // end parent
}