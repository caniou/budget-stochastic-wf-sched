#ifndef FTL_MULT_FIRST_TASK_H
#define FTL_MULT_FIRST_TASK_H

#include "workflow.h"

void algo_offline_ftl_mult_first_task(
    Workflow &wf, SD_task_t ordoT[], std::map<SD_task_t, sg_host_t> &m,
    double deadline, double budget, double cagnotte,
    std::map<sg_host_t, HostCost> mhc,
    void (*fctDivPot)(const Workflow &, const SD_task_t &, const double,
                      std::map<SD_task_t, double> &));

#endif