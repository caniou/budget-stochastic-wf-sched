#ifndef _ALGOHEFTBUDG2
#define _ALGOHEFTBUDG2
#include "HEFTBudg.h"
#include "calcBudgPerTask.h"
#include "hostCost.h"
#include "toolsSched.h"
#include "utilsMath.h"
#include "workflow.h"
#include <cmath>
#include <iostream>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//========================================================================================
/**
 * Fait l'ordonnancement et retourne le mapping tâche -> hôte + l'odre
 *d'attribution (vu qu'il remet sans cesse en question l'ordre d'attribution et
 *que seul
 *
 * ordo_t = tableau des tâches ordonnancées (données dans l'ordre de leur
 *ordonnancement) wf = workflow à placer m = mapping <tâche, hôte> effectué par
 *l'algorithme
 **/

static void algo_offline_heft_budg2(Workflow &wf, SD_task_t ordoT[],
                                    std::map<SD_task_t, sg_host_t> &m,
                                    double deadline, double budget,
                                    double cagnotte,
                                    std::map<sg_host_t, HostCost> mhc) {

  //  std::cout<<"algo offline heft à seconde répartition\n";

  // we are pessimistic, we use the worst values:
  worstValues(wf);

  // first scheduling : heft with budget
  algo_offline_heft_budget(wf, ordoT, m, deadline, budget, cagnotte, mhc);

  // get the amount of not used money
  double resMakespan = -1;
  double resCost = -1;
  getMakespanCostSched(wf, ordoT, m, deadline, budget, cagnotte, mhc,
                       resMakespan, resCost);

  // std::cout <<"ICI algoheftbudgMult, et je lis " <<resMakespan <<" secondes
  // et " <<resCost <<" sous.\n";

  // reattributing the not used budget for the worst case scenario
  double budgLeftovers = budget - resCost;

  //-----------------------------------------------------------
  // pre-calculi: repartition of various budgets
  //.........................................................
  xbt_dynar_t dag = getDagWf(wf);
  // on prend resCost, on le répartit à la heftBudg
  size_t cursor;
  SD_task_t task;
  double bw;

  // vitesse moyenne :
  double lat_m;

  size_t total_nhosts = sg_host_count();
  sg_host_t *hosts = sg_host_list();

  // bw moyenne sauvage, latence aussi (par modèle, c'est les mêmes partout)
  bw = sg_host_route_bandwidth(hosts[0], hosts[1]);
  lat_m = sg_host_route_latency(hosts[0], hosts[1]);

  // calcul du nombre de VMs max :
  int nbVMsMax = nbMaxVMn(dag);

  //  std::cout << "----------------------- nb VMs max : " <<nbVMsMax <<"\n";

  // coûts et vitesse moyens :
  double horCost_m = 0;
  double speed_m = 0;
  double iniCost_m = 0;

  for (cursor = 0; cursor < total_nhosts; cursor++) {
    HostCost testHc = mhc[hosts[cursor]];
    horCost_m += getHC_hC(testHc);
    speed_m += sg_host_speed(hosts[cursor]);
    iniCost_m += getHC_iniC(testHc);
  }

  horCost_m = horCost_m / total_nhosts;
  speed_m = speed_m / total_nhosts;
  iniCost_m = iniCost_m / total_nhosts;

  // réévaluation du budget
  //  std::cout <<"budget initial : " << budget <<"\n";
  double updateB = resCost - (nbVMsMax * iniCost_m + cagnotte);
  //  std::cout << "budget à jour : " <<updateB <<"\n";
  double budgetIni = resCost;

  // calcul du budget par tâche
  std::map<SD_task_t, double> budgPTsk;
  calcBudgPTsk(wf, updateB, bw, speed_m, budgPTsk);

  double timeCalc = 0;
  double cagnotteFin = 0;

  // création de la liste des tâches triées par rang décroissant
  std::list<SD_task_t> lT;
  getTasksSortedByRank(lT, dag, speed_m, bw, lat_m);

  //.............
  // puis on ajoute à la première tâche le budget qui restait dans le cas
  // précédent
  budgPTsk[lT.front()] += budgLeftovers;

  //.............
  // puis on lance un heftBudg normal, avec cette nouvelle répartition

  //===========================================================
  // let's fork
  int fd[2];
  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    close(fd[0]); /* close read */
    unsigned int cursor;
    SD_task_t task;

    std::map<SD_task_t, size_t>
        nb_ord; // map associant une tâche au numéro indiquant dans quel ordre
                // elles ont été ordonnancées
    size_t nb_ordo_tmp =
        0; // compteur indiquant à quel moment la tâche a été ordonnancée

    //==================================================
    xbt_dynar_foreach(dag, cursor, task) { SD_task_watch(task, SD_DONE); }
    //==================================================
    // scheduling
    //------------
    xbt_dynar_t changed_tasks = xbt_dynar_new(sizeof(SD_task_t), NULL);
    // attribution des VMs
    for (std::list<SD_task_t>::iterator it = lT.begin(); it != lT.end(); ++it) {
      sg_host_t bHost;
      //      std::cout <<"youh "<<SD_task_get_name(*it)  <<"\n\n";

      // get best host
      // bHost = SD_task_get_best_host_heft_budget(*it);
      // bHost = SD_task_get_best_host_heft_budget(SD_task_t task,
      // std::map<sg_host_t, HostCost> mhc, std::map<SD_task_t, double>
      // budgPTsk, double cagnotte, double &timeCalc, double &cagnotteFin);
      //      bHost = SD_task_get_best_host_heft_budget(*it, mhc, budgPTsk,
      //      cagnotte, timeCalc,  *(volatile double*) &cagnotteFin);
      bHost = SD_task_get_best_host_heft_budget(*it, mhc, budgPTsk, cagnotte,
                                                timeCalc, cagnotteFin);

      // essai : berk en variable globale
      //      cagnotteFin = berk;

      //      std::cout << "hôte : " << sg_host_get_name(bHost) <<"\n";

      // attribution
      SD_task_schedulel(*it, 1, bHost);

      // attribution des enfants (car on ne suit pas l'ordre d'exécution pour
      // affecter les tâches, les attributions de comm automatiques ne se feront
      // pas d'elles-mêmes. On place donc ce qu'on sait : on aura au moins de la
      // comm sur l'hôte choisi, vu que la tâche va envoyer ses résultats)
      xbt_dynar_t children = SD_task_get_children(task);
      if (!xbt_dynar_is_empty(children)) {
        SD_task_t child;
        unsigned int i;
        xbt_dynar_foreach(children, i, child) {
          if (SD_task_get_kind(child) == SD_TASK_COMM_E2E) {
            if (SD_task_get_state(child) == SD_NOT_SCHEDULED) {
              SD_task_schedulel(child, 1, bHost);
            }
          }
        }
      }

      xbt_dynar_free_container(&children);

      m[*it] = bHost;
      // associer son numéro d'ordre d'ordonnancement
      nb_ord[*it] = nb_ordo_tmp; // vraiment pour garder le template TODO: faire
                                 // la même proprement
      // incrémenter pour avoir le numéro suivant
      nb_ordo_tmp++;

      cagnotte = cagnotteFin;

      double min_finish_time = finish_on_at(*it, bHost);
      //=======================
      // SimDag allows tasks to be executed concurrently when they can by
      // default. Yet schedulers take decisions assuming that tasks wait for
      // resource availability to start. The solution (well crude hack is to
      // keep track of the last task scheduled on a host and add a special type
      // of dependency if needed to force the sequential execution meant by the
      // scheduler. If the last schxeduled task is already done, has failed or
      // is a predecessor of the current task, no need for a new dependency

      SD_task_t last_scheduled_task = sg_host_get_last_scheduled_task(bHost);
      // pour la prise en compte du nombre de coeurs, c'est là qu'il faut taper
      // (rajouter un compteur ou un tableau, ou alors traduire le nombre de
      // coeurs dans la plateforme (un gros cluster de machine mono-coeur
      if (last_scheduled_task &&
          (SD_task_get_state(last_scheduled_task) != SD_DONE) &&
          (SD_task_get_state(last_scheduled_task) != SD_FAILED) &&
          !SD_task_dependency_exists(sg_host_get_last_scheduled_task(bHost),
                                     *it)) {
        SD_task_dependency_add("resource", NULL, last_scheduled_task, *it);
      }
      sg_host_set_last_scheduled_task(bHost, *it);
      sg_host_set_available_at(bHost, min_finish_time);

      //=======================

      // simulate with update
      SD_simulate_with_update(-1.0, changed_tasks);
    }

    //===================================================================
    // demande expresse de Yves R., à refaire proprement
    FILE *fich;
    char *fileName = "credo";

    double totCost = budgetIni - cagnotte;
    double estTime = SD_get_clock();
    char *nomAlgo = "heftBudg2";
    fich = fopen(fileName, "a+");
    if (fich != NULL) {
      fprintf(fich, "%s, %f, %f, %f\n", nomAlgo, budgetIni, totCost, estTime);
    }

    //===================================================================

    //==================================================
    // sending results
    //---------------------
    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number;
        ordo_number = nb_ord[task];
        if (write(fd[1], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error write ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(sg_host_get_name(m[task]));
        if (write(fd[1], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error write hostname length\n");
          exit(-1);
        }

        while (hostnamelen != 0) {
          size_t nb_bytes =
              write(fd[1], sg_host_get_name(m[task]), hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de write dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        }
      }
    }

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    deleteWf(wf);

    size_t total_nhosts = sg_host_count();
    // sg_host_t *hosts = sg_host_list();

    for (cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } // end child
  else {
    // fait l'ordo
    close(fd[1]); /* close write */
    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number = 0;
        if (read(fd[0], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error read ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(SD_task_get_name(task));
        if (read(fd[0], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error read hostname length\n");
          exit(-1);
        }

        unsigned int hostLen =
            hostnamelen; // to keep the lenght of the recieved host name
                         // somewhere and be able to put the \0 at the end of
                         // the reception
        char *nomHost = (char *)malloc(sizeof(char) * (hostnamelen + 1));
        while (hostnamelen != 0) {
          size_t nb_bytes = read(fd[0], nomHost, hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de read dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        } // end of receive name of host

        nomHost[hostLen] = '\0';
        // std::cout <<"j'ai lu le nom de l'hôte : " <<nomHost <<" pour la tâche
        // : " <<SD_task_get_name(task) <<" d'indice : " <<ordo_number  <<"\n";
        m[task] = sg_host_by_name(nomHost);
        ordoT[ordo_number] = task;
        free(nomHost);
      } // end of if task is executable
    }   // end of foreach

    int stat_val;
    waitpid(childpid, &stat_val, 0);
  } // end parent
  xbt_free(hosts);
}

#endif
