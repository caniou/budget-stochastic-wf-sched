#ifndef HARVESTING_DATA_H
#define HARVESTING_DATA_H

#include "hostCost.h"
#include "workflow.h"

void minCostSched(Workflow wf, std::map<sg_host_t, HostCost> mhc, double *res);

void maxCostSched(Workflow wf, std::map<sg_host_t, HostCost> mhc, double *res,
                  std::map<SD_task_t, SD_task_t> mCommRootDest);

void dataGen(char *fileName, Workflow wf, std::map<sg_host_t, HostCost> mhc,
             double budget, double cagnotte, double deadline, char *wfName,
             char *typeFich, sg_host_t *hosts, double worstTTime,
             double worstTCost, double diffR, double pCSd, char *nomAlgo,
             double varTheor, double seed,
             std::map<SD_task_t, SD_task_t> mCommRootDest, double worstCTime,
             double worstCCost);

#endif