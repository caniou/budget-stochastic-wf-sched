#include "hostCost.h"
#include "toolsSched.h"
#include "workflow.h"
#include <iostream>
#include <map>
#include <sys/wait.h>

/*
 * find the cost of the cheapest scheduling (every tasks on the cheapest VM)
 * and the time needed to execute it. The results are written in a given array
 * of double, in this order : {cost, time}
 * !!!!!! Suppose that the environment (platform) has already been loaded !!!!
 */
void minCostSched(Workflow wf, std::map<sg_host_t, HostCost> mhc,
                         double *res) {
  size_t cursor;
  SD_task_t task;
  size_t nhosts = sg_host_count();
  // cet algo remet tout l'ordre dans lequel les tâches sont ordonnancées en
  // question pendant ses calculs du meilleur hôte pour la meilleure tâche : une
  // simu est à faire dès le début

  // let's fork
  int fd[2];
  pid_t childpid;

  xbt_dynar_t dax = getDagWf(wf);
  sg_host_t *hosts = sg_host_list();

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    //==========
    // Child
    //-------
    close(fd[0]); /* close read */

    // initialisation : hôte le moins cher

    sg_host_t h = hosts[0]; // hote premier, pour initialisation
    double cHor = getHC_hC(mhc[hosts[0]]);

    for (size_t i = 0; i < nhosts; i++) {
      // si moins cher, devient le best host
      if (getHC_hC(mhc[hosts[i]]) <= cHor) {
        cHor = getHC_hC(mhc[hosts[i]]);
        h = hosts[i];
      }
    }

    std::cout << sg_host_get_name(h) << "\n";

    xbt_dynar_foreach(dax, cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {
        SD_task_schedulel(task, 1, h);
      }
    }

    SD_simulate(-1);

    double time = SD_get_clock();
    //    printf("ici minCostSched, Simulation time moins cher : %f seconds\n",
    //    time);

    double costVM = getHC_iniC(mhc[h]) + getHC_hC(mhc[h]) * time;
    //    printf("coût moins cher : %f\n", costVM);
    //    printf("données : %f, %f\n", getHC_hC(mhc[h]), getHC_iniC(mhc[h]));

    size_t cursorComm;
    // plus le prix des données :
    // pour chaque mois, payer quantité d'infos * coût de stockage par Go
    int nbMonths = 1 + time / (3600 * 24 * 30);
    //======================================================
    // calcul de la quantité de données à transférer :
    double amountData = 0;

    SD_task_t root, end;

    // récupération de root et de end
    xbt_dynar_foreach(getDagWf(wf), cursorComm, task) {
      if (strcmp(SD_task_get_name(task), "root") == 0) {
        xbt_dynar_t startChildren = SD_task_get_children(task);
        size_t cursorChild;
        SD_task_t sChild;
        xbt_dynar_foreach(startChildren, cursorChild, sChild) {
          // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
          // <<SD_task_get_name(sChild) <<"\n";
          amountData += SD_task_get_amount(sChild);
        }
        xbt_dynar_free_container(&startChildren);
      }

      //--------------------------------------------------------
      if (strcmp(SD_task_get_name(task), "end") == 0) {
        xbt_dynar_t endParents = SD_task_get_parents(task);
        size_t cursorPar;
        SD_task_t ePar;
        xbt_dynar_foreach(endParents, cursorPar, ePar) {
          // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
          // <<SD_task_get_name(sChild) <<"\n";
          amountData += SD_task_get_amount(ePar);
        }
        xbt_dynar_free_container(&endParents);
      }
    }

    double costData = nbMonths * amountData / 1000000000 * STORE_COST +
                      amountData / 1000000000 * TRANSF_COST;
    costVM = costVM - costData;

    if (write(fd[1], &time, sizeof(double)) <= 0) {
      printf("Error write time for cheapest ordo\n");
      exit(-1);
    }

    if (write(fd[1], &costVM, sizeof(double)) <= 0) {
      printf("Error write cost for cheapest ordo\n");
      exit(-1);
    }

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    // freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    // deleteWf(wf);
    size_t total_nhosts = sg_host_count();

    for (cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } else {
    //============
    // Parent
    //--------
    close(fd[1]); /* close write */

    double dTest;

    // read(fd[0], &dTest, sizeof(dTest));
    if (read(fd[0], &dTest, sizeof(double)) <= 0) {
      printf("Error read time for minimal cost\n");
      exit(-1);
    }
    std::cout << "je viens de lire le temps : " << dTest << "\n";
    res[0] = dTest;

    if (read(fd[0], &dTest, sizeof(double)) <= 0) {
      printf("Error read cost for minimal cost\n");
      exit(-1);
    }
    std::cout << "je viens de lire le cost : " << dTest << "\n";
    res[1] = dTest;

    int stat_val;
    waitpid(childpid, &stat_val, 0);
  }

  xbt_free(hosts);
}

/*
 * find the cost of the cheapest scheduling (every tasks on the cheapest VM)
 * and the time needed to execute it. The results are written in a given array
 * of double, in this order : {cost, time}
 * !!!!!! Suppose that the environment (platform) has already been loaded !!!!
 *
 * wf: workflow
 * mhc: map <host, cost>
 * res: {cost, time} of the cheapest scheduling
 * mCommRootDest: map <communication from root, destination task>
 */
void maxCostSched(Workflow wf, std::map<sg_host_t, HostCost> mhc,
                         double *res,
                         std::map<SD_task_t, SD_task_t> mCommRootDest) {
  size_t cursor;
  SD_task_t task;

  // cet algo remet tout l'ordre dans lequel les tâches sont ordonnancées en
  // question pendant ses calculs du meilleur hôte pour la meilleure tâche : une
  // simu est à faire dès le début

  size_t nbTasks = 0;
  xbt_dynar_t dax = getDagWf(wf);
  sg_host_t *hosts = sg_host_list();
  size_t nhosts = sg_host_count();
  size_t cpt;
  sg_host_t h = hosts[0]; // hote premier, pour initialisation

  xbt_dynar_foreach(dax, cpt, task) {
    if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {
      nbTasks++;
    }
  }

  nbTasks -= 2;

  if (nhosts < nbTasks) {
    printf("not enough hosts for every task\n");
    res[0] = -1;
    res[1] = -1;
  } else {

    // let's fork
    int fd[2];
    pid_t childpid;

    if (pipe(fd) == -1) {
      fprintf(stderr, "pipe: %s", strerror(errno));
      exit(EXIT_FAILURE);
    }
    if ((childpid = fork()) == -1) {
      perror("fork");
      exit(1);
    }
    if (childpid == 0) {
      //==========
      // Child
      //-------
      close(fd[0]); /* close read */

      // initialisation : hôte le plus cher

      double iniComp; // equivalent in number of instructions of the time needed
                      // to start the VM
      double newAmount; // new amount of work for the task (start time included)

      xbt_dynar_foreach(dax, cursor, task) {
        // std::cout <<"curio : " <<SD_task_get_name(task) <<", amount : "
        // <<SD_task_get_amount(task) <<"\n";

        if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {
          h = getMostExpensiveFreeHost(mhc);
          setHC_resTime(mhc[h], 10);
          // std::cout <<"pire coût host choisi : " << sg_host_get_name(h)
          // <<"\n";

          // If the scheduled task start a new VM, add the start time
          if (sg_host_get_last_scheduled_task(h) == NULL) {
            iniComp = getHC_tmpIni(mhc[h]) * sg_host_speed(h);
            newAmount = SD_task_get_amount(task) + iniComp;
            SD_task_set_amount(task, newAmount);
          }

          SD_task_schedulel(task, 1, h);
          sg_host_set_last_scheduled_task(h, task);
        }
      }

      realValues(wf);
      SD_simulate(-1);

      //      xbt_dynar_foreach(dax, cursor, task) {
      // std::cout <<"curio : " <<SD_task_get_name(task) <<", amount : "
      // <<SD_task_get_amount(task) <<"\n";}

      //+===================================================================================
      // calcul des coûts
      double costTotVM = 0.;
      double pourcentUsed = 0.;
      std::set<sg_host_t> usedHosts;         // liste des hôtes utilisés
      std::map<sg_host_t, double> tpsVMCalc; // récupération des temps de calcul

      double nbVM = 0.;
      long nbTsk = 0;
      double finWf;
      double hStart = 0.;

      std::map<sg_host_t, SD_task_t>
          starts; // tâches d'initialisation : pour récupérer les heures de
                  // début de lancement de machine + début de temps de premier
                  // téléchargemen
      std::map<sg_host_t, SD_task_t> ends; // tâches de fin de VM

      //=======================================================================================
      // real end of the workflow = start time of end
      cursor = 0;

      xbt_dynar_foreach(getDagWf(wf), cursor, task) {

        sg_host_t *task_host = SD_task_get_workstation_list(
            task); // !!!! the comparison with the forst host of the list works
                   // only because there is at most one VM for one task
        // tache de fin = heure de fin
        if (strcmp(SD_task_get_name(task), "end") == 0) {
          // std::cout <<"tâche end !\n";
          // std::cout <<"début de la tâche end : "
          // <<SD_task_get_start_time(task) <<"\n";
          finWf = SD_task_get_start_time(task);
        }
        if (strcmp(SD_task_get_name(task), "root") == 0) {
          // std::cout <<"tâche root !\n";
          hStart = SD_task_get_start_time(task);

          // std::cout <<"       ########################### récolte, tâche : "
          // <<SD_task_get_name(task) <<" sur la machine : "
          // <<sg_host_get_name(task_host[0]) <<"qui débute à "
          // <<SD_task_get_start_time(task) <<", qui finit à "
          // <<SD_task_get_finish_time(task) <<"\n";
        }

        //-----------------------------------------------------------
        // S'il s'agit des tâches fantômes root et end, NE PAS LES PRENDRE EN
        // COMPTE, ce ne sont pas de "vraies" tâches, idem si la tâche est une
        // des comm de root
        if ((strcmp(SD_task_get_name(task), "root") != 0) &&
            (strcmp(SD_task_get_name(task), "end") != 0) &&
            (mCommRootDest.find(task) == mCommRootDest.end())) {
          // récupération des tâches de départ
          if (starts.find(task_host[0]) == starts.end()) {
            starts[task_host[0]] = task;
          } else {
            if (SD_task_get_start_time(starts[task_host[0]]) >
                SD_task_get_start_time(task)) {
              starts[task_host[0]] = task;
            }
          }

          // récupération des tâches de fin
          if (ends.find(task_host[0]) == ends.end()) {
            ends[task_host[0]] = task;
          } else {
            // if(SD_task_get_finish_time(ends[task_host[0]]) <
            // SD_task_get_finish_time(task)){
            if ((strcmp(SD_task_get_name(task), "end") != 0) &&
                (SD_task_get_finish_time(ends[task_host[0]]) <
                 SD_task_get_finish_time(task))) {
              ends[task_host[0]] = task;
            }
          }

          // récupération du nombre de tâches total
          if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {
            nbTsk++;
          }

          // récupération du nombre d'hôtes utilisés
          if (tpsVMCalc.find(task_host[0]) == tpsVMCalc.end()) {
            // initialisation du temps de calcul
            tpsVMCalc[task_host[0]] = 0.;

            // ajout de l'hôte à la liste des hôtes
            usedHosts.insert(task_host[0]);
          }

          // récupération du temps de calcul pur par machine (sans compter les
          // transferts et les attentes)
          tpsVMCalc[task_host[0]] +=
              SD_task_get_amount(task) / sg_host_speed(task_host[0]);
        } // end not root, not end
          //-----------------------------------------------

      } // end of the foreach task

      for (std::set<sg_host_t>::iterator it = usedHosts.begin();
           it != usedHosts.end(); ++it) {
        nbVM++;
        std::cout << ' ' << sg_host_get_name(*it);
        // récupération de la première tâche de chaque machine
        // SD_task_t tmpTsk = starts[*it];
        // std::cout <<"première tâche : " <<SD_task_get_name(tmpTsk) <<", heure
        // de début : " <<SD_task_get_start_time(tmpTsk) <<"\n";
        // fin d'exec de machine = tous les fichiers ont été envoyés, envoi
        // séquentiel
        // std::cout << "bande passante de l'hôte : "
        // <<sg_host_route_bandwidth(hosts[0], hosts[1]) <<"\n";

        // std::cout<< "dernière tâche schedulée : "
        // <<SD_task_get_name(sg_host_get_last_scheduled_task(*it)) <<", heure
        // de fin de calcul de ladite tâche : "
        // <<SD_task_get_finish_time(sg_host_get_last_scheduled_task(*it))
        // <<"\n"; std::cout<< "dernière tâche effectuée : "
        // <<SD_task_get_name(ends[*it]) <<", heure de fin de calcul de ladite
        // tâche : " <<SD_task_get_finish_time(ends[*it]) <<"\n";

        // quantité de données à transférer avant fermeture :
        xbt_dynar_t children =
            SD_task_get_children(sg_host_get_last_scheduled_task(*it));
        double transfTime = 0.;
        double amountData = 0.;
        // truandage sur la bande passante, vu que je sais que c'est la même
        // partout et que j'ai plusieurs hôtes :
        double bwGlob = sg_host_route_bandwidth(hosts[0], hosts[1]);

        std::map<sg_host_t, double> tpsVMH;

        if (!xbt_dynar_is_empty(children)) {
          SD_task_t child, gChild;
          int iChild, iGChild;

          xbt_dynar_foreach(children, iChild, child) {

            if (SD_task_get_kind(child) == SD_TASK_COMM_E2E) {

              // n'ajouter que si la comm est un transfert des dernières infos
              // du workflow, pour les mener hors cloud
              xbt_dynar_t gChildren = SD_task_get_children(child);
              if (!xbt_dynar_is_empty(
                      gChildren)) { // which should never happen, knowing that
                                    // child is a communication task
                xbt_dynar_foreach(gChildren, iGChild, gChild) {
                  if (strcmp(SD_task_get_name(gChild), "end") == 0) {
                    amountData += SD_task_get_amount(child);
                  }
                }
              }

              // amountData += SD_task_get_amount(child);
              //	    std::cout<< "data à transférer avant fermeture : "
              //<<amountData << "\n";
            } // endif comm

          } // end foreach child

        } // endif there are children

        // test : on vide
        xbt_dynar_free_container(&children);

        // récupération des temps liés aux transferts provenant de root (comptés
        // dans le prix, essentiel pour avoir les vrais coûts par VMs mais qui
        // ont eu le mauvais goût d'être artificiellement placés sur la VM de
        // root)
        size_t cursorComm;
        xbt_dynar_foreach(getDagWf(wf), cursorComm, task) {

          if (mCommRootDest.find(task) != mCommRootDest.end()) {
            sg_host_t *inter =
                SD_task_get_workstation_list(mCommRootDest[task]);
            // si l'hôte concerné est l'hôte qu'on est en train de regarder,
            // ajouter le fichier à la quantité de fichiers à transmettre
            if (inter[0] == *it) {
              amountData += SD_task_get_amount(task);

              // std::cout <<"       récolte, tâche : " <<SD_task_get_name(task)
              // <<"qui débute à " <<SD_task_get_start_time(task) <<", qui finit
              // à " <<SD_task_get_finish_time(task) << " ; elle a pour
              // destination " <<SD_task_get_name(mCommRootDest[task]) <<" qui
              // se trouve sur : " <<sg_host_get_name(inter[0]) <<" et a une
              // quantité de : " <<SD_task_get_amount(task) <<"\n";
            }
          }
        }

        transfTime = amountData / bwGlob;

        // std::cout<< "pire Temps de transfert avant fermeture : " <<transfTime
        // << "\n";

        // temps de calcul total :
        // heure de fin + transfTime - heure de début
        // double totalTimeCalcVM =
        // SD_task_get_finish_time(sg_host_get_last_scheduled_task(*it)) +
        // transfTime - SD_task_get_start_time(tmpTsk); // faux : ce qui est
        // schedulé en premier n'est pas nécessairement ce qui est lancé en
        // premier
        double totalTimeCalcVM =
            SD_task_get_finish_time(ends[*it]) + transfTime -
            SD_task_get_start_time(
                starts[*it]); // + temps de transfert depuis root

        // std::cout <<"temps de calcul total : " <<totalTimeCalcVM <<"\n";
        tpsVMH[*it] = totalTimeCalcVM;

        // pourcentage d'utilisation de la VM :
        if (tpsVMH[*it] != 0) {
          // std::cout<<"pourcentage utilisation : " <<100* tpsVMCalc[*it] /
          // tpsVMH[*it] <<"\n";
          pourcentUsed += tpsVMCalc[*it] / tpsVMH[*it];
        } else {
          // si ça arrive et que la VM est utilisée, c'est qu'on est dans le cas
          // où il n'y a que les tâches vides end et start pourcentUsed += 1;
          pourcentUsed += 0.;
        }

        // coût associé : temps de calcul * coût horaire + coût ini

        //	std::cout<< "youyou, totalTimeCalcVM = " <<totalTimeCalcVM
        //<<"\n"; 	std::cout <<"youyou, getHC_iniC(mhc[*it]) = "
        //<<getHC_iniC(mhc[*it]) <<"\n"; 	std::cout <<"youyou,
        //getHC_hC(mhc[*it]) = " <<getHC_hC(mhc[*it]) <<"\n";
        double costVM =
            getHC_iniC(mhc[*it]) + getHC_hC(mhc[*it]) * totalTimeCalcVM;
        //	std::cout <<"cout total VM -- costVM : " <<costVM <<"\n";
        costTotVM += costVM;
        //	std::cout <<"          ########################## pire cout
        // total actuel --costTotVM : " <<costTotVM <<"\n"; std::cout <<"\n";
      } // end of the for on the used hosts

      //      std::cout <<"          ########################## pire cout total
      //      : " <<costTotVM <<"\n"; std::cout << " ##########################
      //      pire nbVM : " <<nbVM <<"\n";

      //++==================================================================================

      //      double time = SD_get_clock();
      double time = (finWf - hStart);
      // printf("ici maxCostSched, Simulation time plus cher : %f seconds\n",
      // time); std::cout << "temps de calcul du workflow total en pire dépense
      // : " << time <<"\n";

      // calcul du coût :

      size_t cursorComm;
      // plus le prix des données :
      // pour chaque mois, payer quantité d'infos * coût de stockage par Go
      int nbMonths = 1 + time / (3600 * 24 * 30);
      //======================================================
      // calcul de la quantité de données à transférer :
      double amountDataIO = 0;

      SD_task_t root, end;

      // récupération de root et de end
      xbt_dynar_foreach(getDagWf(wf), cursorComm, task) {
        if (strcmp(SD_task_get_name(task), "root") == 0) {
          xbt_dynar_t startChildren = SD_task_get_children(task);
          size_t cursorChild;
          SD_task_t sChild;
          xbt_dynar_foreach(startChildren, cursorChild, sChild) {
            // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
            // <<SD_task_get_name(sChild) <<"\n";
            amountDataIO += SD_task_get_amount(sChild);
          }
          xbt_dynar_free_container(&startChildren);
        }

        //--------------------------------------------------------
        if (strcmp(SD_task_get_name(task), "end") == 0) {
          xbt_dynar_t endParents = SD_task_get_parents(task);
          size_t cursorPar;
          SD_task_t ePar;
          xbt_dynar_foreach(endParents, cursorPar, ePar) {
            // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
            // <<SD_task_get_name(sChild) <<"\n";
            amountDataIO += SD_task_get_amount(ePar);
          }
          xbt_dynar_free_container(&endParents);
        }
      }

      double costData = nbMonths * amountDataIO / 1000000000 * STORE_COST +
                        amountDataIO / 1000000000 * TRANSF_COST;

      double costVM = costTotVM;
      costVM = costVM + costData;

      std::cout << "   pire costData test : " << costData << "\n";
      std::cout << "   pire costTotVM test : " << costTotVM << "\n";

      //      double costVM = getHC_iniC(mhc[h]) + getHC_hC(mhc[h]) * time;
      //    printf("coût moins cher : %f\n", costVM);
      //    printf("données : %f, %f\n", getHC_hC(mhc[h]), getHC_iniC(mhc[h]));

      if (write(fd[1], &time, sizeof(double)) <= 0) {
        printf("Error write time for most expensive ordo\n");
        exit(-1);
      }

      if (write(fd[1], &costVM, sizeof(double)) <= 0) {
        printf("Error write cost for most expensive ordo\n");
        exit(-1);
      }

      //==============================================
      // cleaning...
      // tachesSrc.clear();
      //  mapInfosTask.clear();
      // freeUtilsMath(); // freeing the generator
      //  xbt_dynar_free_container(&dax);
      // deleteWf(wf);
      size_t total_nhosts = sg_host_count();

      for (cursor = 0; cursor < total_nhosts; cursor++) {
        free(sg_host_user(hosts[cursor]));
        sg_host_user_set(hosts[cursor], NULL);
      }

      xbt_free(hosts);
      SD_exit();
      exit(0);

    } else {
      //============
      // Parent
      //--------
      close(fd[1]); /* close write */

      double dTest;

      // read(fd[0], &dTest, sizeof(dTest));
      if (read(fd[0], &dTest, sizeof(double)) <= 0) {
        printf("Error read time for maximal cost\n");
        exit(-1);
      }
      std::cout << "je viens de lire le temps pour les pires dépenses : "
                << dTest << "\n";
      res[0] = dTest;

      if (read(fd[0], &dTest, sizeof(double)) <= 0) {
        printf("Error read cost for maximal cost\n");
        exit(-1);
      }
      std::cout << "je viens de lire le cost pour les pires dépenses : "
                << dTest << "\n";
      res[1] = dTest;

      int stat_val;
      waitpid(childpid, &stat_val, 0);
    }
    // xbt_free(hosts);
  }
  xbt_free(hosts);
}

//==========================================================================================================================================================================
// fonction brute de décoffrage pour alléger et clarfier hex, à exploser en sous
// fonctions par la suite (carté + facilité de changement de l'output)
/**
 * Takes informations about the run, the executed worflow and the used paltform,
 *and generates a csv file ready to be studied
 **/
void dataGen(char *fileName, Workflow wf,
                    std::map<sg_host_t, HostCost> mhc, double budget,
                    double cagnotte, double deadline, char *wfName,
                    char *typeFich, sg_host_t *hosts, double worstTTime,
                    double worstTCost, double diffR, double pCSd, char *nomAlgo,
                    double varTheor, double seed,
                    std::map<SD_task_t, SD_task_t> mCommRootDest,
                    double worstCTime, double worstCCost) {

  FILE *fich;
  //    char* nomFich = "commonResults";

  fich = fopen(fileName, "a+");
  if (fich != NULL) {

    FILE *detailedFich;
    char namedetailedFich[] = "commonPreciseResults";
    detailedFich = fopen(namedetailedFich, "a+");
    if (detailedFich != NULL) {

      //=============================================================================
      // réelle récolte
      //----------------

      double costTotVM = 0.;
      double pourcentUsed = 0.;
      std::set<sg_host_t> usedHosts;         // liste des hôtes utilisés
      std::map<sg_host_t, double> tpsVMCalc; // récupération des temps de calcul
      std::map<sg_host_t, double>
          tpsVMUsed; // récupération des temps de réservation de la VM
      double nbVM = 0.;
      long nbTsk = 0;
      double finWf = 0.;
      double hStart = 0.;

      std::map<sg_host_t, SD_task_t>
          starts; // tâches d'initialisation : pour récupérer les heures de
                  // début de lancement de machine + début de temps de premier
                  // téléchargement
      std::map<sg_host_t, SD_task_t> ends; // tâches de fin de VM

      //----------------------------------------------------------------------------------------------------------------------
      // calculus: cost and makespan
      //    costTimeResults(mhc,mCommRootDest,starts,ends, wf, finWf, hStart,
      //    nbTsk, usedHosts, hosts, tpsVMCalc, nbVM, costTotVM, pourcentUsed);

      costTimeResultsVerbose(mhc, mCommRootDest, starts, ends, wf, finWf,
                             hStart, nbTsk, usedHosts, hosts, tpsVMCalc, nbVM,
                             costTotVM, pourcentUsed, tpsVMUsed);
      //----------------------------------------------------------------------------------------------------------------------

      std::cout << "cout total : " << costTotVM << "\n";
      std::cout << "nom fichier : " << wfName << "\n";
      // nb tasks = tot nb tasks - |{root,end}|
      std::cout << "nb tâches : " << nbTsk << "\n";

      //============== calcul temps de calcul par VM ========
      std::cout << "nb host utilisés : " << usedHosts.size() << "\n";

      // pourcentage global utilisé :
      // double prctUsedVM = 100*pourcentUsed /usedHosts.size();
      double prctUsedVM = 100 * pourcentUsed / nbVM;
      std::cout << "pourcentage d'utilisation du temps réservé : " << prctUsedVM
                << "\n";
      std::cout << "temps de calcul du workflow total : " << (finWf - hStart)
                << "\n";

      double tmpWf = finWf - hStart;
      //========= remplissage du csv commun ==========
      // fprintf(fich, "Wf,type, tmpWf,costWf, budget_residuel, budget_ini,
      // nb_VM_used, prct_VM_used\n");

      double budgRes = 0.;
      //      char* typeFich = "ligo";

      budgRes = budget - costTotVM;
      fprintf(fich,
              "%s,%s, %s, %li, %f, %f, %f, %f, %lu, %f, %f, %f, %f, %f, %f, %f, "
              "%f, %f\n",
              wfName, typeFich, nomAlgo, nbTsk, tmpWf, costTotVM, budgRes,
              budget, usedHosts.size(), prctUsedVM, worstTTime, worstTCost,
              diffR, varTheor, pCSd, seed, worstCTime, worstCCost);
      // var : écart-type  = var * mean (var n'est pas une variance, mais un
      // pourcentage, ici)

      // in parallel another file, "credo", is filled and store the values
      // expected by the offline algorithm

      // vérification des informations récupérées concernant les VMs
      for (std::set<sg_host_t>::iterator it = usedHosts.begin();
           it != usedHosts.end(); ++it) {
        std::cout << "L'hôte s'appelle " << sg_host_get_name(*it) << "\n";
        std::cout << "\tIl a été utilisé pendant " << tpsVMUsed[*it]
                  << "secondes\n";
        std::cout << "\tIl a calculé pendant " << tpsVMCalc[*it]
                  << "secondes\n";

        std::cout << "\tIl est de type " << sg_host_get_name(*it)[2] << "\n";

        fprintf(detailedFich,
                "%s,%s, %s, %li, %f, %f, %f, %f, %lu, %f, %f, %f, %f, %f, %f, "
                "%f, %f, %f, %s, %c, %f, %f\n",
                wfName, typeFich, nomAlgo, nbTsk, tmpWf, costTotVM, budgRes,
                budget, usedHosts.size(), prctUsedVM, worstTTime, worstTCost,
                diffR, varTheor, pCSd, seed, worstCTime, worstCCost,
                sg_host_get_name(*it), sg_host_get_name(*it)[2], tpsVMUsed[*it],
                tpsVMCalc[*it]);

        // un exemple qui utilise les trois types de VM :
        // bin/hex heftBudg 24 data/workflows/Ligo/ligo60_1
        // data/platforms/AL60.xml 0.25 0.2 900 1850 0.2
      }
      // fin de vérification

      fclose(detailedFich);
    }

    fclose(fich);
  }
  // if not, the res file will not be open and the results will not be written
}