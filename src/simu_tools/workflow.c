#include "workflow.h"
#include "utilsMath.h"
#include <iostream>

//====================================================
// getters/setters/initialisers/free-ers
//---------------------------------------
void setDagWf(Workflow &wf, xbt_dynar_t dag) {
  wf.dag = dag; // Beware, if their dag is thought like their task it is a
                // pointer to the real thing After check : it is !
}

//======================================================
// tools
//-------
void generateInfosTasksWf(Workflow &wf, double pCSd) {
  int cpt;
  SD_task_t task;

  if (pCSd != -1) {
    // pour chaque tâche du workflow qui est exécutable, générer l'info et
    // l'ajouter à la map
    xbt_dynar_foreach(getDagWf(wf), cpt, task) {
      if (isTaskComput(task)) {
        // générer l'infotask kivabien
        InfoStochTask ist;

        // double sd = 200000000.0; // tmp value, to be replaced with a list
        // given by the user
        double sd = SD_task_get_amount(task) * pCSd;
        initInfoStochTask(ist, task, sd);
        // std::cout <<getSdIst(ist)<<"\n";
        // faire le lien dans la map
        wf.mapInfosTask[task] = ist;
      }
    }
  } else {
    // aucune indication n'a été donnée : valeurs par défaut
    printf("pas d'information donnée concernant la variance : génération des "
           "valeurs réelles impossible");
    exit(1);
  }
}

/**
 * Randomize amount
 * replace the time of the task by some random values
 * using the given value as base to determine the
 * standard value of the distribution followed by the
 * random number :
 * nouvAmount ~ N(origTime, pCSs * origTime)
 *
 * !!!!! call it BEFORE generateInfosTasksWf or you'll have to regenerate the
 *infos yourself !!!!!
 *
 * pCSd : can have any positive value
 **/
void randAmountWTaskWf(Workflow &wf, double pCSd) {
  if (pCSd != -1) {
    size_t cpt;
    SD_task_t task;
    double newVal, sd, mean;

    // pour chaque tâche du workflow qui est exécutable, générer l'info et
    // l'ajouter à la map
    xbt_dynar_foreach(wf.dag, cpt, task) {
      if (SD_TASK_COMP_SEQ == SD_task_get_kind(task)) {
        if (strcmp(SD_task_get_name(task), "root") != 0) {
          if (strcmp(SD_task_get_name(task), "end") != 0) {

            mean = SD_task_get_amount(task);
            sd = mean * pCSd;
            // newVal = sampleNormal(mean, sd);
            newVal = sampleUniform(mean, sd);

            if (newVal < 0.1)
              newVal = 1e-6;
            std::cout << SD_task_get_name(task)
                      << ": valeur changée, nouvelle valeur : " << newVal
                      << " ; sd : " << sd << ", pCSd : " << pCSd
                      << ", mean : " << mean
                      << ", mean lue directement : " << SD_task_get_amount(task)
                      << "\n";
            SD_task_set_amount(task, newVal);
            //	    std::cout <<SD_task_get_name(task) <<": valeur changée,
            //nouvelle valeur : " <<newVal <<"\n";
          }
        }
      }
    }
  } else {
    printf("pas d'information donnée concernant la variance : génération des "
           "valeurs théoriques impossible");
    exit(1);
  }
}

void loadDaxWf(Workflow &wf, char *fileName) {
  // dax = SD_daxload(argv[1]);
  setDagWf(wf, SD_daxload(fileName));
}

InfoStochTask getInfoStochTaskWf(Workflow wf, SD_task_t task) {
  return wf.mapInfosTask[task];
}

xbt_dynar_t getDagWf(Workflow wf) { return wf.dag; }

void deleteWf(Workflow &wf) {
  wf.mapInfosTask.clear();
  xbt_dynar_free_container(&wf.dag);
}

void realValues(Workflow &wf) {
  int cpt;
  SD_task_t task;

  // pour chaque tâche du workflow qui est exécutable, générer l'info et
  // l'ajouter à la map
  xbt_dynar_foreach(wf.dag, cpt, task) {
    if (SD_TASK_COMP_SEQ == SD_task_get_kind(task)) {
      if (strcmp(SD_task_get_name(task), "root") != 0) {
        if (strcmp(SD_task_get_name(task), "end") != 0) {
          SD_task_set_amount(task, getRealWorkIst(wf.mapInfosTask[task]));
        }
      }
    }
  }
}

void worstValues(Workflow &wf) {
  int cpt;
  SD_task_t task;

  // pour chaque tâche du workflow qui est exécutable, générer l'info et
  // l'ajouter à la map
  xbt_dynar_foreach(wf.dag, cpt, task) {
    if (SD_TASK_COMP_SEQ == SD_task_get_kind(task)) {
      if (strcmp(SD_task_get_name(task), "root") != 0) {
        if (strcmp(SD_task_get_name(task), "end") != 0) {
          SD_task_set_amount(task, getMeanIst(wf.mapInfosTask[task]) +
                                       getSdIst(wf.mapInfosTask[task]));
        }
      }
    }
  }
}
