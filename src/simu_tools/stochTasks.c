#include "stochTasks.h"

//=============================================================================
// Marquer DANS l'algo quand on a la durée exacte et quand on a le temps estimé
//=============================================================================

//====================================================
// getters/setters/initialisers/free-ers
//---------------------------------------

void initInfoStochTask(InfoStochTask &ist, SD_task_t task, double sd) {
  ist.mean = SD_task_get_amount(task);
  ist.sd = sd;
  initRealWork(ist);
}

void initRealWork(InfoStochTask &ist) {
  if (ist.mean != 0) {
    double rTmp = sampleNormal(ist.mean, ist.sd);
    if (rTmp < 0)
      rTmp = 0;
    setRealWorkIst(ist, rTmp);
  }
}

double getRealWorkIst(InfoStochTask ist) { return ist.realWork; }
double getMeanIst(InfoStochTask ist) { return ist.mean; }
double getSdIst(InfoStochTask ist) { return ist.sd; }

void setRealWorkIst(InfoStochTask &ist, double rw) { ist.realWork = rw; }

void setMeanIst(InfoStochTask &ist, double m) { ist.mean = m; }

void setSdIst(InfoStochTask &ist, double sd) { ist.sd = sd; }

//==============================================
// un brin HS, mais je n'avais pas mieux
//---------------------------------------

/**
 * return true if task is a computational task, false otherwise
 **/
bool isTaskComput(SD_task_t task) {
  return SD_task_get_kind(task) == SD_TASK_COMP_SEQ;
}
