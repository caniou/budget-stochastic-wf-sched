#ifndef _WORKFLOW
#define _WORKFLOW

#include "simgrid/simdag.h"
#include "xbt.h"
#include <list>
#include <map>
#include <queue>
#include <string.h>

#include "stochTasks.h"

/**
 * Regroup all the informations proper to a workflow :
 * its dependencies
 * the informations relative to its tasks
 **/
typedef struct _Workflow Workflow;

struct _Workflow {
  std::map<SD_task_t, InfoStochTask>
      mapInfosTask; /// map linking each task with its informations about
                    /// stochasticity
  xbt_dynar_t dag;  /// DAG of dependencies of the workflow
};

//====================================================
// getters/setters/initialisers/free-ers
//---------------------------------------
/**
 * barbarian set for the map of the workflow
 **/
void setMapInfosTasksWf(Workflow &wf,
                        std::map<SD_task_t, InfoStochTask> mapInfosTask);

void setDagWf(Workflow &wk, xbt_dynar_t dag);

/**
 * associates the wf to the dag loaded from the given dax. Memory reservation is
 *done by the load function, with : "xbt_dynar_new(sizeof(SD_task_t),
 *dax_task_free);" (to help to choose which destroy procedure use)
 **/
void loadDaxWf(Workflow &wf, char *fileName);

/**
 * generate the mapInfosTask of an already loaded dag
 **/
void generateInfosTasksWf(Workflow &wf);

/**
 * generate the mapInfosTask of an already loaded dag,
 * using pCSd as an indicator of standard deviation :
 * sd = pCSd * mean
 **/
void generateInfosTasksWf(Workflow &wf, double pCSd);

/**
 * return the InfoStochTask of a given task of the workflow
 **/
InfoStochTask getInfoStochTaskWf(Workflow wf, SD_task_t task);

/**
 * return the dag of wf (beware, it is the actual dag, if the change it, you
 *will change the one of wf)
 **/
xbt_dynar_t getDagWf(Workflow wf);

/**
 * empty the map and free the dag of the given workflow. Beware it WILL destroy
 *the dag.
 **/
void deleteWf(Workflow &wf);

/**
 * Randomize amount
 * replace the time of the task by some random values
 * using the given value as base to determine the
 * standard value of the distribution followed by the
 * random number :
 * nouvAmount ~ N(origTime, pCSs * origTime)
 *
 * !!!!! call it BEFORE generateInfosTasksWf or you'll have to regenerate the
 *infos yourself !!!!!
 *
 * pCSd : can have any positive value
 **/
void randAmountWTaskWf(Workflow &wf, double pCSd);

//====================================================
// Tools
//-------

// simulate theorical dag

// simulate real dag
void realValues(Workflow &wf);

// put in the dag the worst amount possible (mean + sd)
void worstValues(Workflow &wf);

#endif
