#ifndef _UTILSMATH
#define _UTILSMATH

#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>

static gsl_rng *r; /* global generator */

//====================================
// Initialisation and free
//-------------------------

/**
 * Initialise the genrator of random values with a default value
 *(gsl_rng_mt19937)
 **/
void iniUtilsMath();

/**
 * Initialise the generator of random values with a given seed
 **/
void iniUtilsMath(double seed);

/**
 * Free the generator
 **/
void freeUtilsMath();

//=====================================
// Tools
//-----------

/**
 * Generate a random value for a variable following a gaussian law of mean mu
 *and standard deviation sd
 **/
double sampleNormal(double mu, double sd);

/**
 * Generate a random value for a variable following a uniform law in the
 *interval [mu-sd, mu+sd]
 **/
double sampleUniform(double mu, double sd);

/**
 * Return the maximum value between two given integers a and b
 **/
int maxInt(int a, int b);

#endif
