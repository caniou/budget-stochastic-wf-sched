#include "hostCost.h"
#include <iostream>

//====================================================
// getters/setters/initialisers/free-ers
//---------------------------------------

void iniHostCost(HostCost &hc, sg_host_t h) {
  // TODO properly (with a map, a list or something to be able to do the ini
  // automatically
  hc.host = h;
}

void barbSetHostCost(HostCost &hc, sg_host_t h) {
  // génération barbare des infos sur l'host, valable pour la conformation à
  // trois cabinets de datacenter2
  setHC_host(hc, h);
  setHC_resTime(hc, 0);
  setHC_occTime(hc, 0);

  // coût fixé via moyenne des prix de S3
  hc.storeC = STORE_COST;
  setHC_tmpIni(hc, VM_INIT_TIME);
  setHC_iniC(hc, VM_INIT_COST);

  const char *name = sg_host_get_name(h);
  if (strlen(name) >= 3) {
    if (name[2] == '1') {
      if (VM1_COST == 0.145) {
        std::cout << "++++++++++++++ PRICE 1" << std::endl;
      }
      if (VM1_COST == 0.224) {
        std::cout << "++++++++++++++ PRICE 3" << std::endl;
      }
      std::cout << "+++++++++VM1_COST = " << VM1_COST << std::endl;
      setHC_hC(hc, VM1_COST);
    }
    if (name[2] == '2') {
      setHC_hC(hc, VM2_COST);
    }
    if (name[2] == '3') {
      setHC_hC(hc, VM3_COST);
    }

  } else {
    setHC_iniC(hc, 200);
    setHC_hC(hc, 65);
    // std::cout <<"autre\n";
  }
}

/**
 * generates the costs of an already loaded platform, based on the ratio
 *power/cost r r: double, difference in the ratio power/cost between machines
 *with different power
 *
 *
 **/
void barbSetHostCost(HostCost &hc, sg_host_t h, double ratio) {
  // génération barbare des infos sur l'host, valable pour la conformation à
  // trois cabinets de datacenter2

  // coût fixé via moyenne des prix de S3
  hc.storeC = STORE_COST;
  /*
    prix cloud réalistes
    2 coeurs 		0.056 €/h
    4 coeurs		0.111 €/h
    8 coeurs		0.222 €/h
    16 coeurs	0.444 €/h
    32 coeurs	0.889 €/h

    prix base horaire pour VM de type 3 : 13.3 * 0.222/8 ----> 0.369075
    (pas plus irrationnel qu'autre chose, vu qu'on n'a pas la puissance des
    machines correspondantes) Là, on a en fait des secondes (c'est du petit wf)
    : 0.369075/3600

    */
  setHC_host(hc, h);
  setHC_resTime(hc, 0);
  setHC_occTime(hc, 0);

  // if (ratio == -1) {
  // this condition is only use to directly go on the else and call
  // barbSetHostCost(hc, h);
  if (ratio == -1) {
    // datacenter.xml :
    // speed t1 = 5 Gf
    // speed t2 = 8.9 Gf
    // speed t3 = 13.3 Gf

    // double baseCost = 1 * 0.0000000001 * 0.369075/3600; // base : cout par
    // seconde par flops
    double baseCost =
        1 * 0.0000000001 * 0.369075; // base : cout par seconde par flops
    double costFT3 = baseCost;
    double costFT2 = costFT3 * (1 - ratio);
    double costFT1 = costFT2 * (1 - ratio);
    double iniTmp = 0.15;

    double baseIniCost = 2 * 0.0000000001;
    double iniCost3 = baseIniCost;
    double iniCost2 = (1 - ratio) * iniCost3;
    double iniCost1 = (1 - ratio) * iniCost2;

    const char *name = sg_host_get_name(h);
    if (strlen(name) >= 3) {
      if (name[2] == '1') {
        // speed = 5 Gf
        setHC_iniC(hc, sg_host_speed(h) * iniCost1);
        setHC_hC(hc, sg_host_speed(h) * costFT1);
        setHC_tmpIni(hc, iniTmp);

        //	std::cout <<"cab1\n";
        //	std::cout <<"cout horaire : " <<sg_host_speed(h) * costFT1
        //<<"\n";
      }
      if (name[2] == '2') {
        // speed = 8.9 Gf
        setHC_iniC(hc, sg_host_speed(h) * iniCost2);
        setHC_hC(hc, sg_host_speed(h) * costFT2);
        setHC_tmpIni(hc, iniTmp);

        //	std::cout <<"cab2\n";
        //	std::cout <<"cout horaire : " <<sg_host_speed(h) * costFT2
        //<<"\n";
      }
      if (name[2] == '3') {
        // speed = 13.3

        setHC_iniC(hc, sg_host_speed(h) * iniCost3);
        setHC_hC(hc, sg_host_speed(h) * costFT3);
        setHC_tmpIni(hc, iniTmp);

        //	std::cout <<"cab3\n";
        //	std::cout <<"cout horaire : " <<sg_host_speed(h) * costFT3
        //<<"\n";
      }

    } else {
      // default. pas de sens. A remplacer par un honnête message d'erreur.
      setHC_iniC(hc, 200);
      //      setHC_hC(hc, 45);
      setHC_hC(hc, 65);
      setHC_tmpIni(hc, 0.15);
      // std::cout <<"autre\n";
    }
  } else {
    // pas de r de défini dans l'appel à fonction : on applique les valeurs
    // hardcodéees
    barbSetHostCost(hc, h);
  }
}

void setHC_host(HostCost &hc, sg_host_t h) { hc.host = h; }

void setHC_iniC(HostCost &hc, double iniC) { hc.iniC = iniC; }
void setHC_hC(HostCost &hc, double hC) { hc.hC = hC; }

void setHC_tmpIni(HostCost &hc, double tmpIni) { hc.tmpIni = tmpIni; }

void setHC_resTime(HostCost &hc, double resTime) { hc.resTime = resTime; }

void setHC_occTime(HostCost &hc, double occTime) { hc.occTime = occTime; }

void addHC_resTime(HostCost &hc, double resTime) {
  hc.resTime = hc.resTime + resTime;
}

void addHC_occTime(HostCost &hc, double occTime) {
  hc.occTime = hc.occTime + occTime;
}

sg_host_t getHC_host(HostCost hc) { return hc.host; }

double getHC_iniC(HostCost hc) { return hc.iniC; }

double getHC_hC(HostCost hc) { return hc.hC; }

double getHC_tmpIni(HostCost hc) { return hc.tmpIni; }

double getHC_resTime(HostCost hc) { return hc.resTime; }

double getHC_occTime(HostCost hc) { return hc.occTime; }

double getHC_storeC(HostCost hc) { return hc.storeC; }
