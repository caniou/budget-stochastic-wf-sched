#ifndef _HOSTCOST
#define _HOSTCOST

#include "simgrid/simdag.h"
#include "utilsMath.h"
#include "xbt.h"
#include <map>
#include <string.h>


#define VM_INIT_COST 2
#define VM_INIT_TIME 0.16 // 10 min
#define STORE_COST 0.022     // datacenter stockage cost per GB
#define TRANSF_COST 0.055 // cost input/output data in the datacenter in GB

// price indicate in the journal
#ifdef HOST_PRICE_1
#define VM1_COST 0.145
#define VM2_COST 0.247
#define VM3_COST 0.370
#endif

// price find by Binjamyn
#ifdef HOST_PRICE_2 
#define VM1_COST 0.224
#define VM2_COST 1.40
#define VM3_COST 3.64
#endif


/**
 * Regroup all the informations proper to an host :
 * its costs
 * the time needed to boot
 **/
typedef struct _HostCost HostCost;

struct _HostCost {
  sg_host_t host;
  double iniC; // coût ini
  double hC;   // coût horaire

  double tmpIni; // tmps d'ini

  double resTime; // temps réservé
  double occTime; // temps occupé

  double storeC; // coût horaire du stockage de données
};

//====================================================
// getters/setters/initialisers/free-ers
//---------------------------------------

void iniHostCost(HostCost &hc, sg_host_t h);

void barbSetHostCost(HostCost &hc, sg_host_t h);

/**
 * generates the costs of an already loaded platform, based on the ratio
 *power/cost r r: double, difference in the ratio power/cost between machines
 *with different power
 **/
void barbSetHostCost(HostCost &hc, sg_host_t h, double ratio);

void setHC_host(HostCost &hc, sg_host_t h);
void setHC_iniC(HostCost &hc, double iniC);
void setHC_hC(HostCost &hc, double hC);
void setHC_tmpIni(HostCost &hc, double tmpIni);
void setHC_resTime(HostCost &hc, double resTime);
void setHC_occTime(HostCost &hc, double occTime);

void addHC_resTime(HostCost &hc, double resTime);
void addHC_occTime(HostCost &hc, double occTime);

sg_host_t getHC_host(HostCost hc);
double getHC_iniC(HostCost hc);
double getHC_hC(HostCost hc);
double getHC_storeC(HostCost hc);
double getHC_tmpIni(HostCost hc);
double getHC_resTime(HostCost hc);
double getHC_occTime(HostCost hc);
#endif
