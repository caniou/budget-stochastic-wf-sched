#! /bin/bash
# Observation : here we set the directorie where is our simulator as a volume 
# because we need to modified and test it.
# Another solution, if we don't need to modified the simulator, would be directly
# download the simulator in the Dockerfile

# set variable 
image=bmairesse-simgrid:v3.15
volume=~/budget-stochastic-wf-sched #the directorie where is the simulator
mount_point=/home/simulator
#exe=bash
exe=./tools/genXP/1_manageXP.sh

# run 
docker run -it -d --rm --volume $volume:$mount_point $image $exe
