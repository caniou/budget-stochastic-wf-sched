/* Perform DGEMM or DAXPI, returns duration of operation in seconds
Compile with gcc -O3 -lcblas benchMatrixOP.c -o benchMatrixOP
If modification: NO OUTPUT except for result!
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h> /* gettimeofday */
#include "cblas.h"

double timevalsub(struct timeval *tv1, const struct timeval *tv2) {
  double res = 0;
  res = tv2->tv_sec - tv1->tv_sec;
  res += (tv2->tv_usec - tv1->tv_usec)*1.0/1000000;
  return res;
}

int main ( int argc , char ** argv ) {
 
  int op=0 ; /* 1 dgemm | 2 sum */
  int N=0;
  /* Parse cmd line */
  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i],"-d")==0) {
      op=1 ;
    } else if (strcmp(argv[i],"-s")==0) {
      op=2 ;
    } else if (strcmp(argv[i],"-N")==0) {
      N=atoi(argv[++i]);
    } else {
      printf("ERROR, unknown option: %s\n",argv[i]);
      printf("Usage:\n"
             "-d | -s for resp. dgemm or daxpi\n"
	     "-N n, n nb of row/col of square matrices\n");
      exit(1);
    }
  }
  if (op==0) { printf("Missing operation\n"); exit(1); };
  if (N==0) { printf("Missing matrix size\n"); exit(1); };

  double *A, *B, *C;
  A= ( double *) malloc ( N * N * sizeof ( double ));
  B= ( double *) malloc ( N * N * sizeof ( double ));
  C= ( double *) malloc ( N * N * sizeof ( double ));

  /* Fill matrices with random values */
  double maxr=(double)RAND_MAX;
  for (int i = 0; i < N*N; i++) {
    A[i] = rand()/maxr;
    B[i] = rand()/maxr;
  }

  struct timeval start, stop;
  gettimeofday(&start, NULL);

  if (op==1) {
    /* Perform the operation : C = 1.0* A * B + 0* C */
    cblas_dgemm (CblasColMajor
		 , CblasNoTrans, CblasNoTrans
		 , N, N, N
		 , 1.0, A, N, B, N
		 , 0, C, N);
  } else if (op==2) {
    cblas_daxpy (N, 1.0, A, N, B, N);
  }
  gettimeofday(&stop, NULL);
  printf("%f\n",timevalsub(&start, &stop)); /* Seconds */
	 
  free ( A );
  free ( B );
  free ( C );
  return EXIT_SUCCESS ;
}
