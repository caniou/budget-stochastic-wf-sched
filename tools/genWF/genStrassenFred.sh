#!/bin/bash
# https://fr.wikipedia.org/wiki/Algorithme_de_Strassen
# https://hal.inria.fr/inria-00072106/file/RR-4482.pdf

# WARNING: graphviz requires giving 'Mother -> Sister' as relation. Yet we
# do not know job_ids of sisters. We use reverse, and a the end, put things
# in order.

if [ $(grep TODO $0 | grep -v WARNING | wc -l) -ne 0 ]; then
    echo WARNING: still some todo in the code
fi

function usage {
    echo "Usage: $0 { -l lvl | -h N | -n N:Nmax | -m lvl:Nmax } [-g]"
    echo lvl is recursion level
    echo N is size of original N*N matrices, considered as amount of data
    echo Nmax is the max size of Nmax*Nmax matrices, e.g., 10-500
    echo -g for graphviz output 
}    

# Strassen: records all operations, products AND sums, as different tasks
function strassenAll {

    if [ $1 -eq 0 ]; then
	echo Strassen for at least 1 recursion
	return
    fi

    local niv=$1 # Level of recursion we are in, among [1:LVL]
    countFctCall[$niv]=$((${countFctCall[$niv]} + 1))
    local ct=${countFctCall[$niv]}
    local A=$2
    local B=$3
    local n=$4 # nbrow of input square matrices
    local __parentCxy=$5 # name of parent variable to store Cx,y job_ids
    # $6 and possible $7 are the job_id of parent (linked by data)
    
    local sizemat=$(echo "$n * $n * 8" | bc) # 8 bytes for a double
    local sizeqmat=$(( sizemat / 4 ))
    local finishlevel="" # job_ids of C11, C12, C13 and C14

    local Q1_parent=""
    local Q2_parent=""
    local Q3_parent=""
    local Q4_parent=""
    local Q5_parent=""
    local Q6_parent=""
    local Q7_parent=""
    local U1_parent=""
    local U2_parent=""
    local U3_parent=""
    local U4_parent=""
    local C11_parent=""
    local C12_parent=""
    local C21_parent=""
    local C22_parent=""
    
    echo "Recursion ${niv} for $A * $B"

    #T1="${niv}_[T1(${A}11+${A}22)]"
    T1="[${niv}_T1($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    Q1_parent+="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${T1}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${A}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${A}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${T1}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    i=$6
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -ne 0 ]; then
	echo "$job_id => $i # ${T1}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
    
    #T6="${niv}_[T6(${B}11+${B}22)]"
    T6="[${niv}_T6($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    Q1_parent+="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${T6}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${B}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${B}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${T6}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$7
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -ne 0 ]; then
	echo "$job_id => $i # ${T6}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
    
    #Q1="${niv}_[${T1}*${T6}]"
    Q1="[${niv}_Q1($ct)]"
    Q1_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${T1}" "${T6}" $((n/2)) Q1_recursionDP ${Q1_parent}
	task_jobids="$Q1_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	task_jobids="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${Q1}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "<uses file=\"${T1}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${T6}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"Q1_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in ${Q1_parent}; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -ne 0 ]; then
		echo "$job_id => $i # ${Q1}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    U1_parent+="${task_jobids} "
    U3_parent+="${task_jobids} "
    
    #T2="${niv}_[T2(${A}21+${A}22)]"
    T2="[${niv}_T2($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    Q2_parent+="$7 $job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${T2}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${A}21_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${A}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${T2}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$6
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -ne 0 ]; then
	echo "$job_id => $i # ${T2}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"

    #Q2="${niv}_[Q2(${T2}*${B}11)]"
    Q2="[${niv}_Q2($ct)]"
    Q2_recursionDP="" # will contain recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${T2}" "${B}11_${niv}" $((n/2)) Q2_recursionDP ${Q2_parent}
	task_jobids="$Q2_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	task_jobids="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${Q2}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "<uses file=\"${T2}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${B}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${Q2}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in ${Q2_parent}; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -ne 0 ]; then
		echo "$job_id => $i # ${Q2}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    U4_parent+="${task_jobids} "
    C21_parent+="${task_jobids} "

    #T7="${niv}_[T7(${B}12-${B}22)]"
    T7="[${niv}_T7($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    Q3_parent+="$6 $job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${T7}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${B}12_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${B}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${T7}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$7
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -ne 0 ]; then
	echo "$job_id => $i # ${T7}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
    
    #Q3="${niv}_[Q3(${A}11*${T7})]"
    Q3="[${niv}_Q3($ct)]"
    Q3_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${A}11_${niv}" "${T7}" $((n/2)) Q3_recursionDP ${Q3_parent}
	task_jobids="$Q3_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	task_jobids="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${Q3}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "<uses file=\"${A}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${T7}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${Q3}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in ${Q3_parent}; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -ne 0 ]; then
		echo "$job_id => $i # ${Q3}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    U3_parent+="${task_jobids} "
    C12_parent+="${task_jobids} "

    #T8="${niv}_[T8(${B}21-${B}11)]"
    T8="[${niv}_T8($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    Q4_parent+="$6 $job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${T8}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${B}21_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${B}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${T8}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$7
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -ne 0 ]; then
	echo "$job_id => $i # ${T8}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
	    
    #Q4="${niv}_[Q4(${A}22*${T8})]"
    Q4="[${niv}_Q4($ct)]"
    Q4_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${A}22_${niv}" "${T8}" $((n/2)) Q4_recursionDP ${Q4_parent}
	task_jobids="$Q4_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	task_jobids="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${Q4}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "<uses file=\"${A}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${T8}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${Q4}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in $Q4_parent; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -ne 0 ]; then
		echo "$job_id => $i # ${Q4}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    U1_parent+="${task_jobids} "
    C21_parent+="${task_jobids} "

    #T3="${niv}_[T3(${A}11+${A}12)]"
    T3="[${niv}_T3($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    Q5_parent+="$7 $job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${T3}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${A}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${A}12_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${T3}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$6
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -ne 0 ]; then
	echo "$job_id => $i # ${T3}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
    
    #Q5="${niv}_[Q5(${T3}*${B}22)]"
    Q5="[${niv}_Q5($ct)]"
    Q5_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${T3}" "${B}22_${niv}" $((n/2)) Q5_recursionDP ${Q5_parent}
	task_jobids="$Q5_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	task_jobids="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${Q5}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "<uses file=\"${T3}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${B}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${Q5}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in $Q5_parent; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -ne 0 ]; then
		echo "$job_id => $i # ${Q5}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    U2_parent+="${task_jobids} "
    C12_parent+="${task_jobids} "

    #T4="${niv}_[T4(${A}21-${A}11)]"
    T4="[${niv}_T4($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    Q6_parent+="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${T4}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${A}21_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${A}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${T4}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$6
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -ne 0 ]; then
	echo "$job_id => $i # ${T4}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"

    #T9="${niv}_[T9(${B}11+${B}12)]"
    T9="[${niv}_T9($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    Q6_parent+="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${T9}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${B}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${B}12_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${T9}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$7
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -ne 0 ]; then
	echo "$job_id => $i # ${T9}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
    
    #Q6="${niv}_[Q6(${T4}*${T9})]"
    Q6="[${niv}_Q6($ct)]"
    Q6_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${T4}" "${T9}" $((n/2)) Q6_recursionDP ${Q6_parent}
	task_jobids="$Q6_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	task_jobids="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${Q6}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "<uses file=\"${T4}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${T9}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${Q6}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in ${Q6_parent}; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -ne 0 ]; then
		echo "$job_id => $i # ${Q6}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    U4_parent+="${task_jobids} "
    
    #T5="${niv}_[T5(${A}12-${A}22)]"
    T5="[${niv}_T5($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    Q7_parent+="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${T5}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${A}12_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${A}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${T5}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$6
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -ne 0 ]; then
	echo "$job_id => $i # ${T5}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
	
    #T10="${niv}_[T10(${B}21+${B}22)]"
    T10="[${niv}_T10($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    Q7_parent+="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${T10}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${B}21_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${B}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${T10}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$7
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -ne 0 ]; then
	echo "$job_id => $i # ${T10}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
    
    #Q7="${niv}_[Q7(${T5}*${T10}]"
    Q7="[${niv}_Q7($ct)]"
    Q7_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${T5}" "${T10}" $((n/2)) Q7_recursionDP ${Q7_parent}
	task_jobids="$Q7_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	task_jobids="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${Q7}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "<uses file=\"${T5}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${T10}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${Q7}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in $Q7_parent; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -ne 0 ]; then
		echo "$job_id => $i # ${Q7}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    U2_parent+="${task_jobids} "

    ##### Us
    #U1="${niv}_[U1(${Q1}+${Q4})]"
    U1="[${niv}_[U1($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${U1}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q1}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q4}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${U1}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    for i in ${U1_parent}; do
	echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	if [ $gz -ne 0 ]; then
	    echo "$job_id => $i # ${U1}" >> "${GZfilename}"
	fi
    done
    echo "</child>" >> "${WFfilename}Part3"
    C11_parent+="$job_id "

    #U2="${niv}_[U2(${Q5}-${Q7})]"
    U2="[${niv}_U2($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${U2}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q5}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q7}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${U2}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    for i in ${U2_parent}; do
	echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	if [ $gz -ne 0 ]; then
	    echo "$job_id => $i # ${U2}" >> "${GZfilename}"
	fi
    done
    echo "</child>" >> "${WFfilename}Part3"
    C11_parent+="$job_id "

    #U3="${niv}_[U3(${Q3}+${Q1})]"
    U3="[${niv}_U3($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${U3}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q3}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q1}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${U3}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    for i in ${U3_parent}; do
	echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	if [ $gz -ne 0 ]; then
	    echo "$job_id => $i # ${U3}" >> "${GZfilename}"
	fi
    done
    echo "</child>" >> "${WFfilename}Part3"
    C22_parent+="$job_id "

    #U4="${niv}_[U4(${Q2}-${Q7})]"
    U4="[${niv}_U4($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${U4}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q2}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q7}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${U4}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    for i in ${U4_parent}; do
	echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	if [ $gz -ne 0 ]; then
	    echo "$job_id => $i # ${U4}" >> "${GZfilename}"
	fi
    done
    echo "</child>" >> "${WFfilename}Part3"
    C22_parent+="$job_id "

    ##### Cxy
    #C11="${niv}_[C11(${U1}-${U2})]"
    C11="[${niv}_C11($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${C11}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${U1}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${U2}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${C11}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    for i in ${C11_parent}; do
	echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	if [ $gz -ne 0 ]; then
	    echo "$job_id => $i # ${C11}" >> "${GZfilename}"
	fi
    done
    echo "</child>" >> "${WFfilename}Part3"
    finishlevel+="$job_id "

    #C12="${niv}_[C12(${Q3}+${Q5})]"
    C12="[${niv}_C12($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${C12}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q3}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q5}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${C12}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    for i in ${C12_parent}; do
	echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	if [ $gz -ne 0 ]; then
	    echo "$job_id => $i # ${C12}" >> "${GZfilename}"
	fi
    done
    echo "</child>" >> "${WFfilename}Part3"
    finishlevel+="$job_id "

    #C21="${niv}_[C21(${Q2}+${Q4})]"
    C21="[${niv}_C21($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${C21}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q2}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${Q4}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${C21}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    for i in ${C21_parent}; do
	echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	if [ $gz -ne 0 ]; then
	    echo "$job_id => $i # ${C21}" >> "${GZfilename}"
	fi
    done
    echo "</child>" >> "${WFfilename}Part3"
    finishlevel+="$job_id "

    #C22="${niv}_[C22(${U3}-${U4})]"
    C22="[${niv}_C22($ct)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${C22}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "<uses file=\"${U3}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${U4}_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "<uses file=\"${C22}_${niv}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    for i in $C22_parent; do
	echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	if [ $gz -ne 0 ]; then
	    echo "$job_id => $i # ${C22}" >> "${GZfilename}"
	fi
    done
    echo "</child>" >> "${WFfilename}Part3"
    finishlevel+="$job_id "
    
    ##### Cxy / C0
    if [ ${niv} -eq 1 ]; then
	# Finish graph
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"FINISH\" version=\"1.0\" level=\"0\" runtime=\"0\">" >> "${WFfilename}Part2"
	echo "<uses file=\"${C11}_niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${C12}_niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${C21}_niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "<uses file=\"${C22}_niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	# Output C11 C12 C21 C22
	echo "<uses file=\"C_0\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"0\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in ${finishlevel}; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -ne 0 ]; then
		echo "$job_id => $i # FINISH" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    else # recursive
	echo "Parents recursive: $finishlevel"
	eval $__parentCxy=\"'$finishlevel'\"
    fi
}

########################################################################## MAIN

if [ $# -lt 2 ]; then
    usage
    exit 1
fi

# Nhost, biggest size of Matrix N*N that fits on host
# nbMatrices that can be loaded in RAM, 2 (yet, 12 sums for C_0)
nbmatricesonhost=2
Nhost=$(a="$nbmatricesonhost $(free | grep Mem)" ; echo $a | awk '{printf("%d\n",int(sqrt($3/$1)))}')

case $1 in
  "-l") LVL=$2
	if [ $LVL -eq 0 ]; then
	    usage
	    exit 1
	fi
	echo Compute original matrix size from size $Nhost on host for $LVL recursions
	N=$Nhost
	while [ $LVL -ne 0 ]; do
	    N=$(( N * 2 ))
	    LVL=$(( LVL - 1 ))
	done
	LVL=$2
	Nmin=$Nhost
	;;
  "-h") echo Compute recursion level so that $nbmatricesonhost matrices fit on host with original matrices of size $2x$2
	N=$2
	LVL=0
	while [ $N -gt $Nhost ]; do
	    N=$(( N / 2 ))
	    LVL=$(( LVL + 1 ))
	done
	Nmin=$N
	checkpow=$(echo "$N ^ ($LVL+1)" | bc)
	if [ $checkpow -ne $2 ]; then
	    N=$checkpow
	    echo $2 was given as size but not power of 2. Taking N=$checkpow
	else
	    N=$2
	fi
	;;
  "-n") N=$(echo $2 | cut -f 1 -d ':')
	Nmax=$(echo $2 | cut -f 2 -d ':')
	echo Compute level until matrices are at most of size $Nmax
	if [ $Nhost -lt $Nmax ] ;then
	    echo Warning: size $Nmax is greater than size for $nbmatricesonhost matrices on host
	fi
	LVL=0
	Ntmp=$N
	while [ $Ntmp -gt $Nmax ]; do
	    Ntmp=$(( Ntmp / 2 ))
	    LVL=$(( LVL + 1 ))
	done
	Nmin=$Ntmp
	checkpow=$(echo "$N ^ ($LVL+1)" | bc)
	if [ $checkpow -ne $N ]; then
	    N=$checkpow
	    echo $2 was given as size but not power of 2. Taking N=$checkpow
	fi
	;;
    -m) LVL=$(echo $2 | cut -f 1 -d ':')
	Nmax=$(echo $2 | cut -f 2 -d ':')
	echo Compute original matrix size from size $Nmax on host and $LVL level of recursion
	if [ $Nhost -lt $Nmax ] ;then
	    echo Warning: size $Nmax is greater than size for $nbmatricesonhost matrices on host
	fi
	N=$Nmax
	while [ $LVL -ne 0 ]; do
	    N=$(( N * 2 ))
	    LVL=$(( LVL - 1 ))
	done
	LVL=$2
	Nmin=$Nmax
	;;
    *) usage
       exit 1
esac
echo "StrassenAll with NxN A*B, N=$N and recursion level $LVL"
CPU=$(cat /proc/cpuinfo | grep 'model name' | cut -f 2 -d ':' | uniq | sed "s/[ ()]/_/g")
WFfilename="$(basename $0 .sh)${CPU}_${LVL}_$N"
if [ "x$3" == "x-g" ]; then
    case "x$4" in
      "x1") echo . Selected graphiz job_ids mode
	 gz=1
	 ;;
      "x2") echo . Selected graphiz OP mode
	 gz=2
	 ;;
      *) "No argument or argument not understood for -g. Abort."
	 exit 1
    esac
    GZfilename="${WFfilename}".dot
    echo "digraph G {" > "${GZfilename}"
else
    gz=0
fi
echo "Bench for sums"
size=$N
for i in $(seq 1 $((LVL+1))); do
    size=$(( size / 2 ))
    echo -n ". lvl $i : $size x $size -> "
    tmp="scale=6 ; (0"
    idxmax=3
    for j in $(seq 1 $idxmax); do
	tmp="$tmp + $(./benchMatrixOP -s -N $size)"
    done
    tabsum[$i]=$(echo "$tmp)/$idxmax" | bc | awk '{printf "%f", $0}')
    echo ${tabsum[$i]}
done
echo -n "Bench for dgemm of size $Nmin * $Nmin -> "
tmp="scale=6 ; (0"
idxmax=3
for j in $(seq 1 $idxmax); do
    tmp="$tmp+$(./benchMatrixOP -d -N $Nmin)"
done
durationDGEMM=$(echo "$tmp)/$idxmax" | bc | awk '{printf "%f", $0}')
echo $durationDGEMM
for i in $(seq 1 $LVL); do
    countFctCall[$i]=0
done
JOBCOUNT=0
job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
JOBCOUNT=1
# Part description of job
echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"START\" version=\"1.0\" level=\"0\" runtime=\"0\">" >> "${WFfilename}Part2"
sizemat=$(echo "$N * $N * 8" | bc) # 8 bytes for a double
echo "<uses file=\"A_0\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizemat\"/>" >> "${WFfilename}Part2"
echo "<uses file=\"B_0\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizemat\"/>" >> "${WFfilename}Part2"
echo "</job>" >> "${WFfilename}Part2"

varname="" # only useful by its presence in the call. Not sure mandatory.
strassenAll 1 A B $N varname $job_id $job_id

echo "Generating DAX file ${WFfilename}"
echo '<?xml version="1.0" encoding="UTF-8"?>' > "${WFfilename}"
echo """<adag xmlns=\"local generated\" version=\"2.1\" jobCount=\"$JOBCOUNT\" fileCount=\"0\" childCount=\"${JOBCOUNT}\">""" >> "${WFfilename}"
cat "${WFfilename}Part2" "${WFfilename}Part3" >> "${WFfilename}"
echo "</adag>" >> "${WFfilename}"
rm "${WFfilename}Part2" "${WFfilename}Part3"
if [ $gz -ne 0 ]; then
    echo "}" >> "${GZfilename}"
    echo . Generating .dot file for graphviz
fi
if [ $gz -eq 1 ]; then
    while read LINE; do
	if [ $(echo $LINE | grep '=' | wc -l) -eq 0 ]; then
	    echo $LINE
	else
	    echo $LINE | awk '{printf("\"%05d\" -> \"%05d\" # %s\n",$3,$1,$5)}'
	fi
    done < "${GZfilename}"  > 2"${GZfilename}"
    cp "${GZfilename}" test
    mv 2"${GZfilename}" "${GZfilename}"
elif [ $gz -eq 2 ]; then
    # Pb with nxt line is that we need to fix job_ids with _M[...]
    # echo $LINE | awk '{printf("\"%05d\\n%s\" -> \"%05d\"\n",$3,$5,$1)}
    cp "${GZfilename}" bis"${GZfilename}"
    lastjobid=-1
    while read LINE; do
	if [ $(echo $LINE | grep '=' | wc -l) -ne 0 ]; then
	    read jobid name <<< $(echo $LINE | awk '{printf("%05d %s\n",$1,$5)}')
	    if [ $jobid -ne $lastjobid ]; then
		sed -e "s/$jobid/${jobid}n${name}/g" < bis"${GZfilename}" > 3"${GZfilename}"
		mv  3"${GZfilename}" bis"${GZfilename}"
		lastjobid=${jobid}
	    fi
	fi
    done < "${GZfilename}"
    while read LINE; do
	if [ $(echo $LINE | grep '=' | wc -l) -eq 0 ]; then
	    echo $LINE
	else
	    echo $LINE | awk '{printf("\"%s\" -> \"%s\"\n",$3,$1)}'
	fi
    done < bis"${GZfilename}" >> 2"${GZfilename}"
    rm bis"${GZfilename}"
    sed -e "s/n/\\\n/g" < 2"${GZfilename}" > "${GZfilename}"
    mv 2"${GZfilename}" test
fi
if [ $gz -ne 0 ]; then
    if [ -x $(which dot | awk '{print $1}') ]; then
	gzout=${WFfilename}.svg
	echo Generating picture $gzout with graphviz
	dot -Tsvg -o$gzout "${GZfilename}"
    fi
fi
