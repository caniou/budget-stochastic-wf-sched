#!/bin/bash

# !!!!! please run this script at the root of the project !!!!!

readonly MAX_PARENT_FORK=4
readonly MAX_CHILD_FORK=5

readonly exe=$1
###############################################################################
################################# PREPARATION #################################
###############################################################################
info_child() {
	printf '\e[1;34m%-6s\e[m\n' ":::: CHILD $1"
}
info_parent() {
	printf '\e[1;33m%-6s\e[m\n' ":: PARENT $1"
}
info_grd_child() {
	printf '\e[1;32m%-6s\e[m\n' ":::::: GRD-CHILD $1"
}
info_warning() {
	printf '\e[1;31m%-6s\e[m\n' ":: WARNING $1"
}

################################# DIRECTORIES #################################
# the folder where we want to excute the data generation
info_parent "create generation directories"
gen_dir=./generation_$exe
if [ ! -d "$gen_dir" ]; then
	mkdir $gen_dir
fi
info_parent "create xp directories"
xp_dir=$gen_dir/xp
if [ ! -d "$xp_dir" ]; then
	mkdir $xp_dir
fi

info_parent "copie the executable and the data to the xp directories"
# executable
cp ./bin/$exe $xp_dir
# blank files
cp -r ./data/ $xp_dir

# the folder to stock all results
info_parent "create results directories"
results_dir=results
if [ ! -d "$gen_dir/$results_dir" ]; then
	mkdir $gen_dir/$results_dir
fi

# generate precalcul file
info_parent "create all directories which is need to know where is the simulation"
not_yet_dir=not_yet
if [ ! -d "$xp_dir/$not_yet_dir" ]; then
	info_parent "generate all precalcul files"
	./tools/genXP/3_genPreCalculFile.sh $exe || echo error
	mv ./precalcul $xp_dir/$not_yet_dir
fi

cd $xp_dir

# the differents folders to stock the generation progress
# all experiences which is finish
finish_dir=finish
if [ ! -d "$finish_dir" ]; then
	mkdir $finish_dir
fi
# the currents experiences
current_dir=current
if [ ! -d "$current_dir" ]; then
	mkdir $current_dir
fi

################################# ERROR FILE ##################################
# the file which will contain the execution error
info_parent "create the error file"
error_file=errors.txt
if [ ! -f ../$results_dir/$error_file ]; then
	touch ../$results_dir/$error_file
fi
cmd_fail=errors_cmd.txt
if [ ! -f ../$results_dir/$cmd_fail ]; then
	touch ../$results_dir/$cmd_fail
fi

###############################################################################
################################## EXECUTION ##################################
###############################################################################

# this function take a directorie name in parameter and process all subdir to 
# execute all simulation
process_dir() {
	processing_dir=$1
	info_parent "beginning of simulation"
	for dir in $(ls -1A $processing_dir); do
		if (($(jobs -p | wc -l) == $MAX_PARENT_FORK)); then
			info_parent "wait just one fork"
			wait
		fi
		# this fork process all xp in a directory (all xp which have the same workflow type, nb tsk and algo (ex: ligo30minmin))
		(# let's fork
			if [ "$processing_dir" = "$not_yet_dir" ]; then
				info_child "$dir : create current and finish directories"
				mkdir ./$dir || echo "error to create the directorie "
				mv ./$processing_dir/$dir $current_dir/$dir
				mkdir $finish_dir/$dir

				info_child "$dir : copie blank file"
				cp ./data/vierge/commonResultsVierge ./$dir/commonResults
				cp ./data/vierge/commonPreciseResultsVierge ./$dir/commonPreciseResults
				cp ./data/vierge/credoVierge ./$dir/credo
			else
				info_warning "not in the not yet dir"
			fi

			cd $dir
			for file in $(ls -1A ../$current_dir/$dir); do
				# NB_CHILD_FORK=$(jobs -p | wc -l)
				if (($(jobs -p | wc -l) >= $MAX_CHILD_FORK)); then
					info_child "$dir : wait just one fork"
					wait -n
				fi
				(# let's fork, it's use to run the simulation 
					# readonly file_name=${dir#*./$dir/} #delete the "./$not_yet_dir/" at the beginning
					while read line; do
						info_grd_child "$file run"
						$line >./"$file.log" 2>&1 || (info_warning "execution error $file" && echo $line >>../../$results_dir/$cmd_fail)
						rm ./"$file.log"
					done <../$current_dir/$dir/$file

					mv ../$current_dir/$dir/$file ../$finish_dir/$dir/$file
					sleep 2
				) &
			done

			info_child "$dir : wait all forks"
			# before finish, wait all forks
			while (($(jobs -p | wc -l) != 0)); do
				wait -n
			done

			cd ..
			# test if errors have occurred
			readonly nb_results=$(expr $(cat ./$dir/commonResults | wc -l) - 1)
			readonly nb_cmd=$(ls -1A ./$finish_dir/$dir | wc -l) # use to make a verification of error
			if (($nb_cmd != $nb_results)); then
				echo "$dir don't have the right number of result : $nb_results instead of $nb_cmd" >>../$results_dir/$error_file
			fi
			# mv and rm file and directorie	 
			mv ./$dir/commonResults ../$results_dir/$dir
			rm -r ./$dir
			rm -r ./$current_dir/$dir
			info_warning "$dir : FORK finish !!!"
		) &

	done
}

run 
process_dir $current_dir
process_dir $not_yet_dir


# before finish, wait all forks
info_parent "wait all forks"
while (($(jobs -p | wc -l) != 0)); do
	wait -n
done
info_warning "script is finish !!!"
