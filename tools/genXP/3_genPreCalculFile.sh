#!/bin/bash

# ./generateData.sh executable

# executable
simulator=$1

###############################################################################
#################################### DATA #####################################
###############################################################################

# list all scheduler algos (23 algos)
algos=(minmin minminBudg minminBudgDiscrEx heft heftBudg heftBudgMult heftBudgMult2FirstTask heftBudgMult2EveryTask heftBudgPath heftBudgMult2Path heftBudgCascade ftl ftlMultCascade ftlMultCascadeNbChild ftlMultCascadeChildPath ftlMultCascadeChildDesc ftlMultLastTaskLvl heftSecRep heftSecRepInv arabnejad criticalGreedy CGFair CGFairSecRep)

# list of differents workflows (is like an array)
workflows=(ligo cybershake montage) # montage cybershake)

# list of seed from 0 to 30
seeds=$(seq 1 1 30)

# numbers of tasks to simulate per different wf
numbersTasks=(30 60 90)	

##################################### DIR #####################################
# this directory will contain our experimentation's results
output_dir=./precalcul
if [ -d "$output_dir" ]; then
	rm -r $output_dir
fi
if [ ! -d "$output_dir" ]; then
	mkdir $output_dir
fi
# workflow directory
wf_files_dir=../data/workflows
# the platform file
if [ "$simulator" == "hex_price_1" ]; then
	platform_file=../data/platforms/AL100_PRICE1.xml
elif [ "$simulator" == "hex_price_2" ]; then
	platform_file=../data/platforms/AL100_PRICE2.xml
fi

###############################################################################
#################################### MAIN #####################################
###############################################################################
# (# fork for detach the scrip of the current process

# process number of tasks
for nbTasks in ${numbersTasks[@]}; do
	# differents budget for differents workflows
	# 30 tasks
	if [ "$nbTasks" == "30" ]; then
		if [ "$simulator" == "hex_price_1" ]; then
			budgetLigo=$(seq 1250 42 1502)
			budgetMontage=$(seq 50 21 176)
			budgetCybershake=$(seq 150 25 300)
		elif [ "$simulator" == "hex_price_2" ]; then
			budgetLigo=$(seq 55 14 139)
			budgetMontage=$(seq 2 12 74)
			budgetCybershake=$(seq 20 64 404)
		fi
	# 60 tasks
	elif [ "$nbTasks" == "60" ]; then
		if [ "$simulator" == "hex_price_1" ]; then
			budgetLigo=$(seq 2300 84 2804)
			budgetMontage=$(seq 120 30 300)
			budgetCybershake=$(seq 300 50 600)
		elif [ "$simulator" == "hex_price_2" ]; then
			budgetLigo=$(seq 3700 3720 26020)
			budgetMontage=$(seq 200 220 1500)
			budgetCybershake=$(seq 500 550 3800)
		fi

	# 90 tasks
	elif [ "$nbTasks" == "90" ]; then
		if [ "$simulator" == "hex_price_1" ]; then
			budgetLigo=$(seq 3500 120 4220)
			budgetMontage=$(seq 200 42 452)
			budgetCybershake=$(seq 450 60 810)
		elif [ "$simulator" == "hex_price_2" ]; then
			budgetLigo=$(seq 5400 5600 39000)
			budgetMontage=$(seq 300 320 2220)
			budgetCybershake=$(seq 700 785 5410)
		fi
	fi
	# process differents workflows
	for wf in ${workflows[@]}; do
		# select the seed's array and the budget's array
		if [ "$wf" == "ligo" ]; then
			arrBudget=$budgetLigo
		elif [ "$wf" == "montage" ]; then
			arrBudget=$budgetMontage
		elif [ "$wf" == "cybershake" ]; then
			arrBudget=$budgetCybershake
		fi

		# process all algos
		for algo in ${algos[@]}; do
			wfAlgo=$wf${nbTasks}$algo
			if [ ! -d "$output_dir/$wfAlgo" ]; then
				mkdir $output_dir/$wfAlgo
			fi
			# if [ -d "$output_dir/$directory" ]; then
			#     rm $output_dir/$file # reset the file if exist
			# fi
			# process all seeds
			for seed in ${seeds[@]}; do
				num=$seed # modified if we want a seed different of wf's shape
				# process all budgets
				for budget in $arrBudget; do
					# echo "      budget = $budget , seed = $seed , num = $num"
					current_wf="$wf${nbTasks}_$num"

					wf_file=$wf_files_dir/$wf/$current_wf
					output_file="$output_dir/$wfAlgo/$wfAlgo${seed}_$budget"
					touch $output_file

					echo ../$simulator $algo $seed $wf_file $platform_file 0.25 0.0 900 $budget 0.2 >>$output_file

					# echo $workflows $nbTasks $algo $seed $budget >>$output_dir/$file

				done # budgets
			done  # seeds
		done   # algos

	done # workflows
done  # nb tasks
# ) &
