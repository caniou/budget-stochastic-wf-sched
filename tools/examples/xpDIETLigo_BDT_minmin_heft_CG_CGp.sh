#!/bin/bash
seeds="9022 15295 6842 15068 490 15593 27709 15494 16290 17932 22035 13051 15475 14588 24671 4313 9972 26668 13120 6563 22669 13679 3380 2594 31220 7311 4573 24358 20675 27256"
nums=`seq 1 1 30`

for id in `seq 0 1 29`; do
    #id=`echo "scale=4;$num-16"|bc`
    arrNums=($nums)
    num=${arrNums[$id]}
    arrSeeds=($seeds)
    seed=${arrSeeds[$id]}
    echo "idx = $id ; shape = $num ; seed = $seed"

    # minmin
    echo "minmin"
    for budget in  `seq 1100 100 1600` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex minmin $seed  data/workflows/ligo/ligo30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	mkdir ligo30_$num/MM/
	mkdir ligo30_$num/MM/$budget
        cp static_sched ligo30_$num/MM/$budget/static_sched$budget
	make resetGen
    done

    # BDT
    echo "BDT"
    for budget in  `seq 1100 100 1600` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex arabnejad $seed  data/workflows/ligo/ligo30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	mkdir ligo30_$num/BDT/
	mkdir ligo30_$num/BDT/$budget
        cp static_sched ligo30_$num/BDT/$budget/static_sched$budget
	make resetGen
    done

    # heft
    echo "heft"
    for budget in  `seq 1100 100 1600` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heft $seed  data/workflows/ligo/ligo30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	mkdir ligo30_$num/HH/
	mkdir ligo30_$num/HH/$budget
        cp static_sched ligo30_$num/HH/$budget/static_sched$budget
	make resetGen
    done
    
    # CG
    echo "CG"
    for budget in  `seq 1100 100 1600` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex CGFair $seed  data/workflows/ligo/ligo30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	mkdir ligo30_$num/CG/
	mkdir ligo30_$num/CG/$budget
        cp static_sched ligo30_$num/CG/$budget/static_sched$budget
	make resetGen
    done

    # CGp
    echo "CGp"
    for budget in  `seq 1100 100 1600` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex CGFairSecRep $seed  data/workflows/ligo/ligo30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	mkdir ligo30_$num/CGp/
	mkdir ligo30_$num/CGp/$budget
        cp static_sched ligo30_$num/CGp/$budget/static_sched$budget
	make resetGen
    done
    
done
