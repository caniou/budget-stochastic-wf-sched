#!/bin/bash
seeds="0 2 3 4 5 6 7 8 9 10 642 775 35 471 807"

for num in `seq 1 1 15`; do
    id=`echo "scale=4;$num-1"|bc`
    arrSeeds=($seeds)
    seed=${arrSeeds[$id]}
    echo "idx = $id ; shape = $num ; seed = $seed"

    # heftBudg+Inv
    echo "heftBudg+Inv"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftSecRepInv $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done

    # heftBudg+
    echo "heftBudg+"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftSecRep $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done

    # heftBudg
    echo "heftBudg"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftBudg $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done

    # heftBudgMult
    echo "heftBudgMult"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftBudgMult $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done

    # minminBudg
    echo "MinminBudg"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex minminBudg $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done
    
done
