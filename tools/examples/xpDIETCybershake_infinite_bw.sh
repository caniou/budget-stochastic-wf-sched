#!/bin/bash
seeds="31991 17261 27485 10910 10238 15723 23160 7058 4043"
nums="1 2 4 5 6 7 8 9 10"

for id in `seq 0 1 8`; do
    #id=`echo "scale=4;$num-16"|bc`
    arrNums=($nums)
    num=${arrNums[$id]}
    arrSeeds=($seeds)
    seed=${arrSeeds[$id]}
    echo "idx = $id ; shape = $num ; seed = $seed"

    # heftBudg+Inv
    echo "heftBudg+Inv"
    for budget in  `seq 140 116 836` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftSecRepInv $seed  data/workflows/Cybershake/cybershake30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	mkdir cybershake30_infinite_bw_$num
	mkdir cybershake30_infinite_bw_$num/HBpI
	mkdir cybershake30_infinite_bw_$num/HBpI/$budget
	cp static_sched cybershake30_infinite_bw_$num/HBpI/$budget/static_sched$budget
	make resetGen
    done

    # heftBudg+
    echo "heftBudg+"
    for budget in  `seq 140 116 836` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftSecRep $seed  data/workflows/Cybershake/cybershake30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	mkdir cybershake30_infinite_bw_$num
	mkdir cybershake30_infinite_bw_$num/HBp
	mkdir cybershake30_infinite_bw_$num/HBp/$budget
	cp static_sched cybershake30_infinite_bw_$num/HBp/$budget/static_sched$budget
	make resetGen
    done
    
    # heftBudg
    echo "heftBudg"
    for budget in  `seq 140 116 836` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftBudg $seed  data/workflows/Cybershake/cybershake30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	mkdir cybershake30_infinite_bw_$num
	mkdir cybershake30_infinite_bw_$num/HB
	mkdir cybershake30_infinite_bw_$num/HB/$budget
	cp static_sched cybershake30_infinite_bw_$num/HB/$budget/static_sched$budget
	make resetGen
    done
    
    # minminBudg
    echo "minminBudg"
    for budget in  `seq 140 116 836` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex minminBudg $seed  data/workflows/Cybershake/cybershake30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	mkdir cybershake30_infinite_bw_$num
	mkdir cybershake30_infinite_bw_$num/MB
	mkdir cybershake30_infinite_bw_$num/MB/$budget
	cp static_sched cybershake30_infinite_bw_$num/MB/$budget/static_sched$budget
	make resetGen
    done   

    # heftBudgMult
    echo "heftBudgMult"
    for budget in  `seq 140 116 836` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftBudgMult $seed  data/workflows/Cybershake/cybershake30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	mkdir cybershake30_infinite_bw_$num
	mkdir cybershake30_infinite_bw_$num/HBM
	mkdir cybershake30_infinite_bw_$num/HBM/$budget
	cp static_sched cybershake30_infinite_bw_$num/HBM/$budget/static_sched$budget
	make resetGen
    done
    
done
