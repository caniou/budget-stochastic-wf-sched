#!/bin/bash
seeds="26328 1441 5121 27830 15283 30071 5785 9096 26868 16110 2365 17341 11932 15859 11088"
for num in `seq 16 1 30`; do
    id=`echo "scale=4;$num-16"|bc`
    arrSeeds=($seeds)
    seed=${arrSeeds[$id]}
    echo "idx = $id ; shape = $num ; seed = $seed"

    # heftBudg+Inv
    echo "heftBudg+Inv"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftSecRepInv $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	#mkdir montage30_$num/HBpI/
	#mkdir montage30_$num/HBpI/$budget
        cp static_sched montage30_$num/HBpI/$budget/static_sched$budget
	make resetGen
    done   
done
