#!/bin/bash
seeds="0 2 3 4 5 6 7 8 9 10 642 775 35 471 807 26328 1441 5121 27830 15283 30071 5785 9096 26968 16110 2365 17341 11932 15859 11088"
nums=`seq 1 1 30`

for id in `seq 0 1 29`; do
    #id=`echo "scale=4;$num-16"|bc`
    arrNums=($nums)
    num=${arrNums[$id]}
    arrSeeds=($seeds)
    seed=${arrSeeds[$id]}
    echo "idx = $id ; shape = $num ; seed = $seed"

    # heftBudg+Inv
    echo "heftBudg+Inv"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftSecRepInv $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done

    # heftBudg+
    echo "heftBudg+"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftSecRep $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done

    # heftBudg
    echo "heftBudg"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftBudg $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done

    # heftBudgMult
    echo "heftBudgMult"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftBudgMult $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done

    # minminBudg
    echo "minminBudg"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex minminBudg $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done
    
done
